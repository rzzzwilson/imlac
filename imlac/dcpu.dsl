####################################################
# DSL code to test operation of the graphics CPU.
#
# Note that the test code zeros all of memory and
# AC+L at start.  The 0o000000 opcode is a HLT.
####################################################

# uncomment line below to stop on first error
onerror ignore

# just test very basic DCPU stuff

# DHLT
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DHLT]; run 0100
    checkcycles 5; checkreg ac 0106; checkreg pc 0106
    checkdcycles 1; checkreg dpc 0107

# DNOP
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DNOP|DHLT]; run 0100
    checkcycles 8; checkreg ac 0106; checkreg pc 0106
    checkdcycles 2; checkreg dpc 0110

# DJMP       100      101 102 103 104      105 106       107  110
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DJMP 0110|DHLT|DHLT]; run 0100
    checkcycles 8; checkreg ac 0106; checkreg pc 0106
    checkdcycles 2; checkreg dpc 0111

# DJMS
#            100      101 102 103 104      105 106       107  110
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DJMS 0110|DHLT|DHLT]; run 0100
    checkcycles 8;
    checkreg ac 0106; checkreg pc 0106
    checkdcycles 2: checkreg dpc 0111; checkreg lendt 1

#            100      101 102 103 104      105 106       107  110       111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DJMS 0110|DHLT|DJMS 0111|DHLT]; run 0100
    checkcycles 8; checkreg ac 0106; checkreg pc 0106;
    checkdcycles 3; checkreg dpc 0112; checkreg lendt 2

# DJRM
#            100      101 102 103 104      105 106       107  110       111  112  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DJMS 0110|DHLT|DJMS 0113|DRJM|DHLT|DRJM]; run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 5; checkreg dpc 0110; checkreg lendt 0

# DEIM
#            100      101 102 103 104      105 106    107       110       111       112         113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC e,b33|data 040100|DHLT]; run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0103; checkreg dy 0103

# DLXA
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0200|DHLT]; run 0100
    checkcycles 8; checkdcycles 2
    checkreg ac 0106; checkreg pc 0106
    checkreg dpc 0110; checkreg dx 0200

# DLYA
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLYA 0300|DHLT]; run 0100
    checkcycles 8; checkdcycles 2
    checkreg ac 0106; checkreg pc 0106
    checkreg dpc 0110; checkreg dy 0300

# DSTS
# NEED TO CHECK DSTS RESULTS, EFFECT ON DX, DY

#            100      101 102 103 104      105 106    107       110       111       112         113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC E,B33|DATA 040100|DHLT]; run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0103; checkreg dy 0103
#            100      101 102 103 104      105 106    107       110       111       112         113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 1|DLXA 0100|DLYA 0100|INC e,b33|data 040100|DHLT]; run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0106; checkreg dy 0106

#            100      101 102 103 104      105 106    107       110       111       112         113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0100|INC E,B33|DATA 040100|DHLT]; run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0114; checkreg dy 0114

#            100      101 102 103 104      105 106    107       110       111       112         113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 3|DLXA 0100|DLYA 0100|INC E,B33|DATA 040100|DHLT]; run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0122; checkreg dy 0122	# DSTS wrong?

######
# Check the DEIM operation.  Move beam +3 in X and Y direction.
# Check DX/DY after DSTS 0/1/2/3.
######

#            100      101 102 103 104      105 106    107       110       111       112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC E,B33|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0103; checkreg dy 0103

#            100      101 102 103 104      105 106    107       110       111         112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC E,B3-3|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0103; checkreg dy 0075

#            100      101 102 103 104      105 106    107       110       111         112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC E,D-33|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0075; checkreg dy 0103

#            100      101 102 103 104      105 106    107       110       111         112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 0|DLXA 0100|DLYA 0100|INC E,B-3-3|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0075; checkreg dy 0075

#            100      101 102 103 104      105 106    107       110       111       112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 1|DLXA 0100|DLYA 0100|INC E,B33|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0106; checkreg dy 0106

#            100      101 102 103 104      105 106    107       110       111         112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 1|DLXA 0100|DLYA 0100|INC E,B-3-3|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0072; checkreg dy 0072

#            100      101 102 103 104      105 106    107       110       111       112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0100|INC E,B33|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0114; checkreg dy 0114

#            100      101 102 103 104      105 106    107       110       111       112 exit INC  113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 3|DLXA 0100|DLYA 0100|INC E,B33|INC A100,A100|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114
    checkreg dx 0122; checkreg dy 0122

######
# Check some short-vector non-increment micro-ops
######

# exit - A100 in first byte
#            100      101 102 103 104      105 106       107(exit)     110
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|INC E,B33|INC A100,A000|DHLT]
    run 0100
    checkcycles 8; checkreg ac 0106; checkreg pc 0106
    checkdcycles 3; checkreg dpc 0111

# exit - A100 in last byte
#            100      101 102 103 104      105 106       107(exit)     110
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|INC E,B33|INC A000,A100|DHLT]
    run 0100
    checkcycles 8; checkreg ac 0106; checkreg pc 0106
    checkdcycles 3; checkreg dpc 0111

# drjm - A040 + exit
#            100      101 102 103 104      105 106    107       110       111       112  113       114 (x,r)     115
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0100|DJMS 0113|DHLT|INC E,B33|INC A000,A140|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 7; checkreg dpc 0113

# +1 to DX(msb) - exit+(+1 to DX (msb))
#            100      101 102 103 104      105 106    107       110       111       112(+1->Xmsb) 113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0100|INC E,B00|INC A120,A000|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114; checkreg dx 0140

# +1 to DY(msb) - exit+(+1 to DY (msb))
#            100      101 102 103 104      105 106    107       110       111       112(+1->Xmsb) 113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0100|INC E,B00|INC A102,A000|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114; checkreg dy 0140

# 0 to DX(lsb) - exit+(0 to DX (lsb))
#            100      101 102 103 104      105 106    107       110       111       112(0->Xlsb) 113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0137|DLYA 0100|INC E,B00|INC A110,A000|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114; checkreg dx 0100

# 0 to DY(lsb) - exit+(0 to DY (lsb))
#            100      101 102 103 104      105 106    107       110       111       112(+1->Xmsb) 113
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DSTS 2|DLXA 0100|DLYA 0137|INC E,B00|INC A101,A000|DHLT]
    run 0100
    checkcycles 14; checkreg ac 0106; checkreg pc 0106
    checkdcycles 6; checkreg dpc 0114; checkreg dy 0100

######
# Test other display opcodes
######

# DIXM
#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 0000|DIXM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dx 0040

#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0340|DLYA 0000|DIXM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dx 0400

#            100      101 102 103 104      105 106        107    110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 03740|DLYA 0|DIXM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dx 0000

# DIYM
#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 0000|DIYM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dy 0040

#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 0340|DIYM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dy 0400

#            100      101 102 103 104      105 106    107        110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0|DLYA 03740|DIYM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dy 0000


# DDXM
#            100      101 102 103 104      105 106      107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 0000|DDXM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dx 01740

#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 01777|DLYA 0000|DDXM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dx 01737

# DDYM
#            100      101 102 103 104      105 106       107       110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 0000|DDYM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dy 01740

#            100      101 102 103 104      105 106       107        110  111
setmem 0100 [LAW 0106|DLA|DON|DSN|JMP 0103|HLT|DLXA 0000|DLYA 01777|DDYM|DHLT]
    run 0100
    checkcycles 11; checkreg ac 0106; checkreg pc 0106
    checkdcycles 4; checkreg dpc 0112; checkreg dy 01737

