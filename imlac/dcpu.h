//*****************************************************************************
// External routines for the display CPU.
//*****************************************************************************

#ifndef DCPU_H
#define DCPU_H

WORD dcpu_get_PC(void);
WORD dcpu_get_prev_PC(void);
void dcpu_set_PC(WORD value);
void dcpu_set_DRSindex(int value);
int dcpu_get_DRSindex(void);
void dcpu_set_DX(WORD value);
void dcpu_set_DY(WORD value);
WORD dcpu_get_DX(void);
WORD dcpu_get_DY(void);
bool dcpu_running(void);

int dcpu_execute_one(void);
bool dcpu_running(void);
void dcpu_start(void);
void dcpu_stop(void);
void dcpu_set_drsindex(int index);
int dcpu_get_x(void);
int dcpu_get_y(void);

#endif
