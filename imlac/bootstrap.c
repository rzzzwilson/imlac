///****************************************************************************
// Define the ROM code for different types of load.
///****************************************************************************


#include "imlac.h"
#include "bootstrap.h"

// PTR ROM Rbootloader code
WORD PtrROMImage[] = {0060077, // start  lac    base  ;40 get load address
                      0020010, //        dac    10    ;41 put into auto-inc reg
                      0104100, //        lwc    0100  ;42 -0100 into AC
                      0020020, //        dac    20    ;43 put into memory
                      0001061, //        hon          ;44 start PTR
                      0100011, // wait   cal          ;45 clear AC+LINK
                      0002400, //        hsf          ;46 skip if PTR has data
                      0010046, //        jmp    .-1   ;47 wait until is data
                      0001051, //        hrb          ;50 read PTR -> AC
                      0074075, //        sam    what  ;51 skip if AC == 2
                      0010045, //        jmp    wait  ;52 wait until PTR return 0
                      0002400, // loop   hsf          ;53 skip if PTR has data
                      0010053, //        jmp    .-1   ;54 wait until is data
                      0001051, //        hrb          ;55 read PTR -> AC
                      0003003, //        ral    3     ;56 move byte into high AC
                      0003003, //        ral    3     ;57
                      0003002, //        ral    2     ;60
                      0102400, //        hsn          ;61 wait until PTR moves
                      0010061, //        jmp    .-1   ;62
                      0002400, //        hsf          ;63 skip if PTR has data
                      0010063, //        jmp    .-1   ;64 wait until is data
                      0001051, //        hrb          ;65 read PTR -> AC
                      0120010, //        dac    *10   ;66 store word, inc pointer
                      0102400, //        hsn          ;67 wait until PTR moves
                      0010067, //        jmp    .-1   ;70
                      0100011, //        cal          ;71 clear AC & LINK
                      0030020, //        isz    20    ;72 inc mem and skip zero
                      0010053, //        jmp    loop  ;73 if not finished, jump
                      0110076, //        jmp    *go   ;74 execute loader
                      0000002, // what   data   2     ;75
                      0003700, // go     word   03700H;76
                      0003677  // base   word   03677H;77
                     };

// TTY ROM Rbootloader code
WORD TtyROMImage[] = {0060077, //         LAC     staddr  ;40
                      0020010, //         DAC     010     ;41 010 points to loading word
                      0104076, //         LWC     blsize-2;42
                      0020020, //         DAC     020     ;43 020 is ISZ counter of loader size
                      0001032, // skpzer  RCF             ;44
                      0100011, //         CAL             ;45
                      0002040, //         RSF             ;46 wait for next byte
                      0010046, //         JMP     .-1     ;47
                      0001031, //         RRB             ;50 get next TTY byte
                      0074075, //         SAM     fbyte   ;51 wait until it's the expected byte
                      0010044, //         JMP     skpzer  ;52
                      0002040, // nxtwrd  RSF             ;53 wait until TTY byte ready
                      0010053, //         JMP     .-1     ;54
                      0001033, //         RRC             ;55 get high byte and clear flag
                      0003003, //         RAL     3       ;56 shift into AC high byte
                      0003003, //         RAL     3       ;57
                      0003002, //         RAL     2       ;60
                      0002040, //         RSF             ;61 wait until next TTY byte
                      0010061, //         JMP     .-1     ;62
                      0001033, //         RRC             ;63 get low byte and clear flag
                      0120010, //         DAC     *010    ;64 store word
                      0100011, //         CAL             ;65 clear AC ready for next word
                      0030020, //         ISZ     020     ;66 finished?
                      0010053, //         JMP     nxtwrd  ;67 jump if not
                      0110076, //         JMP     *blstrt ;70 else execute the blockloader
                      0000000, //         DATA    000000  ;71 empty space?
                      0000000, //         DATA    000000  ;72
                      0000000, //         DATA    000000  ;73
                      0000000, //         DATA    000000  ;74
                      0000002, // fbyte   DATA    000002  ;75 expected first byte of block loader
                      0037700, // blstrt  data    bladdr  ;76 start of blockloader code
                      0037677  // staddr  data    bladdr-1;77 ISZ counter for blockloader size
                     };
