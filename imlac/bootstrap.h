//*****************************************************************************
// Interface for the bootstrap code.
//*****************************************************************************

#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

#include "imlac.h"

extern WORD PtrROMImage[];
extern WORD TtyROMImage[];

#endif
