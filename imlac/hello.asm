; Code taken from the PDS-1 Technical Manual, 6_2.18

	org	0100	;
start	dof		; display off
loop	dsn		; skip if display is off
	jmp	.-1	;
	ssf		; skip if 40 cycle sync on
	jmp	.-1	;
	scf		; clear 40 cycle sync
	lda		; read data switches to AC
	and	highbit	; check high data switch
	dac	.+1	; save HLT or NOP below
	data	0	;
	law	display	; show the display
	dla		;
	don		;
	jmp	loop	;
;------------------------
display	dlxa	0100	; set posn to 64,64
	dlya	0100	;
	djms	dsts2	;
	djms	dsts1	;
	djms	dsts0	;
	dlxa	04000	; centre beam
	dlya	04000	;
	dhlt		; stop display
			;
dsts2	dsts	2	; DSTS 2
	dlxa	0100	; set posn to 64,64
	dlya	0100	;
	djms	h	; write "hello"
	djms	e	;
	djms	l	;
	djms	l	;
	djms	o	;
	drjm		;
			;
dsts1	dsts	1	; DSTS 1
	dlxa	0100	; set posn to 64,128
	dlya	0200	;
	djms	h	; write "hello"
	djms	e	;
	djms	l	;
	djms	l	;
	djms	o	;
	drjm		;
			;
dsts0	dsts	0	; DSTS 0
	dlxa	0100	; set posn to .192,192
	dlya	0300	;
	djms	h	; write "hello"
	djms	e	;
	djms	l	;
	djms	l	;
	djms	o	;
	drjm		;
			;
h	inc	e,b03	; letter "H"
	inc	03,02	;
	inc	d30,30	;
	inc	b0-3,0-3;
	inc	0-2,d03	;
	inc	01,b-30	;
	inc	-30,f	;
			;
e	inc	e,B03	; letter "E"
	inc	03,02	;
	inc	30,30	;
	inc	d-1-3,-1-1 ;
	inc	b-30,-10;
	inc	d0-3,0-1;
	inc	b30,30	;
	inc	f,f	;

;l	inc	e,b03	; letter "L"
;	inc	03,02	;	
;	inc	a1,p	;
;	inc	30,30	;
;	inc	f,f	;
			;
l	inc	e,d03	; letter "L"
	inc	d03,d02
	inc	b0-3,b0-3
	inc	b0-2,b30
	inc	b20,f
			;
o	inc	e,d02	; letter "O"
	inc	b03,23	;
	inc	20,2-3	;
	inc	0-3,-2-2;
	inc	-20,-22	;
	inc	f,f	;
;-----------------------;
highbit	data	0100000	; high bit set
;------------------------
	end	start
