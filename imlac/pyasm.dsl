####################################################
# DSL code to test operation of the pyasm assembler.
####################################################

# uncomment line below to stop on first error
onerror ignore

# check multiple assembler instructions in DSL
setmem 0100 [LAW 1|NOP|NOP|NOP]; run 0100 0103
    checkcycles 3; checkreg pc 0103; checkreg ac 1
    checkmem 0077 0000000; checkmem 0100 0004001; checkmem 0101 0100000
    checkmem 0102 0100000; checkmem 0103 0100000; checkmem 0104 0000000
setmem 0100 [LAW 1|NOP|HLT|NOP]; setreg pc 0100; run
    checkcycles 3; checkreg pc 0103; checkreg ac 1
    checkmem 0077 0000000; checkmem 0100 0004001; checkmem 0101 0100000
    checkmem 0102 0000000; checkmem 0103 0100000; checkmem 0104 0000000
