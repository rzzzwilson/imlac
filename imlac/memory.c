/*
 * Implementation of imlac memory.
 *
 * Memory size is a compile-time constant.
 * The code here handles indirect memory references as well
 * as the 8 auto-index registers at 010-017 in every 2K block.
 */

#include <stdio.h>
#include <stdlib.h>
#include "memory.h"
#include "log.h"

/*****
 * constants for the memory
 ******/

/* size of memory is 16K words */
#define ADDR_MASK	    0x3fff

/* mask  and limits to check if address is auto-index register */
#define INDEX_MASK	    03777
#define LOWER_INDEX	    010
#define HIGHER_INDEX	017

/*****
 * State variables for the memory emulation
 ******/

#define ROM_START	    040
#define ROM_END		    077
#define ROM_SIZE	    (ROM_END - ROM_START + 1)

static bool rom_readonly = false;

/* the physical memory */
static WORD memory[MEM_SIZE];

/******
 * Macro to decide if we are using an auto-index register
 ******/

//#define AUTO_INDEX(a)	(((a & INDEX_MASK) >= LOWER_INDEX) && ((a & INDEX_MASK) <= HIGHER_INDEX))
//#define ISAUTOINC(a)   ((((a) & 03777) >= 010) && (((a) & 03777) <= 017))


//*****************************************************************************
// Get a word of data from 'address'.  If 'indirect' is TRUE, do an indirect
// fetch.  Return the word of data.
//*****************************************************************************

WORD
mem_get(WORD address, bool indirect)
{
    WORD a = address & ADDR_MASK;	/* wrap address to physical memory */
    WORD result;

    if (indirect && ISAUTOINC(a))	/* if auto-index and indirect, increment */
	    memory[a] = (memory[a] + 1) & WORD_MASK;

    result = memory[a & ADDR_MASK];
    if (indirect)
	    result = memory[result & ADDR_MASK];

    return result;
}

//*****************************************************************************
// Put a word of data, 'value' into 'address' in memory.  If 'indirect' is TRUE,
// do indirect put.
//*****************************************************************************

void
mem_put(WORD address, bool indirect, WORD value)
{
//    vlog("mem_put: address=%07o, indirect=%s, value=%07o",
//	 address, indirect ? "true" : "false", value);

    WORD a = address & ADDR_MASK;	/* wrap address to physical memory */

    if (indirect && ISAUTOINC(a))	/* if auto-index and indirect, increment */
    	memory[a] = (memory[a] + 1) & WORD_MASK;

    if (indirect)
    	a = memory[a & ADDR_MASK];

    memory[a & ADDR_MASK] = value;

//    vlog("mem_put: after, address=%07o is %07o",
//         a & ADDR_MASK, memory[a & ADDR_MASK]);
}

//*****************************************************************************
// Clear memory to the value in 'value'.
// Protects ROM if that is not writable.
//*****************************************************************************

void
mem_clear(WORD value)
{
    if (rom_readonly)
    {	/* save the ROM contents */
        WORD rom[ROM_SIZE];

        memcpy(rom, &memory[ROM_START], sizeof(WORD)*ROM_SIZE);
        memset(memory, value, sizeof(WORD)*MEM_SIZE);
        memcpy(&memory[ROM_START], rom, sizeof(WORD)*ROM_SIZE);
    }
    else
        memset(memory, value, sizeof(WORD)*MEM_SIZE);
}

//*****************************************************************************
// Set ROM memory.  Read ROM_SIZE words from 'rom'.
// Sets the ROM to be write-protected.
//*****************************************************************************

void
mem_set_rom(WORD *rom)
{
    rom_readonly = true;
    memcpy(&memory[ROM_START], rom, sizeof(WORD)*ROM_SIZE);
}

//*****************************************************************************
// Set ROM write-protect flag.  'true' means ROM is not writable.
//*****************************************************************************

void
mem_set_rom_readonly(bool readonly)
{
    rom_readonly = readonly;
}

//*****************************************************************************
// Load memory from a 'core' file.
// Use this to implement the imlac persistent core memory feature.
//*****************************************************************************

void
mem_load_core(char *filename)
{
    FILE *fd = fopen(filename, "rb");
    WORD addr;

    if (fd == NULL)
    {
        vlog("Can't open file '%s': %s", strerror(errno));
	exit(1);
    }

    for (addr = 0; addr < MEM_SIZE; ++addr)
    {
        unsigned char byte1;
        unsigned char byte2;
        WORD value;
	size_t ignored;

    	ignored = fread(&byte1, 1, 1, fd);
    	ignored = fread(&byte2, 1, 1, fd);
	if (ignored == 0) ;
    	value = (byte1 << 8) + byte2;
    	memory[addr] = value;
    }

    fclose(fd);
}

//*****************************************************************************
// Save memory to a 'core' file.
// Use this to implement the imlac persistent core memory feature.
//*****************************************************************************

void
mem_save_core(char *filename)
{
    vlog("mem_save_core: filename='%s'", filename);

    FILE *fd = fopen(filename, "wb");
    WORD addr;

    /* write memory in bytes, get around endian problems */
    for (addr = 0; addr < MEM_SIZE; ++addr)
    {
        unsigned char byte;
        WORD value;

    	value = memory[addr];
    	byte = (value >> 8) & 0xff;
    	fwrite(&byte, 1, 1, fd);
    	byte = value & 0xff;
    	fwrite(&byte, 1, 1, fd);
    }

    fclose(fd);
}
