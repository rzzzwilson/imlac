; special TTY bootstrap loader from loading.pdfA
nxtadr	equ	010	; pointer to next address to save in
XYZZY	equ	020	; 
nybcnt	equ	021	; nybble counter
wdacum	equ	022	; place to save AC
			;
	org	040	;
			;
	rcf		; clear TTY flag
	lwc	0101	; set address pointer
	dac	nxtadr	;     for loaded code store
	dac	XYZZY	; ?
L044	lwc	4	; 
	dac	nybcnt	;
	cal		;
	dac	wdacum	;
nxtnyb	cal		; clear AC and Link
	rsf		; skip if TTY char available
	jmp	.-1	;
	rrc		; "or" TTY char to AC and clear TTY flag
	dac	023	; save AC in 023
	and	L075	; get low 3 bits of high nybble
	sam	L076	; skip if 0100
	jmp	nxtnyb	;     jump if not same
	lac	023	; restore AC
	and	L077	; keep low 4 bits of AC
	xam	wdacum	; exchange AC wdacum
	ral	3	; rotate AC+L 4 places left
	ral	1	;
	ior	wdacum	; add in the latest 4 bits
	dac	wdacum	; save in wdacum
	isz	nybcnt	; got 4 nybbles yet?
	jmp	nxtnyb	;     jump if not
	dac	*nxtadr	; save word at next address
	isz	XYZZY	; finished?
	jmp	L044	;     jump if not
	jmp	*0	;
			;
L075	data	0160	;
L076	data	0100	;
L077	data	017	;
			;
	end		;
