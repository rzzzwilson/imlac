//*****************************************************************************
// Implement the Imlac main CPU.
//*****************************************************************************

#include "imlac.h"
#include "cpu.h"
#include "dcpu.h"
#include "memory.h"
#include "kb.h"
#include "ptrptp.h"
#include "ttyin.h"
#include "ttyout.h"
#include "trace.h"
#include "log.h"


//*****
// Emulated registers, state, memory, etc.
//*****

WORD        r_AC;
WORD        r_L;
WORD        r_PC;
WORD        r_DS;           // data switches
static WORD Prev_r_PC;      // the previous PC value

//*****
// Imlac state stuff.
//*****

// true if main processor is running
static bool cpu_on; 

// 40Hz sync stuff
static bool Sync40HzOn = false;
static long sync_cycle_count = 0L;

#define SYNC_CYCLES_40HZ        40
#define PTR_CYCLES_PER_CHAR     (int)(CPU_HERTZ / SYNC_CYCLES_40HZ)


//*****************************************************************************
// Description : Tick function for the 40Hz sync flip-flop.
//  Parameters : cycles  the number of CPU cycles that have elapsed
//     Returns : 
//    Comments : The 40Hz sync flip-flop is cleared by the SCF instruction.
//             : Roughly 1/40 sec after that the FF is set again.
//*****************************************************************************
void
cpu_sync_tick(long cycles)
{
    sync_cycle_count -= cycles;
    if (sync_cycle_count <= 0L)
    {
        Sync40HzOn = true;
        sync_cycle_count += PTR_CYCLES_PER_CHAR;
    }
}

static void
cpu_sync_clear(void)
{
    sync_cycle_count = PTR_CYCLES_PER_CHAR;
    Sync40HzOn = false;
}

//*****************************************************************************
// Description : Calculate the effective address.
//  Parameters : address   - the memory address
//             : indirect  - 'true' if the access is indirect
//     Returns : The effective address.
//    Comments : Also handle increments if AUTOINC memory location.
//*****************************************************************************
static WORD
cpu_eff_address(WORD address, bool indirect)
{
    // the Imlac can get into infinite defer loops, and so can we!
    while (indirect)
    {
        if (ISAUTOINC(address))
            mem_put(address, false, mem_get(address, false) + 1);
        address = mem_get(address, false);
        indirect = (address & 0100000);
    }

    return address;
}

//*****************************************************************************
// Description : Function to start the main CPU.
//  Parameters :
//     Returns :
//    Comments :
//*****************************************************************************
void
cpu_start(void)
{
    cpu_on = true;
}

//*****************************************************************************
// Description : Function to stop the main CPU.
//  Parameters :
//     Returns :
//    Comments :
//*****************************************************************************
void
cpu_stop(void)
{
    cpu_on = false;
}

//*****************************************************************************
// Description : Functions to get various registers and states.
//  Parameters :
//     Returns :
//    Comments :
//*****************************************************************************
WORD
cpu_get_AC(void)
{
    return r_AC;
}

WORD
cpu_get_L(void)
{
    return r_L;
}

WORD
cpu_get_PC(void)
{
    return r_PC;
}

WORD
cpu_get_DS(void)
{
    return r_DS;
}

WORD
cpu_get_prev_PC(void)
{
    return Prev_r_PC;
}

bool
cpu_running(void)
{
    return cpu_on;
}

void
cpu_abort(void)
{
    cpu_on = false;
}

void
cpu_set_AC(WORD new_ac)
{
    r_AC = new_ac;
}

void
cpu_set_L(WORD new_l)
{
    r_L = (new_l && 1);
}

void
cpu_set_PC(WORD new_pc)
{
    r_PC = new_pc;
}

void
cpu_set_DS(WORD new_ds)
{
    r_DS = new_ds;
}


void
memdump(WORD addr, int count)
{
    for (int i = 0; i < count; ++i)
    {
        vlog("memdump: [%07o]=%07o", addr, mem_get(addr, false));
        ++addr;
    }
}

//*****************************************************************************
// Description : Function to handle an unrecognized instruction.
//  Parameters :
//     Returns :
//    Comments :
//*****************************************************************************
static void
illegal(void)
{
    WORD oldPC = Prev_r_PC & MEMMASK;

    memdump(oldPC - 8, 16);
    memdump(03700, 8);

    mem_save_core("dump.core");

    error("INTERNAL ERROR: "
          "unexpected main processor opcode %06.6o at address %06.6o",
          mem_get(oldPC, false), oldPC);
}

//*****************************************************************************
// Description : Emulate the IMLAC LAW/LWC instructions.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Load AC with immediate value.
//             : The "indirect" bit here selects between the LWC and LAW opcodes.
//*****************************************************************************
static int
cpu_LAW_LWC(bool indirect, WORD address)
{
    if (indirect)
    {
        // LWC
        r_AC = (~address + 1) & WORD_MASK;

        trace_cpu("LWC     %5.5o", address);
    }
    else
    {
        // LAW
        r_AC = address;

        trace_cpu("LAW     %5.5o", address);
    }

    return 1;
}

//*****************************************************************************
// Description : Emulate the JMP instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : PC set to new address.
//*****************************************************************************
static int
cpu_JMP(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);

    r_PC = new_address & MEMMASK;

    if (indirect)
        trace_cpu("JMP     *%5.5o", address);
    else
        trace_cpu("JMP     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the DAC instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Deposit AC in MEM.
//*****************************************************************************
static int
cpu_DAC(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);

    mem_put(new_address, false, r_AC);

    if (indirect)
    {
        trace_cpu("DAC     *%5.5o", address);
    }
    else
    {
        trace_cpu("DAC     %5.5o", address);
    }

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC XAM instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Exchange AC with MEM.
//****************************************************************************
static int
cpu_XAM(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    WORD tmp = mem_get(new_address, false);

    mem_put(new_address, false, r_AC);
    r_AC = tmp;

    if (indirect)
        trace_cpu("XAM     *%5.5o", address);
    else
        trace_cpu("XAM     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the ISZ instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Increment MEM and skip if MEM == 0.
//*****************************************************************************
static int
cpu_ISZ(bool indirect, WORD address)
{
    WORD new_value;
    WORD new_address = cpu_eff_address(address, indirect);
    new_value = (mem_get(new_address, false) + 1) & WORD_MASK;
    mem_put(new_address, false, new_value);
    if (new_value == 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    if (indirect)
        trace_cpu("ISZ     *%5.5o", address);
    else
        trace_cpu("ISZ     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC JMS instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Store PC in MEM, jump to MEM + 1.
//*****************************************************************************
static int
cpu_JMS(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    mem_put(new_address, false, r_PC);
    r_PC = (++new_address) & MEMMASK;

    if (indirect)
        trace_cpu("JMS     *%5.5o", address);
    else
        trace_cpu("JMS     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC AND instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : AND MEM with AC.
//*****************************************************************************
static int
cpu_AND(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    r_AC &= mem_get(new_address, false);

    if (indirect)
        trace_cpu("AND     *%5.5o", address);
    else
        trace_cpu("AND     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOR instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Inclusive OR MEM with AC.
//*****************************************************************************
static int
cpu_IOR(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    r_AC |= mem_get(new_address, false);

    if (indirect)
        trace_cpu("IOR     *%5.5o", address);
    else
        trace_cpu("IOR     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC XOR instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : XOR AC and MEM.  LINK unchanged.
//*****************************************************************************
static int
cpu_XOR(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    r_AC ^= mem_get(new_address, false);

    if (indirect)
        trace_cpu("XOR     *%5.5o", address);
    else
        trace_cpu("XOR     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC LAC instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Load AC from MEM.
//*****************************************************************************
static int
cpu_LAC(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    r_AC = mem_get(new_address, false);

    if (indirect)
        trace_cpu("LAC     *%5.5o", address);
    else
        trace_cpu("LAC     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the ADD instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Add value at MEM to AC.
//*****************************************************************************
static int
cpu_ADD(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    r_AC += mem_get(new_address, false);
    if (r_AC & OVERFLOWMASK)
    {
        r_L ^= 1;
        r_AC &= WORD_MASK;
    }

    if (indirect)
        trace_cpu("ADD     *%5.5o", address);
    else
        trace_cpu("ADD     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC SUB instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Subtract MEM from AC.  LINK complemented if carry.
//*****************************************************************************
static int
cpu_SUB(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    WORD value = (~mem_get(new_address, false) + 1) & WORD_MASK;
    r_AC += value;
    if (r_AC & OVERFLOWMASK)
    {
        r_L = !r_L;
        r_AC &= WORD_MASK;
    }

    if (indirect)
        trace_cpu("SUB     *%5.5o", address);
    else
        trace_cpu("SUB     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Emulate the IMLAC SAM instruction.
//  Parameters : indirect - TRUE if address is indirect, FALSE if immediate
//             : address  - the memory address
//     Returns : The number of cycles consumed.
//    Comments : Skip if AC same as MEM.
//*****************************************************************************
//static int
int
cpu_SAM(bool indirect, WORD address)
{
    WORD new_address = cpu_eff_address(address, indirect);
    if (r_AC == mem_get(new_address, false))
        r_PC = (r_PC + 1) & WORD_MASK;

    if (indirect)
        trace_cpu("SAM     *%5.5o", address);
    else
        trace_cpu("SAM     %5.5o", address);

    return (indirect) ? 3 : 2;
}

//*****************************************************************************
// Description : Decode the 'microcode' instructions.
//  Parameters : instruction - the complete instruction word
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
microcode(WORD instruction)
{
    char trace_cpu_msg[20];     // little buffer for opcode

    // the NOP isn't decoded, but it doesn't do anything anyway
 
    /* T1 */
    if (instruction & 001)      // CLA
    {
        r_AC = 0;
    }
    if (instruction & 010)      // CLL
    {
        r_L = 0;
    }

    /* T2 */
    if (instruction & 002)      // CMA
    {
        r_AC = (~r_AC) & WORD_MASK;
    }
    if (instruction & 020)      // CML
    {
        r_L = (~r_L) & 01;
    }

    /* T3 */
    if (instruction & 004)      // IAC
    {
        if (++r_AC & OVERFLOWMASK)
            r_L = (~r_L) & 01;
        r_AC &= WORD_MASK;
    }
    if (instruction & 040)      // ODA
    {
        r_AC |= r_DS;
    }

    /* do some sort of trace_cpu */
    strcpy(trace_cpu_msg, "");
    switch (instruction)
    {
        case 0100000: strcat(trace_cpu_msg, "NOP"); break;
        case 0100001: strcat(trace_cpu_msg, "CLA"); break;
        case 0100002: strcat(trace_cpu_msg, "CMA"); break;
        case 0100003: strcat(trace_cpu_msg, "STA"); break;
        case 0100004: strcat(trace_cpu_msg, "IAC"); break;
        case 0100005: strcat(trace_cpu_msg, "COA"); break;
        case 0100006: strcat(trace_cpu_msg, "CIA"); break;
        case 0100010: strcat(trace_cpu_msg, "CLL"); break;
        case 0100011: strcat(trace_cpu_msg, "CAL"); break;
        case 0100020: strcat(trace_cpu_msg, "CML"); break;
        case 0100030: strcat(trace_cpu_msg, "STL"); break;
        case 0100040: strcat(trace_cpu_msg, "ODA"); break;
        case 0100041: strcat(trace_cpu_msg, "LDA"); break;
    }

    if ((instruction & 0100000) == 0)
    {
        /* bit 0 is clear, it's HLT */
        cpu_stop();

        if (trace_cpu_msg[0] != 0)
            strcat(trace_cpu_msg, "+HLT");
        else
            strcat(trace_cpu_msg, "HLT");
    }

    strcat(trace_cpu_msg, "  ");
    trace_cpu(trace_cpu_msg);

    return 1;
}

//*****************************************************************************
// Description : Emulate the DSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if display is ON.
//*****************************************************************************
static int
cpu_DSF(void)
{
    if (dcpu_running())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("DSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC HRB instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Read PTR value into AC.
//             : If PTR motor off return 0.
//             : If PTR motor on return byte from file.
//*****************************************************************************
static int
cpu_HRB(void)
{
    r_AC |= ptr_read();

    trace_cpu("HRB");

    return 1;
}

//*****************************************************************************
// Description : Emulate the DSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if display is OFF.
//*****************************************************************************
static int
cpu_DSN(void)
{
    if (!dcpu_running())
    {
        r_PC = (r_PC + 1) & WORD_MASK;
    }

    trace_cpu("DSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC HSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if PTR has data.
//    Comments : No data until cycle counter >= 'char ready' number.
//*****************************************************************************
static int
cpu_HSF(void)
{
    if (ptr_has_data())
    {
        r_PC = (r_PC + 1) & WORD_MASK;
    }

    trace_cpu("HSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC HSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if PTR has no data.
//             : There is no data until cycle counter >= 'char ready' number.
//*****************************************************************************
static int
cpu_HSN(void)
{
    if (!ptr_has_data())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("HSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC KCF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Clear the keyboard flag.
//*****************************************************************************
static int
cpu_KCF(void)
{
    kb_clear_flag();

    trace_cpu("KCF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC KRB instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Read a character from the keyboard into bits 5-15 of AC.
//*****************************************************************************
static int
cpu_KRB(void)
{
    r_AC |= kb_get_char();

    trace_cpu("KRB");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC KRC instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Combine the KCF and KRB instruction: Read keyboard and clear flag.
//*****************************************************************************
static int
cpu_KRC(void)
{
    r_AC |= kb_get_char();
    kb_clear_flag();

    trace_cpu("KRC");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC KSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if keyboard char available.
//*****************************************************************************
static int
cpu_KSF(void)
{
    if (kb_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("KSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC KSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if no keyboard char available.
//*****************************************************************************
static int
cpu_KSN(void)
{
    if (!kb_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("KSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC PUN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Punch AC low byte to tape.
//*****************************************************************************
static int
cpu_PUN(void)
{
    ptp_punch(r_AC & 0xff);

    trace_cpu("PUN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC PSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if PTP ready.
//*****************************************************************************
static int
cpu_PSF(void)
{
    if (ptp_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("PSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RAL instruction.
//  Parameters : shift - the number of bits to shift by [1,3]
//     Returns : The number of cycles consumed.`
//    Comments : Rotate AC+L left 'shift' bits.
//*****************************************************************************
static int
cpu_RAL(int shift)
{
    int save_shift = shift;

    while (shift-- > 0)
    {
        WORD oldlink = r_L;
        r_L = (r_AC >> 15) & LOWBITMASK;
        r_AC = ((r_AC << 1) + oldlink) & WORD_MASK;
    }

    trace_cpu("RAL     %d", save_shift);

    return 1;
}

//*****************************************************************************
// Description : Emulate the RAR instruction.
//  Parameters : shift - number of bits to rotate [1,3]
//     Returns : The number of cycles consumed.
//    Comments : Rotate right AC+L 'shift' bits.
//*****************************************************************************
static int
cpu_RAR(int shift)
{
    int save_shift = shift;

    while (shift-- > 0)
    {
        WORD oldlink = r_L;
        r_L = r_AC & LOWBITMASK;
        r_AC = ((r_AC >> 1) | (oldlink << 15)) & WORD_MASK;
    }

    trace_cpu("RAR     %d", save_shift);

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RCF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Clear the TTY buffer flag.
//*****************************************************************************
static int
cpu_RCF(void)
{
    ttyin_clear_flag();

    trace_cpu("RCF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RRB instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : OR a character from the TTY into bits 7-15 of AC.
//*****************************************************************************
static int
cpu_RRB(void)
{
    BYTE byte = ttyin_get_char();
    WORD new_r_AC = r_AC | byte;
    r_AC = new_r_AC;
//    r_AC |= ttyin_get_char();

    trace_cpu("RRB");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RRC instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Or a character from the TTY into bits 7-15 of AC
//             : and clear buffer flag.
//*****************************************************************************
static int
cpu_RRC(void)
{
    BYTE byte = ttyin_get_char();
    WORD new_r_AC = r_AC | byte;
    r_AC = new_r_AC;
//    r_AC |= ttyin_get_char();
//
    ttyin_clear_flag();

    trace_cpu("RRC");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if TTY char available.
//*****************************************************************************
static int
cpu_RSF(void)
{
    if (ttyin_ready())
    {
        r_PC = (r_PC + 1) & WORD_MASK;
    }

    trace_cpu("RSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC RSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if no TTY char available.
//*****************************************************************************
static int
cpu_RSN(void)
{
    if (!ttyin_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("RSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC SAL instruction.
//  Parameters : shift - the number of bits to shift by
//     Returns : The number of cycles consumed.
//    Comments : Shift AC left n places.  LINK unchanged.
//*****************************************************************************
static int
cpu_SAL(int shift)
{
    WORD oldbit0 = r_AC & HIGHBITMASK;
    int save_shift = shift;

    while (shift-- > 0)
    {
        r_AC = ((r_AC << 1) & BITS_2_15) | oldbit0;
    }

    trace_cpu("SAL     %d", save_shift);

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC SAR instruction.
//  Parameters : shift - the number of bits to shift by
//     Returns : The number of cycles consumed.
//    Comments : Shift AC right n places.
//*****************************************************************************
static int
cpu_SAR(int shift)
{
    WORD oldbit0 = r_AC & HIGHBITMASK;
    int save_shift = shift;

    while (shift-- > 0)
    {
        r_AC = (r_AC >> 1) | oldbit0;
    }

    trace_cpu("SAR     %d", save_shift);

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC SSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if 40Hz sync flip-flop is set.
//*****************************************************************************
static int
cpu_SSF(void)
{
    if (Sync40HzOn)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("SSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC SSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if 40Hz sync flip-flop is NOT set.
//*****************************************************************************
static int
cpu_SSN(void)
{
    if (!Sync40HzOn)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("SSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC TCF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Reset the TTY "output done" flag.
//*****************************************************************************
static int
cpu_TCF(void)
{
    ttyout_clear_flag();

    trace_cpu("TCF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC TPC instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Transmit char in AC and clear TTY ready flag
//*****************************************************************************
static int
cpu_TPC(void)
{
    ttyout_send(r_AC & 0xff);
    ttyout_clear_flag();

    trace_cpu("TPC");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC TPR instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Send low byte in AC to TTY output.
//*****************************************************************************
static int
cpu_TPR(void)
{
    ttyout_send(r_AC & 0xff);

    trace_cpu("TPR");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC TSF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if TTY done sending
//*****************************************************************************
static int
cpu_TSF(void)
{
    if (ttyout_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("TSF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC TSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if TTY not done sending
//*****************************************************************************
static int
cpu_TSN(void)
{
    if (!ttyout_ready())
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("TSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the ASZ instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if AC == 0.
//*****************************************************************************
static int
cpu_ASZ(void)
{
    if (r_AC == 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("ASZ");

    return 1;
}

//*****************************************************************************
// Description : Emulate the ASN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if AC != 0.
//*****************************************************************************
static int
cpu_ASN(void)
{
    if (r_AC != 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("ASN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC ASP instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if AC is positive.
//*****************************************************************************
static int
cpu_ASP(void)
{
    if ((r_AC & HIGHBITMASK) == 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("ASP");

    return 1;
}

//*****************************************************************************
// Description : Emulate the LSZ instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if LINK is zero.
//*****************************************************************************
static int
cpu_LSZ(void)
{
    if (r_L == 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("LSZ");

    return 1;
}

//*****************************************************************************
// Description : Emulate the LSN instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if LINK isn't zero.
//*****************************************************************************
static int
cpu_LSN(void)
{
    if (r_L != 0)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("LSN");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC ASM instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Skip if AC is negative.
//*****************************************************************************
static int
cpu_ASM(void)
{
    if (r_AC & HIGHBITMASK)
        r_PC = (r_PC + 1) & WORD_MASK;

    trace_cpu("ASM");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC DLA instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Load display CPU with a new PC.
//*****************************************************************************
static int
cpu_DLA(void)
{
    dcpu_set_PC(r_AC);

    trace_cpu("DLA");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC CTB instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Load display CPU with a new PC.
//*****************************************************************************
static int
cpu_CTB(void)
{
    // TODO - load DPC

    trace_cpu("CTB");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC DOF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Turn the display processor off.
//*****************************************************************************
static int
cpu_DOF(void)
{
    dcpu_stop();

    trace_cpu("DOF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC DON instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Turn the display processor on.
//*****************************************************************************
static int
cpu_DON(void)
{
    dcpu_start();

    trace_cpu("DON");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC HOF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Turn the PTR off.
//*****************************************************************************
static int
cpu_HOF(void)
{
    ptr_stop();

    trace_cpu("HOF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC HON instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Turn the PTR on.
//*****************************************************************************
static int
cpu_HON(void)
{
    ptr_start();

    trace_cpu("HON");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC STB instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_STB(void)
{
    // TODO - implement!

    trace_cpu("STB");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC SCF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Clear the 40Hz "sync" flag.
//*****************************************************************************
static int
cpu_SCF(void)
{
    Sync40HzOn = false;
    cpu_sync_clear();

    trace_cpu("SCF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOS instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOS(void)
{
    // TODO - implement!

    trace_cpu("IOS");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOT101 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT101(void)
{
    // TODO - implement!

    trace_cpu("IOT101");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOT111 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT111(void)
{
    // TODO - implement!

    trace_cpu("IOT111");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC cpu_IOT131 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT131(void)
{
    // TODO - implement!

    trace_cpu("IOT131");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC cpu_IOT132 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT132(void)
{
    // TODO - implement!

    trace_cpu("IOT132");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOT134 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT134(void)
{
    // TODO - implement!

    trace_cpu("IOT134");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOT141 instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOT141(void)
{
    // TODO - implement!

    trace_cpu("IOT141");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC IOF instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_IOF(void)
{
    // TODO - implement!

    trace_cpu("IOF");

    return 1;
}

//*****************************************************************************
// Description : Emulate the IMLAC ION instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
cpu_ION(void)
{
    // TODO - implement!

    trace_cpu("ION");

    return 1;
}

//*****************************************************************************
// Description : Further decode the initial '02' opcode instruction.
//  Parameters : instruction - the complete instruction word
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
page02(WORD instruction)
{
    switch (instruction)
    {
        case 0002001: return cpu_ASZ();
        case 0102001: return cpu_ASN();
        case 0002002: return cpu_ASP();
        case 0102002: return cpu_ASM();
        case 0002004: return cpu_LSZ();
        case 0102004: return cpu_LSN();
        case 0002010: return cpu_DSF();
        case 0102010: return cpu_DSN();
        case 0002020: return cpu_KSF();
        case 0102020: return cpu_KSN();
        case 0002040: return cpu_RSF();
        case 0102040: return cpu_RSN();
        case 0002100: return cpu_TSF();
        case 0102100: return cpu_TSN();
        case 0002200: return cpu_SSF();
        case 0102200: return cpu_SSN();
        case 0002400: return cpu_HSF();
        case 0102400: return cpu_HSN();
        default:      illegal();
    }

    return 0;    /* CAN'T REACH */
}

//*****************************************************************************
// Description : Further decode the initial '00' opcode instruction.
//  Parameters : instruction - the complete instruction word
//     Returns : The number of cycles consumed.
//    Comments :
//*****************************************************************************
static int
page00(WORD instruction)
{
    // Pick out microcode or page 2 instructions.
    if ((instruction & 0077700) == 000000)
        return microcode(instruction);

    if ((instruction & 0077000) == 002000)
        return page02(instruction);

    // Decode a page 00 instruction
    switch (instruction)
    {
        case 001003: return cpu_DLA();
        case 001011: return cpu_CTB();
        case 001012: return cpu_DOF();
        case 001021: return cpu_KRB();
        case 001022: return cpu_KCF();
        case 001023: return cpu_KRC();
        case 001031: return cpu_RRB();
        case 001032: return cpu_RCF();
        case 001033: return cpu_RRC();
        case 001041: return cpu_TPR();
        case 001042: return cpu_TCF();
        case 001043: return cpu_TPC();
        case 001051: return cpu_HRB();
        case 001052: return cpu_HOF();
        case 001061: return cpu_HON();
        case 001062: return cpu_STB();
        case 001071: return cpu_SCF();
        case 001072: return cpu_IOS();
        case 001101: return cpu_IOT101();
        case 001111: return cpu_IOT111();
        case 001131: return cpu_IOT131();
        case 001132: return cpu_IOT132();
        case 001134: return cpu_IOT134();
        case 001141: return cpu_IOT141();
        case 001161: return cpu_IOF();
        case 001162: return cpu_ION();
        case 001271: return cpu_PUN();
        case 001274: return cpu_PSF();
        case 003001: return cpu_RAL(1);
        case 003002: return cpu_RAL(2);
        case 003003: return cpu_RAL(3);
        case 003021: return cpu_RAR(1);
        case 003022: return cpu_RAR(2);
        case 003023: return cpu_RAR(3);
        case 003041: return cpu_SAL(1);
        case 003042: return cpu_SAL(2);
        case 003043: return cpu_SAL(3);
        case 003061: return cpu_SAR(1);
        case 003062: return cpu_SAR(2);
        case 003063: return cpu_SAR(3);
        case 003100: return cpu_DON();
        default:     illegal();
    }

    return 0;    /* CAN'T REACH */
}

//*****************************************************************************
// Description : Function to execute one main processor instruction.
//  Parameters :
//     Returns : The number of cycles consumed.
//    Comments : Perform initial decode of 5 bit opcode and either call
//             : appropriate emulating function or call further decode function.
//*****************************************************************************
int
cpu_execute_one(void)
{
    WORD instruction;
    bool indirect;
    WORD opcode;
    WORD address;

    // If main processor not running, return immediately.
    if (!cpu_on)
        return 0;

#ifdef JUNK
    // If interrupt pending, force JMS 0.
    if (InterruptsEnabled && (InterruptWait <= 0) && InterruptsPending)
    {
        InterruptsEnabled = false;
        cpu_JMS(false, 0);
        return 0;
    }
#endif

    // Fetch the instruction.  Split into initial opcode and address.
    Prev_r_PC = r_PC;
    instruction = mem_get(r_PC, false);
    ++r_PC;
    r_PC = r_PC & MEMMASK;

    indirect = (bool) (instruction & 0100000);    /* high bit set? */
    opcode = (instruction >> 11) & 017;           /* bits 1-4 */
    address = instruction & 03777;                /* low 11 bits */

    // Now decode the opcode.
    switch (opcode)
    {
        case 000: return page00(instruction);
        case 001: return cpu_LAW_LWC(indirect, address);
        case 002: return cpu_JMP(indirect, address);
        /* case 003: illegal(); */
        case 004: return cpu_DAC(indirect, address);
        case 005: return cpu_XAM(indirect, address);
        case 006: return cpu_ISZ(indirect, address);
        case 007: return cpu_JMS(indirect, address);
        /* case 010: illegal(); */
        case 011: return cpu_AND(indirect, address);
        case 012: return cpu_IOR(indirect, address);
        case 013: return cpu_XOR(indirect, address);
        case 014: return cpu_LAC(indirect, address);
        case 015: return cpu_ADD(indirect, address);
        case 016: return cpu_SUB(indirect, address);
        case 017: return cpu_SAM(indirect, address);
        default:  illegal();
    }

    return 0;       /* CAN'T REACH */
}
