#!/usr/bin/env python

"""
A small program to create Imlac test PTR files.  Data punched will
be 8 bit values starting at 1 and incrementing for each following byte.

Usage: write_ptr_tape.py [<options>] <filename>

where <options>  is one or more of:
          -h           print this help and stop
          -d <string>  write <string> instead of default 8-bit values
          -l           print a leader of zeros
          -n N         create N bytes of data, default is 10
                       (ignored if '-d' option used)
          -t           print a trailer of zeros
      <filename> name of the PTP file to produce
"""

import sys
import os.path
import getopt


LeadTrailSize = 10


def usage(msg=None):
    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)

def create_ptp(filename, leader, trailer, data):
    if leader or trailer:
        zeros = [0] * LeadTrailSize

    with open(filename, 'wb') as fd:
        if leader:
            fd.write(bytearray(zeros))
        fd.write(bytearray(data))
        if trailer:
            fd.write(bytearray(zeros))

def main(argv):
    try:
        opts, args = getopt.gnu_getopt(argv, "hd:ln:t",
                                       ["help", "data=", "leader",
                                           "number=", "trailer"])
    except getopt.GetoptError:
        usage('Bad option')
        return 10

    # set default values
    leader = False
    trailer = False
    numbytes = 10
    str_data = None

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            return 0
        elif opt in ("-d", "--data"):
            str_data = arg
        elif opt in ("-l", "--leader"):
            leader = True
        if opt in ("-t", "--trailer"):
            trailer = True
        if opt in ("-n", "--number"):
            try:
                numbytes = int(arg)
            except ValueError:
                usage("Number of bytes must be an integer.")
                return 1
            if numbytes < 1:
                usage("Number of bytes must be an integer greater than 0.")
                return 1

    if len(args) != 2:
        usage()
        return 10

    # check input file actually exists
    filename = args[1]
    if os.path.exists(filename):
        print(f"Sorry, file '{filename}' already exists, won't overwrite.")
        return 10

    # create data that will be punched
    if str_data is None:
        data = list(range(1, numbytes + 1))
    else:
        data = [ord(ch) for ch in str_data]

    # OK< do it
    create_ptp(filename, leader, trailer, data)


try:
    sys.exit(main(sys.argv))
except KeyboardInterrupt:
    # handle ^C nicely
    print()
