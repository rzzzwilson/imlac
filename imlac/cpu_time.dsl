# uncomment line below to stop on first error
onerror abort

######
# Loops to exercise instruction timing
######
setmem 0100 [LWC 0400|DAC 0200|ISZ 0200|JMP 0102|NOP]; run 0100 0104
    checkreg ac 0177400; checkreg pc 0104; checkmem 0200 0
setmem 0100 [LWC 0400|DAC 0200|LAW 0|ISZ 0200|JMP 0102|NOP]; run 0100 0105
    checkreg ac 0;       checkreg pc 0105; checkmem 0200 0
