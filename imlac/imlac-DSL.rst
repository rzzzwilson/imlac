Testing DSL
===========

When testing the CPU it was decided that a DSL (domain specific language)
would be a great way to make testing simpler rather than the one-off bits
of code used previously.  This made writing test cases faster and more complete.

DSL Statements File
-------------------

The test code accepts the name of a file containing DSL statements for testing.

The file contains a series of DSL test suites, each on a single line, with the
following exceptions:

* A line with a '#' character in column 1 is a comment and is ignored
* A line that starts with whitespace (tab or space) is a continuation of the previous line
* Empty lines are ignored

A suite will usually contain more than one DSL statement.  A ';' character
ends each statement.  For example, a testing file might look like:

::

    # a comment line, and the next line will be ignored as it's empty

    SETREG AC 1; SETMEM 0100 [HLT];
        RUN; CHECKCYCLES 2

The above file is equivalent to:

::

    SETREG AC 1; SETMEM 0100 [HLT]; RUN; CHECKCYCLES 2

Note that numeric values may be entered as octal numbers (leading zero) or as
decimal.

Also note that when joining continued lines the first line is assumed to have a
terminal ';' character.  There is no need to add it, though it doesn't hurt if
you do.  For example, these two test files are identical:

::

    SETREG AC 1; SETMEM 0100 [HLT]; RUN
        CHECKCYCLES 2

::

    SETREG AC 1; SETMEM 0100 [HLT]; RUN; CHECKCYCLES 2

Initial Conditions
------------------

Before each test suite is executed the following may be assumed:

* All registers contain 0
* All memory locations contain 0

Post Condition Tests
--------------------

After a test suite has finished the following are checked:

* All unchecked registers still contain 0
* All unchecked memory locations still contain 0

The *unchecked* condition above means that any register/memory that is checked
in the execution of the suite is NOT checked as part of the post condition
tests.

Note that using either of **allreg** or **allmem** statements changes the post
condition checks for registers or memory.

DSL statements
--------------

setreg <name> <value>
---------------------

Sets the contents of register *<name>* to *<value>*.

*<name>* may be one of 'PC', 'AC', 'L' or 'DS'.

*<value>* is a numeric value that is placed into the named register.
If the *<value>* is wider than the register allows an error is raised.

Examples:

::

    setreg AC 123
    setreg PC 0100

setmem <address> <value>
------------------------

Sets the contents of the memory location at *<address>* to *<value>*.

*<address>* must be in the valid address range.

*<value>* may be a numeric value (octal or decimal) or may be of the form:

::

    [<asm>|<asm>|...]

This assembles one or more statements between the '[ ]' and places the
opcodes at addresses starting at *<address>*.  The assembler instructions are
delimited by the '|' character.  The code to be assembled is assumed to be
opcode+fields only and may not have any labels.

Examples:

::

    setmem 010 5
    setmem 0100 [LAW 4]
    setmem 0100 [LWC 1|HLT]

allmem <value>
--------------

Sets all memory locations to *<value>*.

Note that this also changes the memory post condition test to check that
memory contain *<value>*.

Examples:

::

    allmem 10

bootrom <type>
--------------

Sets the boot ROM address range to contain the code for the specified
bootloader.

*<type>* may be one of **PTR** or **TTY**.

The memory contents of address range 040 to 077 are set to the values
for the **PTR** or **TTY** bootstrap loaders.  In addition, the ROM write
state is set to **readonly** as if actual ROM was switched into place.
The user may decide to set the address range writable again after this
statement.

Examples:

::

    bootrom ptr; romwrite true

romwrite <bool>
---------------

Sets whether memory in the ROM address range (040 to 077) is writeable or not.

*<bool>* may be either **true** or **false**.  **true** means the address range
is writeable.

Examples:

::

    romwrite false; setmem 045 0

The above example would cause an error in the test suite as address 045 is not
writable.

mount <device> <filename>
---------------------

Mounts a file on a *<device>*.  The *<filename>* specifies the file to mount.
If the device is used for input then *<filename>* must exist.

Examples:

::

    # papertape boot loader should HLT when tape loaded
    boot ptr; mount ptr test_file.ptr; setreg pc 040; rununtil 0

dismount <device>
---------------------

Dismount the file that is mounted on *<device>*.

It is an error if there is no mounted file.

Examples:

::

    mount ptr test_file.ptr; run; dismount ptr

device <device> <state>
-----------------------

Set the *<device>* to the state *<state>*.

The *<device>* will be one of "PTP" or "PTR".  The *<state>* value
will be one of "ON" or "OFF".

run [<start_address>[ <stop_address>]]
--------------------------------------

Run the emulator.  If the *<start_address>* is supplied execution
will commence at that address.  If the *<stop_address>* is supplied
execution will stop upon reaching that address.

Examples:

::

    run             # start execution at current PC value
    run 010         # set PC to 010 and then start executing
    run 010 020     # set PC to 010 and execute until PC is 020

checkcycles <number>
--------------------

Check that the number of emulated cycles in the previous *run* or *rununtil*
command is *<number>*.

Examples:

::

    setmem 0100 [HLT]; run; checkcycles 2

checkreg <name> <value>
-----------------------

Check that the *<name>* register contains *<value>*.

*<name>* may be one of **AC**, **L**, **PC** or **DS**.

If *<value>* contains more bits than the width of the checked register
then an error is raised.

Note that post condition tests are not run on the checked register.

Examples:

::

    setmem 0100 [LAW 1]; run; checkreg ac 1

checkmem <addr> <value>
-----------------------

Check that the memory location at *<address>* contains *<value>*.

If *<value>* contains more bits than the width of the memory location
then an error is raised.

Note that post condition tests are not run on the checked memory location.

Examples:

::

    setmem 0100 [LAW 1; DAC 0200]; run 2; checkmem 0200 1

checkcpu <state>
----------------

Check that the main CPU state is as desired.  *<state>* will be one of **ON**
or **OFF**.

Examples:

::

    setmem 0100 [HLT]; run; checkcpu off

checkdcpu <state>
----------------

Check that the display CPU state is as desired.  *<state>* will be one of **ON**
or **OFF**.

Examples:

::

    setmem 0100 [HLT]; run; checkdcpu off

checkfile <file1> <file2>
-------------------------

Check that the contents of two files are the same.  This is used in testing I/O
using the papertape reader/punch or the TTY devices.

Examples:

::

    setreg ac 0; mount ptp test.ptp;
        setmem 0100 [LAW 0|HON|PSF|JMP 0102|PPC|IAC|JMP 0102];
        run 0100 0105; checkreg ac 0;
        run 0100 0105; checkreg ac 1;
        run 0100 0105; checkreg ac 2;
        checkfile test.ptp test.ptr

This test suite runs code that will punch a three byte file.  The **checkfile**
instruction checks that the punched file contains the correct data by comparing
it against a file containing the correct data.

dumpmem <file> <begin>,<end>
----------------------------

Dump a part of memory to a file.  The dump file will be created if it doesn't
exist, or overwritten if it does.

The address values **begin** and **end** must be specified, within valid memory
and **begin** must be less than **end**.

The file written by this command is a pure binary file.  This DSL command:

::

    dumpmem example.out 0100,0120

Would produce a binary file containing:

::

    $ fdump example.out
    000100  100000 010104 000000 000000 004111 000000 000000 000000 |...D.....I......|


cmpmem <file>
-------------

This DSL command reads a memory dump file produced by **dumpmem** and compares
memory with the contents of the file.  An error is raised if memory doesn't
agree with the file.

Example:

::

    cmpmem memdump.dmp

trace <range>[:<range>:...]
--------------------------

This command instructs the test engine to trace CPU execution within the
range(s) specified.  More than one range may be specified.

For example, the following **trace** command would tell *test_CPU.py* to trace
between the addresses 040 and 077:

::

    trace 040,077

You can specify more than one range:

::

    trace 040,077:0100,0110

onerror <action>
----------------

Determines what happens when an error is found.  If *<action>* is
*ignore* the DSL execution continues.  If *<action>* is "abort* the
DSL execution is terminated.

Initially *onerror abort* is assumed.

Example:

::

    onerror abort

abort <message> <linenum>
-------------------------

Terminates the DSL execution.  The *<message>* and *<linenum>* are displayed.

Example:

::

    abort "The framistan is not responding" 123

include <filename>
------------------

This command is textually replaced by the contents of the *<filename>*
file.

The *onerror* status just before the *include* is maintained after the
*include* command, no matter what the included code does.

Example:

::

    include test.dsl
