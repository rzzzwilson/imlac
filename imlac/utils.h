//*****************************************************************************
// Utility stuff for the IMLAC implementation.
//*****************************************************************************

#ifndef UTILS_H
#define UTILS_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int time_elapsed(struct timespec *start, struct timespec *stop);

#endif
