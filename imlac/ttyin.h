/*
 * Interface for the imlac TTY input device.
 */

#ifndef TTYIN_H
#define TTYIN_H

void ttyin_mount(char *fname);
void ttyin_dismount(void);
bool ttyin_ready(void);
void ttyin_clear_flag(void);
void ttyin_set_flag(void);
BYTE ttyin_get_char(void);
void ttyin_reset(void);
void ttyin_tick(int cycles);
void ttyin_vlog_state(char *prefix);

#endif
