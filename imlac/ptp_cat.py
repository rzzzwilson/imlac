#!/usr/bin/env python

"""
A small program to print an "output" PTP file.

Usage: ptp_cat.py [<options>] <filename>

where <options>  is one or more of:
          -h     print this help and stop
      <filename> name of the PTP file to produce
"""

import sys
import os.path
import getopt
import time


def usage(msg=None):
    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)


def cat_ptp(filename):
    with open(filename, 'rb') as fd:
        while True:
            b = fd.read(1)
            if not b:
                return

            print(str(b, 'UTF-8'), end='', flush=True)


def main(argv):
    try:
        opts, args = getopt.gnu_getopt(argv, "h", ['help'])
    except getopt.GetoptError:
        usage('Bad option')
        return 10

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            return 0

    if len(args) != 2:
        usage()
        return 10

    # check input file actually exists
    filename = args[1]
    if not os.path.exists(filename):
        print(f"Sorry, file '{filename}' doesn't exist.")
        return 10

    cat_ptp(filename)


try:
    sys.exit(main(sys.argv))
except KeyboardInterrupt:
    # handle ^C nicely
    print()
