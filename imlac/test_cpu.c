//*****************************************************************************
// Standalone program to test imlac CPU opcodes directly using a DSL.
// 
// Usage: test_cpu [<options>] <filename>
// 
// where <filename> is a file of test instructions and
//       <options> is one or more of
//           -h    prints this help and stops
//           -t    turn on trace for each test
//*****************************************************************************

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>

#include "imlac.h"
#include "cpu.h"
#include "dcpu.h"
#include "memory.h"
#include "ptrptp.h"
#include "ttyin.h"
#include "ttyout.h"
#include "bootstrap.h"
#include "hashtable.h"
#include "error.h"
#include "trace.h"
#include "log.h"
#include "utils.h"


// various constants, sizes, etc.
#define LIST2INT_SIZE       128     // max number of values in list2int()
#define ASMRESULT_SIZE      128     // max WORDS from assemble()

#define ASM_FILENAME        "_#ASM#_.asm"
#define LST_FILENAME        "_#ASM#_.lst"

#define DELIM  "****************************************************************"
#define DELIM2 "----------------------------------------------------------------"

#define LenDSLFilename        256            // max length of filename
                                    
// struct holding each test plus the file linenumber the test starts on
// the code keeps a dynamic array of pointers to each Test
typedef struct
{
    int linenum;
    char *test;
} Test;

WORD MemAllValues = 0;              // initial memory contents

WORD MemValues[MEMORY_SIZE];        // checked memory locations

WORD RegValues[4] = {0, 0, 0, 0};
#define Reg_AC_index    0
#define Reg_L_index     1
#define Reg_PC_index    2
#define Reg_DS_index    3

char ErrorMsg[512];                 // buffer for error messages

int UsedCycles = 0;                 // number of cycles executed in test
int UsedDCycles = 0;                // number of display cycles executed in test

bool AbortOnError = true;           // abort on error if this is true

// elapsed time structs
struct timespec time_start;
struct timespec time_stop;

// global values updated by "process_file()"
int num_executes = 0;
int num_errors = 0;

char DSLFilename[LenDSLFilename];   // current DSL filename

// buffer for an error string
char ErrorBuff[256];


// forward declarations
void process_file(char *filename);


//*****************************************************************************
// Description : Test two files to see if they are the same.
//  Parameters : filename1 - filename of the first file
//             : filename2 - filename of the second file
//     Returns : 'true' if files identical. else 'false'.
//    Comments : 
//*****************************************************************************
bool
files_same(char *filename1, char *filename2)
{
    FILE *fd1 = fopen(filename1, "rb");
    FILE *fd2 = fopen(filename2, "rb");
    bool result = true;         // assume files are the same
    unsigned char ch1;          // bytes read from each file
    unsigned char ch2;

    while (true)
    {
        // get next byte of file 1
        int read1 = fread(&ch1, 1, sizeof(ch1), fd1);
        int read2 = fread(&ch2, 1, sizeof(ch2), fd2);

        if (read1 == 0 && read2 == 0)
            break;

        if ((read1 != read2) || (ch1 != ch2))
        {
            result = false;
            break;
        }
    }

    fclose(fd1);
    fclose(fd2);

    return result;
}

//*****************************************************************************
// Description : Append string to existing string, update original variable.
//  Parameters : head - address of (char *), which MUST have been malloced
//             : tail - string to append to string at *head
//     Returns : void
//    Comments : Must pass first param as address of char* variable.
//*****************************************************************************
void
dyn_append(char **head, char *tail)
{
    int new_len = strlen(*head) + strlen(tail) + 1;
    char *new_dyn = malloc(new_len);

    strcpy(new_dyn, *head);
    strcat(new_dyn, tail);

    free(*head);
    *head = new_dyn;
}

//*****************************************************************************
// Description : A form of dyn_append() that accepts format string+data for tail.
//  Parameters : head     - address of (char *), which MUST have been malloced
//             : fmt, ... - printf-style varargs
//     Returns : void
//    Comments : Must pass first param as address of char* variable.
//*****************************************************************************
void
dyn_appendf(char **head, char *fmt, ...)
{
    va_list ap;
    char buff[1024];

    va_start(ap, fmt);
    vsprintf(buff, fmt, ap);
    va_end(ap);

    dyn_append(head, buff);
}

//*****************************************************************************
// Description : Convert a string to lowercase, in place.
//  Parameters : str - address of the string to convert to lowercase
//     Returns : 
//    Comments : 
//*****************************************************************************
void
lowercase(char *str)
{
    while (*str != '\0')
    {
        *str = tolower(*str);
        ++str;
    }
}

//*****************************************************************************
// Description : Convert a string to a boolean (true or false).
//  Parameters : str - address of string holding the boolean value
//     Returns : The boolean value.
//    Comments : 
//*****************************************************************************
bool
str2bool(char *str)
{
    if (STREQ(str, "true"))
        return true;
    if (STREQ(str, "false"))
        return false;
    error("Bad str2bool() string: '%s'", str);
    return true;    // CAN'T REACH
}

//*****************************************************************************
// Description : Convert an octal value string to an integer.
//  Parameters : str - address of string holding the value
//     Returns : The integer value.
//    Comments : Doesn't handle decimal strings, use str2int().
//*****************************************************************************
int
octstr2int(char *str)
{
    char *ch_ptr = str;

    int result = 0;

    while (*ch_ptr)
    {
        if (*ch_ptr < '0' || *ch_ptr > '7')
            error("Bad digit in octal string: %s\n", str);
        result = result * 8 + *ch_ptr - '0';
        ++ch_ptr;
    }

    return result;
}

//*****************************************************************************
// Description : Convert a value string to an integer.
//  Parameters : str - address of string holding the value
//     Returns : The integer value.
//    Comments : Handles both decimal and octal strings.
//*****************************************************************************
int
str2int(char *str)
{
    char *ch_ptr = str;

    // check first digit, if '0' then it's an octal string
    if (*ch_ptr == '0')
        return octstr2int(str);

    return atoi(str);
}

//*****************************************************************************
// Description : Delimit the first space-delimited field of a string.
//  Parameters : str - address of the string to process
//     Returns : The address of the first character of the first field.
//    Comments : The input string is destroyed.
//             : Any leading blank space is skipped.
//*****************************************************************************
char *
get_first_field(char *str)
{
    char *ch_ptr = str;     // scan pointer
    char *result;           // result char pointer

    // skip leading whitespace
    while (*ch_ptr && (*ch_ptr == ' ' || *ch_ptr == '\t'))
    {
        ++ch_ptr;
    }

    // if empty string point at NULL end
    if (*ch_ptr == '\0')
        return ch_ptr;

    result = ch_ptr;

    // skip over any non-whitespace
    while (*ch_ptr && (*ch_ptr != ' ' && *ch_ptr != '\t'))
    {
        ++ch_ptr;
    }

    *ch_ptr = '\0';
    return result;
}

//*****************************************************************************
// Description : Read the lines from a file and return them along with a count.
//  Parameters : filename  - string holding name of the file to read
//             : num_lines - address of integer to receive returned line count
//     Returns : The address of an array of line pointers.
//    Comments : Caller is responsible for freeing the memory allocated here.
//*****************************************************************************
char **
fread_lines(char *filename, int *num_lines)
{
    // variables for the input buffer and result array, etc
    int max_result = 10;        // max size of result array
    int next_result = 0;        // index of next free slot in array
    char *buff = NULL;          // input buffer, NULL means getline allocates
    size_t buff_size = 0;       // size of returned line
    int count = 0;              // line counter

    // open the file to read
    FILE *fd = fopen(filename, "r");

    if (fd == NULL)
        error("Error opening file %s", filename);

    char **result = malloc(max_result * sizeof(char *));   // the result array

    while (true)
    {
        int ret = getline(&buff, &buff_size, fd);
        if (ret < 0)
            break;
        ++count;

        result[next_result] = buff;
        ++next_result;

        if (next_result >= max_result)
        {
            // make result array bigger
            max_result = 2 * max_result;
            result = realloc(result, max_result * sizeof(char *));
        }

        buff = NULL;
    }

    // close the file
    fclose(fd);

    *num_lines = count;
    return result;
}

//*****************************************************************************
// Description : Generate one line of 'fdump' style output.
//  Parameters : buff   - address of a char buffer to fill
//             : s      - base address of array of words
//             : num_s  - number of words in 's' array
//             : offset - offset from start of dump
//     Returns : The address of the char buffer 's'.
//    Comments : 
//*****************************************************************************
char *
octword_line(char *buff, WORD *s, int num_s, WORD offset)
{
#define HEXLEN      56
#define CHARLEN     16

    // define temporary buffers for hex and char values of WORDs
    char hex_buff[HEXLEN + 1] = "";
    char *hex_ptr = hex_buff;
    char char_buff[CHARLEN + 1] = "";
    char *char_ptr = char_buff;

    // step through WORD data in 's', generating hex & char data as we go
    for (int i = 0; i < num_s; ++i)
    {
        WORD word_value = s[i];
        sprintf(hex_ptr, "%06o ", word_value);
        hex_ptr += strlen(hex_ptr);

        unsigned int hi = word_value >> 8;
        unsigned int lo = word_value & 0xff;

        if ((' ' <= hi) && (hi <= '~'))
            *char_ptr++ = hi;
        else
            *char_ptr++ = '.';
        *char_ptr = '\0';

        if ((' ' <= lo) && (lo <= '~'))
            *char_ptr++ = lo;
        else
            *char_ptr++ = '.';
        *char_ptr = '\0';
    }

    sprintf(buff, "%06o  %-*s |%s|", offset, HEXLEN, hex_buff, char_buff);
    return buff;
}

//*****************************************************************************
// Description : Print the register contents to the log.
//  Parameters : void
//     Returns : void
//    Comments : 
//*****************************************************************************
void
logreg(void)
{
    vlog("r_AC=%06o, r_L=%o, r_PC=%06o, r_DS=%06o\n", r_AC, r_L, r_PC, r_DS);
}

//*****************************************************************************
// Description : Print memory contents to the log.
//  Parameters : addr - maximum address to dump
//     Returns : void
//    Comments : Dumps from address 0 up to and including 'addr'.
//*****************************************************************************
void
logmem(WORD addr)
{
    char buff[256];
    int offset = 0;
    WORD mirror[8];
    int mptr = 0;

    vlog("logmem: dump from 0 to %07o:", addr);
    vlog("-------------------------------");

    while (mptr <= addr)
    {
        for (int i = 0; i < 8; ++i)
            mirror[i] = mem_get(mptr+i, false);

        char *line = octword_line(buff, mirror, 8, offset);

        vlog(line);
        offset += 8;
        mptr += 8;
    }
}

//*****************************************************************************
// Description : Display a "progress" indicator on the console.
//  Parameters : void
//     Returns : void
//    Comments : Shows a rotating bar.
//*****************************************************************************
void
show_progress(void)
{
    static int progress_count = 0;
    static char *ProgressChar = "|\\-/";

    printf("\r%c", ProgressChar[progress_count]);
    fflush(stdout);
    if (++progress_count >= strlen(ProgressChar))
        progress_count = 0;
}

//*****************************************************************************
// Description : Remove leading while space (space or tab) from a string.
//  Parameters : str - address of the string to process
//     Returns : The address of the string after any leading space.
//    Comments : Handles the case when the input string is ALL spaces/tabs.
//*****************************************************************************
char *
strip_lead_ws(char *str)
{
    // create pointer to first character in string
    char *ch_ptr = str;

    while (*ch_ptr && (*ch_ptr == ' ' || *ch_ptr == '\t'))
    {
        ++ch_ptr;
    }

    return ch_ptr;
}

//*****************************************************************************
// Description : Remove trailing while space (space or tab) from a string.
//  Parameters : str - address of the string to process
//     Returns : The unchanged address of the head of the string.
//    Comments : Handles the case when the input string is ALL spaces/tabs.
//*****************************************************************************
char *
strip_trail_ws(char *str)
{
    // create pointer to last character in string
    char *ch_ptr = str + strlen(str) - 1;

    while (ch_ptr >= str && (*ch_ptr == ' ' || *ch_ptr == '\t'))
        --ch_ptr;
    *(ch_ptr + 1) = '\0';

    return str;
}

//*****************************************************************************
// Description : Split an instruction string into fields.
//  Parameters : instruction - address of the instruction string to process
//             : opcode      - place to store pointer to opcode field
//             : param1      - place to store pointer to the first param field
//             : param2      - place to store pointer to the second param field
//     Returns : The number of fields found including the opcode.
//    Comments : Stores NULL if a field is not found.
//             : Fields after the first three (opcode + 2 params) are ignored.
//             : The code expects that leading/trailing whitespace has been
//             : removed from the 'instruction' string.
//*****************************************************************************
int
split_instruction(char *instruction, char **opcode, char **param1, char **param2)
{
    // create char pointer to scan through the 'instructon' string
    char *ch_ptr = instruction;
    int num_fields = 0;         // counter of number of found fields

    // set all receiving values to NULL
    *opcode = NULL;
    *param1 = NULL;
    *param2 = NULL;

    // find each field
    *opcode = ch_ptr;
    if (*ch_ptr == '\0')
        return num_fields;
    ++num_fields;

    // skip to start of next field, if any
    while (*ch_ptr && *ch_ptr != ' ' && *ch_ptr != '\t')
        ++ch_ptr;
    if (*ch_ptr == '\0')
        return num_fields;
    *ch_ptr++ = '\0';       // delimit the opcode field
    while (*ch_ptr && (*ch_ptr == ' ' || *ch_ptr == '\t'))
        ++ch_ptr;
    if (*ch_ptr == '\0')
        return num_fields;

    *param1 = ch_ptr;
    ++num_fields;

    // skip to start of next field, if any
    // note that this might be an ASM literal: '[...]'
    while (*ch_ptr && *ch_ptr != ' ' && *ch_ptr != '\t')
        ++ch_ptr;
    if (*ch_ptr == '\0')
        return num_fields;
    *ch_ptr++ = '\0';       // delimit the param2 field
    while (*ch_ptr && (*ch_ptr == ' ' || *ch_ptr == '\t'))
        ++ch_ptr;
    if (*ch_ptr == '\0')
        return num_fields;

    *param2 = ch_ptr;
    ++num_fields;

    // check for [...] and return everything inside the [] if so
    if (*ch_ptr == '[')
    {
        // look for terminating ']'
        while (*ch_ptr && *ch_ptr != ']')
        {
            ++ch_ptr;
        }
        if (*ch_ptr == '\0')
            error("Missing final ']' in ASM list");
        ++ch_ptr;
    }
    else
    {
        // delimit param2
        while (*ch_ptr && *ch_ptr != ' ' && *ch_ptr != '\t')
            ++ch_ptr;
    }
    *ch_ptr++ = '\0';

    return num_fields;
}

//*****************************************************************************
// Description : Convert a string of comma-delimited integers to an array of
//             : WORD values.
//  Parameters : values - address of a string of integer values, comma-separated
//     Returns : The address of a the array of values.
//    Comments : Updates 'num_values' to hold the number of values in the array.
//             : The array of values is static here.
//*****************************************************************************

WORD list2int_array[LIST2INT_SIZE];

WORD *
list2int(char *values, int *num_values)
{
    // define array that will hold WORD values
    static WORD list2int_array[LIST2INT_SIZE];
    int next_value = 0;     // index pointing at next empty slot in list2int_array
    char *val_ptr;          // pointer to each integer string

    // step through comma-delimited vavlues
    while ((val_ptr = strsep(&values, ";")) != NULL)
    {
        // remove leading/trailing white space
        val_ptr = strip_lead_ws(val_ptr);
        val_ptr = strip_trail_ws(val_ptr);
        if (strlen(val_ptr) == 0)
            continue;       // ignore blank values

        list2int_array[next_value] = str2int(val_ptr);
        ++next_value;

        if (next_value >= LIST2INT_SIZE)
            error("Too many values in list: list2int()");
    }

    *num_values = next_value;
    
    return list2int_array;
}

//*****************************************************************************
// Description : Assemble a list of instructions.
//  Parameters : addr - the initial address to assemble at (implicit ORG value)
//             : code - address of a list of instructions
//     Returns : A list of WORD values from the assembly.
//    Comments : 
//*****************************************************************************

WORD asm_result[ASMRESULT_SIZE];

WORD *
assemble(WORD addr, char *code, int *num_words)
{
    // define a scanning ptr
    char *scan_ptr = code;      // define a scanning ptr

    // split code (on '|') into instruction lines and
    // write to a temporary ASM file
    FILE *fd = fopen(ASM_FILENAME, "w");

    if (fd == NULL)
        error("Error writing to %s", ASM_FILENAME);

    fprintf(fd, "\tORG\t0%o\n", addr);

    while (true)
    {
        char *ch_ptr = strchr(scan_ptr, '|');

        if (ch_ptr == NULL)
        {
            // emit last line of code and break
            fprintf(fd, "\t%s\n", scan_ptr);
            break;
        }

        // terminate the first command
        *ch_ptr = '\0';

        // write next line of code
        fprintf(fd, "\t%s\n", scan_ptr);

        // move pointers to search for next line
        scan_ptr = ch_ptr + 1;
    }

    // write END statement to file
    fprintf(fd, "\tEND\n");
    fclose(fd);

    // run the code through the assembler
    char cmd[256];
    sprintf(cmd, "../pyasm/pyasm -l %s %s", LST_FILENAME, ASM_FILENAME);
    if (system(cmd) < 0)
    {
        error("Error doing: %s\n", cmd);
    }

    // read assembler LST file and put assembled values into array
    int num_lines = 0;
    char **lines = fread_lines(LST_FILENAME, &num_lines);

    // read all lines except for first and last (ORG and END lines)
    // get assembled value and place into the array
    int next_asm_result = 0;

    for (int i = 1; i < num_lines-1; ++i)
    {
        char *asm_value = get_first_field(lines[i]);

        asm_result[next_asm_result] = str2int(asm_value);
        ++next_asm_result;
    }

    // free the allocated memory
    for (int i = 0; i < num_lines; ++i)
    {
        free(lines[i]);
    }
    free(lines);

    // update the num_words param and return ASM array base address
    *num_words = next_asm_result;
    return asm_result;
}

//*****************************************************************************
// Here we implement the DSL primitives.  They all take two parameters which are
// the DSL field1 and field2 values (lowercase strings).  If one or both are
// missing from the DSL command then the parameters are NULL.
//
//   setreg <name> <value>
//   setmem <address> <value>
//   allmem <value>
//   bootrom <type>
//   romwrite <bool>
//   run [<start_address>[ <stop_address>]]
//   checkcycles <number>
//   checkdcycles <number>
//   checkreg <name> <value>
//   checkmem <addr> <value>
//   checkcpu <state>
//   checkdcpu <state>
//   mount <device> <filename>
//   dismount <device>
//   device <device> ON|OFF
//   checkfile <file1> <file2>
//   dumpmem file begin,end
//   cmpmem file
//   trace <range>[:<range>:...]
//       where <range> ::= <addr>,<addr>
//   onerror ('ignore'|'abort')
//       initially, "onerror abort" assumed
//   include <filename>
//       include text from <filename> in this test
//   abort
//       stop DSL execution immediately
//*****************************************************************************

//*****************************************************************************
// Description : DSL primitive - set a register value.
//  Parameters : name  - the name of the register to set
//             : value - the string value to put into the register
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
setreg(char *name, char *value)
{
    // check we have both name and value
    if (!name || !value)
    {
       return "setreg: missing 'name' and/or 'value' argument";
    }

    WORD word_value = str2int(value);

    if (STREQ(name, "ac"))
    {
        RegValues[Reg_AC_index] = r_AC = word_value;
    }
    else if (STREQ(name, "l"))
    {
        RegValues[Reg_L_index] = r_L = word_value & 1;
    }
    else if (STREQ(name, "pc"))
    {
        RegValues[Reg_PC_index] = r_PC = word_value;
    }
    else if (STREQ(name, "ds"))
    {
        RegValues[Reg_DS_index] = r_DS = word_value;
    }
    else if (STREQ(name, "dpc"))
    {
        dcpu_set_PC(word_value);
    }
    else if (STREQ(name, "dx"))
    {
        dcpu_set_DX(word_value);
    }
    else if (STREQ(name, "dy"))
    {
        dcpu_set_DY(word_value);
    }
    else if (STREQ(name, "dsts"))
    {
    }
    else
    {
        sprintf(ErrorMsg, "Bad register name: %s", name);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - set a memory value.
//  Parameters : addr  - the string address to set
//             : value - the string value to put into the memory location
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
setmem(char *addr, char *values)
{
    // check we have both name and value
    if (!addr || !values)
    {
       return "setmem: missing 'addr' and/or 'values' argument";
    }

    WORD int_addr = str2int(addr);
    int num_values = 0;
    WORD *val_list = NULL;

    if (*values == '[')
    {
        // remove last char of values string (assume it's ']') and assemble code
        char *last_char = values + strlen(values) - 1;

        if (*last_char != ']')
            return "Missing ']' in line";
        *last_char = '\0';

        val_list = assemble(int_addr, values+1, &num_values);
    }
    else
        val_list = list2int(values, &num_values);

    // put values into memory
    for (int i = 0; i < num_values; ++i)
    {
        WORD v = val_list[i];

        MemValues[int_addr] = v;        // remember changed address
        mem_put(int_addr, false, v);    // update simulator memory
        if (++int_addr > MEMORY_SIZE)   // inc address, wrap if required
            int_addr &= MEMORY_SIZE;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - set all memory locations to a value.
//  Parameters : value   - the value to set memory to
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments : Remember the value we set for after-run checks.
//*****************************************************************************
char *
allmem(char *value, char *ignored)
{
    // check we have both name and value
    if (!value)
    {
       return "allmem: missing 'value' argument";
    }

    WORD new_value = str2int(value);

    MemAllValues = new_value;

    for (WORD i = 0; i < MEMORY_SIZE; ++i)
    {
        MemValues[i] = new_value;
        mem_put(i, false, new_value);
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - set bootloader ROM to required code.
//  Parameters : loader_type   - either "ptr" or "tty"
//             : ignored       - UNUSED
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
bootrom(char *loader_type, char *ignored)
{
    // check we have required parameters
    if (!loader_type)
    {
       return "bootrom: missing 'loader_type' argument";
    }

    if (STREQ(loader_type, "ptr"))
    {
        mem_set_rom(PtrROMImage);
        return NULL;
    }

    if (STREQ(loader_type, "tty"))
    {
        mem_set_rom(TtyROMImage);
        return NULL;
    }

    sprintf(ErrorMsg, "bootrom: invalid bootloader type: '%s'", loader_type);
    return ErrorMsg;
}

//*****************************************************************************
// Description : DSL primitive - set ROM memory to writable/unwriteable.
//  Parameters : writable - "true" if ROM memory is writable
//             : ignored  - UNUSED
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
romwrite(char *writable, char *ignored)
{
    // check we have required parameters
    if (!writable)
    {
       return "romwrite: missing 'writable' argument";
    }

    bool readonly = ! str2bool(writable);

    mem_set_rom_readonly(readonly);

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - execute a number of instructions.
//  Parameters : start - address to start executing at, if NULL, just run
//             : stop  - address to stop at (may be NULL)
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
run(char *start, char *stop)
{
    // both args may be NULL, don't check

    WORD stop_address = 0xffff;		// default to "impossible" address

    // if given start, set PC to that
    if (start)
    {
        r_PC = str2int(start) & MEMMASK;
    }

    // if given stop address, set that, else run until HLT
    if (stop)
    {
        stop_address = str2int(stop) & MEMMASK;
    }

    cpu_start();

    clock_gettime(CLOCK_MONOTONIC, &time_start);
    int num_executed = 0;
    int dnum_executed = 0;
    while (cpu_running())
    {
        int cycles = cpu_execute_one();
	int dcycles = dcpu_execute_one();

        ptr_tick(cycles);
        ptp_tick(cycles);
        UsedCycles += cycles;
        UsedDCycles += dcycles;
	num_executed += 1;
	dnum_executed += 1;

        if (r_PC == stop_address)
            break;
    }
    clock_gettime(CLOCK_MONOTONIC, &time_stop);
    vlog("%d instructions took %d usec",
         num_executed, time_elapsed(&time_start, &time_stop));

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check number of cycles executed.
//  Parameters : cycles  - number of cycles expected
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkcycles(char *cycles, char *ignored)
{
    // check we have required parameters
    if (!cycles)
    {
       return "checkcycles: missing 'cycles' argument";
    }

    int num_cycles = str2int(cycles);

    if (num_cycles != UsedCycles)
    {
        sprintf(ErrorMsg, "Run used %d cycles, expected %d",
                          UsedCycles, num_cycles);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check number of display cycles executed.
//  Parameters : cycles  - number of cycles expected
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkdcycles(char *cycles, char *ignored)
{
    // check we have required parameters
    if (!cycles)
    {
       return "checkdcycles: missing 'cycles' argument";
    }

    int num_cycles = str2int(cycles);

    if (num_cycles != UsedDCycles)
    {
        sprintf(ErrorMsg, "Run used %d display cycles, expected %d",
                          UsedDCycles, num_cycles);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check register contains given value.
//  Parameters : reg   - register to check
//             : value - expected register value
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkreg(char *reg, char *value)
{
    // check we have required parameters
    if (!value)
    {
       return "checkreg: missing 'reg' and/or 'value' argument";
    }

    WORD new_value = str2int(value);

    if (STREQ(reg, "ac"))
    {
        // save actual value in RegValues for check_all_regs()
        RegValues[Reg_AC_index] = r_AC;

        if (r_AC != new_value)
        {
            sprintf(ErrorMsg, "AC is %07o, should be %07o",
                              r_AC, new_value);
            return ErrorMsg;
        }
    }
    else if (STREQ(reg, "l"))
    {
        // save actual value in RegValues for check_all_regs()
        RegValues[Reg_L_index] = r_L;

        if (r_L != new_value)
        {
            sprintf(ErrorMsg, "L is %02o, should be %07o",
                              r_L, new_value);
            return ErrorMsg;
        }
    }
    else if (STREQ(reg, "pc"))
    {
        // save actual value in RegValues for check_all_regs()
        RegValues[Reg_PC_index] = r_PC;

        if (r_PC != new_value)
        {
            sprintf(ErrorMsg, "PC is %07o, should be %07o",
                              r_PC, new_value);
            return ErrorMsg;
        }
    }
    else if (STREQ(reg, "ds"))
    {
        // save actual value in RegValues for check_all_regs()
        RegValues[Reg_DS_index] = r_DS;

        if (r_DS != new_value)
        {
            sprintf(ErrorMsg, "DS is %07o, should be %07o",
                              r_DS, new_value);
            return ErrorMsg;
        }
    }
    else if (STREQ(reg, "dpc"))
    {
        WORD dpc = dcpu_get_PC();
	if (dpc != new_value)
	{
            sprintf(ErrorMsg, "DPC is %07o, should be %07o",
                              dpc, new_value);
            return ErrorMsg;
	}
    }
    else if (STREQ(reg, "dx"))
    {
//        // save actual value in RegValues for check_all_regs()
//        RegValues[Reg_DS_index] = r_DS;
        WORD dx = dcpu_get_DX();
        if (dx != new_value)
	{
            sprintf(ErrorMsg, "DX is %07o, should be %07o",
                              dx, new_value);
            return ErrorMsg;
	}
    }
    else if (STREQ(reg, "dy"))
    {
        WORD dy = dcpu_get_DY();
        if (dy != new_value)
	{
            sprintf(ErrorMsg, "DY is %07o, should be %07o",
                              dy, new_value);
            return ErrorMsg;
	}
    }
    else if (STREQ(reg, "lendt"))
    {
        int depth = dcpu_get_DRSindex();

	if (depth != new_value)
	{
            sprintf(ErrorMsg, "DT stack is %d deep, should be %d",
                              depth, new_value);
            return ErrorMsg;
	}
    }
    else
    {
        sprintf(ErrorMsg, "checkreg: bad register name: %s", reg);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check memory contains given value.
//  Parameters : addr  - memory to check
//             : value - expected memory value
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkmem(char *addr, char *value)
{
    // check we have required parameters
    if (!addr || !value)
    {
       return "checkmem: missing 'addr' and/or 'value' argument";
    }

    WORD new_addr = str2int(addr);
    WORD new_value = str2int(value);
    WORD memvalue = mem_get(new_addr, false);

    MemValues[new_addr] = memvalue;        // remember that we have hecked this location

    if (memvalue != new_value)
    {
        sprintf(ErrorMsg, "Memory %07o is %07o, should be %07o",
                          new_addr, memvalue, new_value);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check that the state of the CPU is as expected.
//  Parameters : state   - expected CPU state
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkcpu(char *state, char *ignored)
{
    // check we have required parameters
    if (!state)
    {
       return "checkcpu: missing 'state' argument";
    }

    // check the 'state' string is valid
    if (!STREQ(state, "on") && !STREQ(state, "off"))
    {
        sprintf(ErrorMsg, "checkcpu: bad state: %s", state);
        return ErrorMsg;
    }

    // OK, check CPU state
    bool cpu_is_running = cpu_running();
    if (STREQ(state, "on"))
    {
        if (!cpu_is_running)
	{
            sprintf(ErrorMsg, "Main CPU run state is 'OFF', should be 'ON'");
            return ErrorMsg;
	}
    }
    else if (STREQ(state, "off"))
    {
        if (cpu_is_running)
	{
            sprintf(ErrorMsg, "Main CPU run state is 'ON', should be 'OFF'");
            return ErrorMsg;
	}
    }

    return NULL;	// no error
}

//*****************************************************************************
// Description : DSL primitive - check that the state of the DCPU is as expected.
//  Parameters : state   - expected DCPU state
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkdcpu(char *state, char *ignored)
{
    // check we have required parameters
    if (!state)
    {
       return "checkdcpu: missing 'state' argument";
    }

    // check the 'state' string is valid
    if (!STREQ(state, "on") && !STREQ(state, "off"))
    {
        sprintf(ErrorMsg, "checkdcpu: bad state: %s", state);
        return ErrorMsg;
    }

    // OK, check CPU state
    bool dcpu_is_running = dcpu_running();
    if ((STREQ(state, "on") && !dcpu_is_running) ||
            (STREQ(state, "off") && dcpu_is_running))
    {
        char *state_str = dcpu_is_running ? "on" : "off";

        sprintf(ErrorMsg, "Display CPU run state is '%s', should be '%s'",
                          state_str, state);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - mount a file on a device.
//  Parameters : device   - the device to mount the file on
//             : filename - name of the file to mount
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
mount(char *device, char *filename)
{
    // check we have required parameters
    if (!device || !filename)
    {
       return "mount: missing 'device' and/or 'filename' argument";
    }

    if (STREQ(device, "ptr"))
    {
        if (access(filename, F_OK|R_OK) == -1)
        {
            sprintf(ErrorMsg, "mount: '%s' doesn't exist or isn't a file", filename);
            vlog("mount ptr: '%s' doesn't exist or isn't a file", filename);
            return ErrorMsg;
        }

        ptr_mount(filename);
    }
    else if (STREQ(device, "ptp"))
    {
        // output - no need to check if file exists or is readable

        ptp_mount(filename);
    }
#ifdef JUNK
    else if (STREQ(device, "ttyin"))
    {
        if (access(filename, F_OK|R_OK) == -1)
        {
            sprintf(ErrorMsg, "mount: '%s' doesn't exist or isn't a file", filename);
            vlog("mount tty: '%s' doesn't exist or isn't a file", filename);
            return ErrorMsg;
        }

        ttyin_mount(filename);
    }
    else if (STREQ(device, "ttyout"))
    {
        // output - no need to check if file exists or is readable

        ttyout_mount(filename);
    }
#endif
    else
    {
        sprintf(ErrorMsg, "mount: bad device: %s", device);
        vlog("mount: bad device: %s", device);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - dismount a file on a device.
//  Parameters : device  - the device to mount the file on
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
dismount(char *device, char *ignored)
{
    // check we have required parameters
    if (!device)
    {
       return "dismount: missing 'device' argument";
    }

    if (STREQ(device, "ptr"))
    {
        ptr_dismount();
    }
    else if (STREQ(device, "ptp"))
    {
        ptp_dismount();
    }
    else
    {
        sprintf(ErrorMsg, "dismount: bad device: %s", device);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - control a device.
//  Parameters : device  - the device to control
//             : state   - the new state of the device
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
device(char *device, char *state)
{
    // check we have required parameters
    if (!device)
    {
       return "device: missing 'device' argument";
    }

    if (STREQ(device, "ptr"))
    {
        if (! STREQ(state, "on") && ! STREQ(state, "off"))
        {
            sprintf(ErrorMsg, "device ptr: '%s' isn't a valid state", state);
            vlog("device ptr: '%s' isn't a valid state", state);
            return ErrorMsg;
        }

        if (STREQ(state, "on"))
            ptr_start();
        else
            ptr_stop();
    }
    else if (STREQ(device, "ptp"))
    {
        if (! STREQ(state, "on") && ! STREQ(state, "off"))
        {
            sprintf(ErrorMsg, "device ptp: '%s' isn't a valid state", state);
            vlog("device ptp: '%s' isn't a valid state", state);
            return ErrorMsg;
        }

        if (STREQ(state, "on"))
            ptp_start();
        else
            ptp_stop();
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - check that two files have the same contents.
//  Parameters : file1 - first file to compare
//             : file2 - second file to compare
//     Returns : NULL or a status string.
//    Comments :
//*****************************************************************************
char *
checkfile(char *file1, char *file2)
{
    // check we have required parameters
    if (!file1 || !file2)
    {
       return "checkfile: missing 'file1' and/or 'file2' argument";
    }

    // check that the files exist
    if (access(file1, F_OK|R_OK) == -1)
    {
        sprintf(ErrorMsg, "checkfile: '%s' doesn't exist or isn't a file", file1);
        return ErrorMsg;
    }

    if (access(file2, F_OK|R_OK) == -1)
    {
        sprintf(ErrorMsg, "checkfile: '%s' doesn't exist or isn't a file", file2);
        return ErrorMsg;
    }

    // check that the files have the same contents
    if (! files_same(file1, file2))
    {
        sprintf(ErrorMsg, "Files %s and %s are different", file1, file2);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - dump memory to a file.
//  Parameters : filename  - file to dump the memory to
//             : addresses - string containing "begin,end" addresses
//     Returns : NULL or a status string.
//    Comments : Dump from 'begin' to 'end', not including the end address.
//*****************************************************************************
char *
dumpmem(char *filename, char *addresses)
{
    // check we have required parameters
    if (!filename || !addresses)
    {
       return "dumpmem: missing 'filename' and/or 'addresses' argument";
    }

    char *copy_addresses = NULL;

    int begin = 0;     // dump from here
    int end = 0;       //  to here
    if (addresses == NULL)
    {
        // dump everything
        begin = 0;
        end = MEMORY_SIZE - 1;
    }
    else
    {
        copy_addresses = malloc(strlen(addresses) + 1);
        strcpy(copy_addresses, addresses);

        char *ch_ptr = strchr(addresses, ',');

        if (ch_ptr == NULL)
        {
            sprintf(ErrorMsg, "dumpmem: dump limits are bad: %s", copy_addresses);
            return ErrorMsg;
        }

        *ch_ptr = '\0';
        begin = str2int(addresses);
        addresses = ch_ptr + 1;

        ch_ptr = strchr(addresses, ',');
        if (ch_ptr != NULL)
        {
            sprintf(ErrorMsg, "dumpmem: dump limits are bad: %s", copy_addresses);
            return ErrorMsg;
        }
        end = str2int(addresses) - 1;
    }

    // check begin/end validity
    if (end < begin || end < 0 || begin < 0)
    {
        sprintf(ErrorMsg, "dumpmem: 'begin' must be before 'end': %s", copy_addresses);
        return ErrorMsg;
    }

    // create octdump-like text file with required memory locations
    FILE *fd = fopen(filename, "w");
    WORD addr = begin;
    WORD offset = addr;
    WORD chunk[8];
    int chunk_index = 0;

    while (addr <= end)
    {
        WORD word = mem_get(addr, false);

        ++addr;
        chunk[chunk_index] = word;
        ++chunk_index;
        if (chunk_index >= 8)
        {
            char buff[256];
            char *line = octword_line(buff, chunk, chunk_index, offset);

            fprintf(fd, "%s\n", line);
            chunk_index = 0;
            offset += 8;
        }
    }

    // emit any unfinished line
    if (chunk_index > 0)
    {
        char buff[256];
        char *line = octword_line(buff, chunk, chunk_index, offset);
        fprintf(fd, "%s\n", line);
    }

    fclose(fd);

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - compare a 'dumpmem' file with memory.
//  Parameters : filename  - file to compare the memory to
//             : ignored   - UNUSED
//     Returns : NULL or a status string.
//    Comments : A dumpmem file has the following format:
//             : 000100  100000 010104 000000 000000 004111 000000 000000 000000 |...D.....I......|
//             : address value1 value2 ...
//*****************************************************************************
char *
cmpmem(char *filename, char *ignored)
{
    // check we have required parameters
    if (!filename)
    {
       return "cmpmem: missing 'filename' argument";
    }

    FILE *fp = fopen(filename, "r");
    size_t buff_len = 128;
    char *line_buff = malloc(buff_len+1);
    char *result = malloc(1);

    *result = '\0';

    if (fp == NULL)
    {
        sprintf(ErrorMsg, "cmpmem: Can't open file %s", filename);
        return ErrorMsg;
    }

    // read file line by line, compare contents with memory
    // no error checking, soince we know the file format
    while (getline(&line_buff, &buff_len, fp) != -1)
    {
        char *scan_ptr = line_buff;
        char *num_ptr = line_buff;

        // delimit and get initial address
        scan_ptr = strchr(scan_ptr, ' ');
        *scan_ptr = '\0';
        ++scan_ptr;

        WORD addr = str2int(num_ptr);

        // now get the other integers on the line, stop at '|'
        while (true)
        {
            // scan forward to next non-space character
            while (*scan_ptr == ' ')
                ++scan_ptr;

            // if it's a '|', we are finished
            if (*scan_ptr == '|')
                break;

            // else it's another WORD value, get it and compare with memory
            num_ptr = scan_ptr;
            while (*scan_ptr != ' ')
                ++scan_ptr;
            *scan_ptr = '\0';
            ++scan_ptr;

            WORD expected = octstr2int(num_ptr);
            WORD actual = mem_get(addr, false);

            if (actual != expected)
            {
                dyn_appendf(&result,
                            "\nAddress %06o is %06o, expected %06o",
                            addr, actual, expected);
            }

            addr += 1;
        }
    }
    
    return result;
}

//*****************************************************************************
// Description : DSL primitive - Set the action upon an error.
//  Parameters : action  - action on error, either 'ignore' or 'abort'
//             : ignored - UNUSED
//     Returns : NULL or a status string.
//    Comments : 
//*****************************************************************************
char *
onerror(char *action, char *ignored)
{
    // check we have required parameters
    if (!action)
    {
       return "onerror: missing 'action' argument";
    }

    if (STREQ(action, "ignore"))
        AbortOnError = false;
    else if (STREQ(action, "abort"))
        AbortOnError = true;
    else
    {
        sprintf(ErrorMsg, "onerror: bad action name: %s", action);
        return ErrorMsg;
    }

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - Include another test file.
//  Parameters : filename  - name of the file to include
//             : ignored   - UNUSED
//     Returns : NULL or a status string.
//    Comments : Preserves existing AbortOnError & DSLFilename values.
//*****************************************************************************
char *
include(char *filename, char *ignored)
{
    // check we have required parameters
    if (!filename)
    {
       return "include: missing 'filename' argument";
    }

    // if file doesn't exist report error
    if (access(filename, F_OK) != 0)
    {
        sprintf(ErrorMsg, "include: can't file include file '%s'", filename);
        return ErrorMsg;
    }

    // remember old AbortOnError status, return to this after process_file() below
    // we don't want any abort status changes in an include to be carried forward
    bool old_abort_status = AbortOnError;

    // also preserves the "current" DSL filename
    char old_dslfilename[LenDSLFilename];
    strcpy(old_dslfilename, DSLFilename);

    process_file(filename);

    AbortOnError = old_abort_status;        // reset abort status
    strcpy(DSLFilename, old_dslfilename);   // and DSL filename

    return NULL;
}

//*****************************************************************************
// Description : DSL primitive - Immediate abort.
//  Parameters : message     - Message to print before stopping
//             : linenum_str - The line number in the DSL file
//     Returns : 
//*****************************************************************************
char *
dsl_abort(char *message, char *linenum_str)
{
    // check we have required parameters
    if (!message || !linenum_str)
    {
       return "dsl_abort: missing 'message' and/or 'linenum_str' argument";
    }

    message = message ? message : "";
    error("ABORT:%s:%s:%s\n", DSLFilename, linenum_str, message);

    return NULL;	// not rexecute, error() doesn't return
}

//*****************************************************************************
// Now we have the routines to check things before and after a test.
//*****************************************************************************

//*****************************************************************************
// Description : Check memory for unwanted changes.
//  Parameters : 
//     Returns : NULL or a dynamic status string.
//    Comments : 
//*****************************************************************************
char *
check_all_mem(void)
{
    // count of errors found
    int err_cnt = 0;

    // stuff for a dynamic string
    char *result = malloc(15);
    strcpy(result, "check_all_mem:");

    // check each memory value
    for (int index = 0; index < MEMORY_SIZE; ++index)
    {
        WORD value = mem_get(index, false);
        WORD expected = MemValues[index];

        if (value != expected)
        {
            vlog("check_all_mem: index=%07o, value=%07o, expected=%07o", index, value, expected);
            dyn_appendf(&result,
                        "\nMemory at %07o is %07o, should be %07o",
                        index, value, expected);
            ++err_cnt;
        }
    }

    if (err_cnt)
        return result;
    
    return NULL;
}

//*****************************************************************************
// Description : Check registers for unwanted changes.
//  Parameters : 
//     Returns : NULL or a dynamic status string.
//    Comments : 
//*****************************************************************************
char *
check_all_regs(void)
{
    // stuff for dynamic string
    char *result = malloc(1);     // the dynamic message buffer
    strcpy(result, "");

    if (r_AC != RegValues[Reg_AC_index])
        dyn_appendf(&result, "\nAC is %07o, should be %07o",
                             r_AC, RegValues[Reg_AC_index]);

    if (r_L != (RegValues[Reg_L_index] & 1))
        dyn_appendf(&result, "\nL is %07o, should be %07o",
                             r_L, RegValues[Reg_L_index] & 1);

    if (r_PC != RegValues[Reg_PC_index])
        dyn_appendf(&result, "\nPC is %07o, should be %07o",
                             r_PC, RegValues[Reg_DS_index]);

    if (r_DS != RegValues[Reg_DS_index])
        dyn_appendf(&result, "\nDS is %07o, should be %07o",
                             r_DS, RegValues[Reg_DS_index]);

    if (strlen(result))
        return result;

    return NULL;
}

//*****************************************************************************
// Description : Execute one test suite.
//  Parameters : test      string containing the test suite
//             : line_num  the line number in the file of the suite
//             : filename  string holding test suite filename
//     Returns : The number of errors found.
//    Comments : 
//*****************************************************************************
int
execute(char *test, int line_num, char *filename)
{
    int err_cnt = 0;  // number of errors found

    // work on a copy of the test in 'test' because we chop the test string up
    char *copy_test = malloc(strlen(test) + 1);
    strcpy(copy_test, test);

    // allocate a place for error messages, grows dynamically
    char *result = malloc(1);     // the dynamic message buffer
    strcpy(result, "");

    // convert line number to string
    char line_num_str[32];
    sprintf(line_num_str, "%d", line_num);

    // error string holder
    char *r = NULL;

    // set registers and memory to ZERO
    setreg("ac", 0);
    setreg("l", 0);
    allmem("0", "ignored");

    // set number main and display cycles used to 0
    UsedCycles = 0;
    UsedDCycles = 0;

    // reset the PTR/PTP device
    ptrptp_reset();

    // split test line into instructions (at ';') and do each in turn
    char *instruction;  // complete single test instruction
    char *opcode;       // the opcode string
    char *param1;       // first parameter string
    char *param2;       // second parameter string

    while ((instruction = strsep(&copy_test, ";")) != NULL)
    {
        char *r = NULL;     // returned status string, may be NULL

        // remove leading/trailing white space
        instruction = strip_lead_ws(instruction);
        instruction = strip_trail_ws(instruction);
        if (strlen(instruction) == 0)
            continue;       // ignore blank instructions

        // split command into opcode/param1/param2
        split_instruction(instruction, &opcode, &param1, &param2);
        if (opcode != NULL)
            lowercase(opcode);
        if (param1 != NULL)
            lowercase(param1);
        if (param2 != NULL)
            lowercase(param2);

        vlog("\tDSL: '%s' '%s' '%s'", opcode ? opcode:"",
                                      param1 ? param1:"",
                                      param2 ? param2:"");

        // call the correct DSL primitive
        if      (STREQ(opcode, "setreg"))
            r = setreg(param1, param2);
        else if (STREQ(opcode, "setmem"))
            r = setmem(param1, param2);
//        else if (STREQ(opcode, "allreg"))
//            r = allreg(param1, param2);
        else if (STREQ(opcode, "allmem"))
            r = allmem(param1, param2);
        else if (STREQ(opcode, "bootrom"))
            r = bootrom(param1, param2);
        else if (STREQ(opcode, "romwrite"))
            r = romwrite(param1, param2);
        else if (STREQ(opcode, "run"))
            r = run(param1, param2);
        else if (STREQ(opcode, "checkcycles"))
            r = checkcycles(param1, param2);
        else if (STREQ(opcode, "checkdcycles"))
            r = checkdcycles(param1, param2);
        else if (STREQ(opcode, "checkreg"))
            r = checkreg(param1, param2);
        else if (STREQ(opcode, "checkmem"))
            r = checkmem(param1, param2);
        else if (STREQ(opcode, "checkcpu"))
            r = checkcpu(param1, param2);
        else if (STREQ(opcode, "checkdcpu"))
            r = checkdcpu(param1, param2);
        else if (STREQ(opcode, "mount"))
        {
            char *r = mount(param1, param2);

            if (r)
            {
                ++err_cnt;
                error(r);
                vlog("Error doing 'mount'");
                break;
            }
        }
        else if (STREQ(opcode, "dismount"))
            r = dismount(param1, param2);
        else if (STREQ(opcode, "device"))
            r = device(param1, param2);
        else if (STREQ(opcode, "checkfile"))
            r = checkfile(param1, param2);
        else if (STREQ(opcode, "dumpmem"))
            r = dumpmem(param1, param2);
        else if (STREQ(opcode, "cmpmem"))
            r = cmpmem(param1, param2);
        else if (STREQ(opcode, "onerror"))
            r = onerror(param1, param2);
        else if (STREQ(opcode, "include"))
            r = include(param1, param2);
        else if (STREQ(opcode, "abort"))
	{
	    param2 = line_num_str;
            r = dsl_abort(param1, param2);
	}
        else
            error("%s:%4d: Unrecognized instruction: '%s'\n", filename, line_num, instruction);

//        // afterwards, dump registers
//        logreg();
//        logmem(0110);

        // check returned value, maybe error
        if (r)
        {
            ++err_cnt;
            dyn_appendf(&result, "\n%s", r);
	    vlog("error %s:%3d:%s", filename, line_num, r);
        }
    }

    // now check memory and registers for unexpected changes
    r = check_all_mem();
    if (r)
    {
        ++err_cnt;
        dyn_appendf(&result, "\n%s", r);
        free(r);
    }

    r = check_all_regs();
    if (r)
    {
        ++err_cnt;
        dyn_appendf(&result, "\n%s", r);
        free(r);
    }

    if (strlen(result) > 1)
    {
        vlog(DELIM2);
        vlog("%s", result);
        vlog(DELIM2);

        // show user the test line we are executing
        printf("\r \r%s\n", DELIM2);
        printf("Test %s:%3d: %s\n", filename, line_num, test);
        printf("%s", DELIM2);

        // show the error messages
        printf("%s\n", result);

        vlog("AbortOnError=%s", AbortOnError ? "true" : "false");
        if (AbortOnError)
            exit(10);
    }

    return err_cnt;
} 

//*****************************************************************************
// Description : 
//  Parameters : 
//     Returns : 
//    Comments : 
//*****************************************************************************
void
usage(void)
{
    printf("%s", "\nTest imlac CPU opcodes directly using a DSL.\n\n");
    printf("%s", "Usage: test_cpu [<options>] <filename>\n\n");
    printf("%s", "where <filename> is a file of test instructions and\n");
    printf("%s", "      <options> is one or more of\n");
    printf("%s", "          -h    prints this help and stops\n\n");
}

//*****************************************************************************
// Description : Read and return complete test lines from a file.
//  Parameters : fname         - path to the file to read
//             : ret_num_tests - return int to be set to the number of Tests returned
//     Returns : The address of an array of Test structs.
//    Comments : An entry for each non-empty line in the input file is created.
//             : Lines that are comments or empty are ignored.
//*****************************************************************************
Test **
get_lines(char *fname, int *ret_num_lines)
{
    // array of Test and state variables
    int next_test = 0;          // index to first unused Test entry
    int num_tests = 4;          // number of Tests in 'test_array'
    Test **test_array = malloc(sizeof(Test *) * num_tests);  // array of Test * structs

    // open file for reading
    FILE *fp = fopen(fname, "r");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }

    // read each line, allow for long lines
    size_t buff_len = 256;      // initial size of buffer
    char *line_buff = malloc(buff_len+1);

    int linenum = 0;
    while (getline(&line_buff, &buff_len, fp) != -1)
    {
        ++linenum;

        // read a line, strip any trailing newline character
        char *ch_ptr = strchr(line_buff, '\n');
        if (ch_ptr != NULL)
            *ch_ptr = '\0';

        // strip trailing spaces, if any
        strip_trail_ws(line_buff);

        // remove any trailing comments
        ch_ptr = strchr(line_buff, '#');
        if (ch_ptr != NULL)
            *ch_ptr = '\0';

        // if line is blank, ignore it
        if (strlen(line_buff) == 0)
        {
            continue;
        }

        // if new line is a continuation line, add to line previously read
        if (line_buff[0] == ' ' || line_buff[0] == '\t')
        {
            // strip any leading space for the continuation line
            char *short_line = line_buff;

            while (*short_line == ' ' || *short_line == '\t')
            {
                ++short_line;
            }

            // continuation line, create longer line, this len + previous
            int new_len = strlen(test_array[next_test-1]->test) + strlen(short_line);
            char *cont_line = malloc(new_len + 1 + 2);

            strcpy(cont_line, test_array[next_test-1]->test);
            strcat(cont_line, "; ");
            strcat(cont_line, short_line);

            free(test_array[next_test-1]->test);
            test_array[next_test-1]->test = cont_line;
        }
        else
        {
            // new test, check if array still has room
            if (next_test >= num_tests)
            {
                // must extend the 'test_array' in size
                Test **new_test_array = malloc(sizeof(Test *) * num_tests*2);
                for (int i = 0; i < num_tests; ++i)
                {
                    new_test_array[i] = test_array[i];
                }
                num_tests *= 2;
                test_array = new_test_array;
            }

            // put new Test into array
            Test *new_test = malloc(sizeof(Test));
            test_array[next_test] = new_test;
            new_test->linenum = linenum;

            char *new_line = malloc(strlen(line_buff) + 1);
            strcpy(new_line, line_buff);
            new_test->test = new_line;

            // move "next" index to empty slot
            ++next_test;
        }
    }

    // close file and return the array of lines
    fclose(fp);
    *ret_num_lines = next_test;     // update the line counter parameter

    return test_array;
}

//*****************************************************************************
// Description : Process one file of DSL suites.
//  Parameters : filename - path to the file to read
//     Returns : 
//    Comments : Updates the globals:
//             :    num_executes  number of suites executed
//             :    num_errors    number of errors encountered
//*****************************************************************************
void
process_file(char *filename)
{
    // open the given file and get all test lines
    int num_lines = 0;
    Test **lines = get_lines(filename, &num_lines);

    vlog("TraceFlag=%s", TraceFlag ? "true" : "false");
    vlog("Reading file '%s', got %d lines", filename, num_lines);
    if (TraceFlag)
    {
        trace_open();        // DEBUG
    }

    // for each line, perform the test
    for (int line_index = 0; line_index < num_lines; ++line_index)
    {
        Test *test = lines[line_index];
        int linenum = test->linenum;
        char *line = test->test;

        if (strlen(line))
        {
            vlog(DELIM);
            vlog("Test %s:%3d: %s", filename, linenum, line);
            vlog(DELIM);

            int errors = execute(line, linenum, filename);
            ++num_executes;
            if (errors > 0)
            {
                ++num_errors;
            }
        }

        show_progress();
    }
}


//*****************************************************************************
// Description : Code for the program.
//  Parameters : argc - parameter count
//             : argv - vector of CLI params
//     Returns : 0 if no problem, else a value > 0.
//    Comments : 
//*****************************************************************************
int
main(int argc, char *argv[])
{
    // vars to measure elapsed time
    struct timespec start, end;
    uint64_t delta_us;

    vlog("Start of Imlac test");

    // check args
    if (argc != 2 && argc != 3)
    {
        usage();
        exit(1);
    }

    if (STREQ(argv[1], "-h"))
    {
        usage();
        exit(0);
    }

    char *filename = argv[1];        // assume no tracing

    if (STREQ(argv[1], "-t"))
    {
        TraceFlag = true;
        if (argc != 3)
        {
            usage();
            exit(1);
        }
        filename = argv[2];
    }

    vlog("Running test file '%s'", filename);

    // get start time
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    // process the file
    num_executes = 0;        // reset global num executes and error counters
    num_errors = 0;
    strcpy(DSLFilename, filename);
    process_file(filename);

    // get end time
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

    printf("\r \r");
    fflush(stdout);
    printf("Executed %d test%s, %d errored\n",
    num_executes, (num_executes > 1) ? "s" : "", num_errors);

    // show elapsed time
    printf("Tests ran in %.2fs\n", (float) delta_us / 1000000);

    return 0;
}
