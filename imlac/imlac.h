//*****************************************************************************
// All the things that need to be global.
//*****************************************************************************

#ifndef IMLAC_H
#define IMLAC_H

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include "error.h"

#define IMLAC_VERSION   "0.1"

typedef unsigned int    WORD;
typedef unsigned char   BYTE;

#define MEMORY_SIZE     04000

// number of machine cycles per second (1000000 / 1.8us)
#define CPU_HERTZ       (10000000/18)

#define MEMMASK         0xffff
#define HIGHBITMASK     0x8000
#define WORD_MASK       0xffff
#define BITS_2_15	0x7fff
#define OVERFLOWMASK    0x10000
#define LOWBITMASK      0x1

// macro to convert a boolean value to a string
#define BOOL2STR(a)     ((a) ? "true" : "false")

// macros to more reliably compare strings
#define STREQ(a, b)     ((a) && (strcmp((a), (b)) == 0))
#define STREQCASE(a, b) ((a) && (strcmpcase((a), (b)) == 0))

// macro to mask an address to the address space limits
#define MASK_MEM(addr)  ((addr) & (MEMORY_SIZE - 1))

// macro to determine if address is one of the auto-increment ones
#define ISAUTOINC(a)    ((((a) & 03777) >= 010) && (((a) & 03777) <= 017))

#endif
