//*****************************************************************************
// Utility stuff for the IMLAC implementation.
//*****************************************************************************

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//*****************************************************************************
// Description : Calculate microseconds between "start" and "stop" times.
//  Parameters : start - address of struct timespec containing start time
//             : stop  - address of struct timespec containing stop time
//     Returns : The integer elapsed time in microseconds.
//    Comments : 
//*****************************************************************************

int time_elapsed(struct timespec *start, struct timespec *stop)
{
    int seconds = stop->tv_sec - start->tv_sec;
    int nsecs = stop->tv_nsec - start->tv_nsec;

    return seconds * 1000000 + nsecs / 1000;
}
