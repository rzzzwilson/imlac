//*****************************************************************************
// Log routines for the imlac simulation.
//*****************************************************************************

#ifndef LOG_H
#define LOG_H

void vlog_init(void);
void vlog(char *fmt, ...);
extern char *LogPrefix;

#endif
