/*
 * Implementation of the imlac TTY input device.
 *
 * Temporarily the TTYIN code will not be timed by Imlac cycles.
 * The TTY flag will be reset by the *second* call to RSF.  So
 * when the RSF instruction checks the flag:
 *     . if the flag is not set, don't skip and SET THE FLAG
 *     . second call will skip
 */

#include "imlac.h"
#include "ttyin.h"
#include "log.h"


/*****
 * constants for the TTYIN device
 * these are not used, at the moment (TTYIN runs at top speed)
 ******/

#define TTY_CHARS_PER_SECOND    65000
#define TTY_NOT_READY_CYCLES    ((int) CPU_HERTZ / TTY_CHARS_PER_SECOND)


/*****
 * State variables for the TTYIN device
 ******/

static char *device_filename = NULL;    // name of file mounted, if any
static FILE *device_open_file = NULL;   // open file handle
static bool flag = false;               // true if device has data, always true until EOF
static BYTE prev_byte = 0;		// previous byte from TTY
static long device_cycle_count = 0;     // unused at the moment
static bool file_at_eof = false;        // true if TTY file at EOF
static int elapsed_cycles = 0;		// cycle counter for TTYIN state machine


#define BUFFSIZE        1024            // size of TTY buffer
static BYTE buffer[BUFFSIZE + 1];       // buffer for read chars
static int bytes_remaining = 0;         // number of unread bytes in buffer
static BYTE *ptr = buffer;              // pointer to next BYTE to read


//*****************************************************************************
// Description : Dumps the TTYIN state to the log file
//  Parameters :   prefix  a prefix string added to all logged lines
//     Returns : Nothing.
//    Comments : 
//*****************************************************************************

void
ttyin_vlog_state(char *prefix)
{
    vlog("%s: device_filename=%s", prefix, device_filename);
    vlog("%s: device_open_file=%p", prefix, device_open_file);
    vlog("%s: flag=%s, prev_byte=%02x", prefix, flag ? "true" : "false", prev_byte);
    vlog("%s: ptr=%p, buffer=%p", prefix, ptr, buffer);
    vlog("%s: bytes_remaining=%d", prefix, bytes_remaining);
    if (bytes_remaining > 0)
        vlog("%s: next char read = %02x", prefix, *ptr);
    vlog("%s: file_at_eof=%s", prefix, file_at_eof ? "true" : "false");
}

//*****************************************************************************
// Description : Resets the TTYIN device.
//  Parameters :
//     Returns : Nothing.
//    Comments : 
//*****************************************************************************

void
ttyin_reset(void)
{
    device_filename = NULL;
    device_open_file = NULL;
    flag = false;
    device_cycle_count = 0;
    ptr = buffer;
    prev_byte = 0;
    bytes_remaining = 0;
    file_at_eof = false;
    elapsed_cycles = 0;
}

//*****************************************************************************
// Description : Mount a file on the TTYIN device.
//  Parameters : fname - name of the file
//     Returns : 
//    Comments : 
//*****************************************************************************

void
ttyin_mount(char *fname)
{
    vlog("ttyin_mount: fname=%s", fname);

    if (device_filename != NULL)
       error("ttyin_mount: Can't mount TTYIN file, file '%s' already mounted",
             device_filename);

    ttyin_reset();

    device_filename = fname;
    device_open_file = fopen(fname, "rb");
    if (device_open_file == NULL)
    {
        ttyin_dismount();
        error("ttyin_mount: can't mount file %s", fname);
    }

//    bytes_remaining = 0;

    bytes_remaining = fread(buffer, sizeof(BYTE),
                            BUFFSIZE, device_open_file);
    if (bytes_remaining < 0)
    {
        flag = false;
        file_at_eof = true;
	prev_byte = 0;
    }
    else
    {
        flag = false;
        ptr = buffer;
        prev_byte = 0;
    }

    ttyin_vlog_state("ttyin_mount");
}

//*****************************************************************************
// Description : Dismount the file on the TTYIN device.
//  Parameters : 
//     Returns : 
//    Comments : 
//*****************************************************************************

void
ttyin_dismount(void)
{
    if (device_open_file)
    {
        fclose(device_open_file);
    }

    ttyin_reset();
}

//*****************************************************************************
// Description : Clear the TTY flag (char available flag).
//  Parameters :
//     Returns : Nothing.
//    Comments : 
//*****************************************************************************

void
ttyin_clear_flag(void)
{
    vlog("ttyin_clear_flag: called");
    flag = false;
}


//*****************************************************************************
// Description : Set the TTY flag (char available flag).
//  Parameters :
//     Returns : Nothing.
//    Comments : 
//*****************************************************************************

void
ttyin_set_flag(void)
{
    vlog("ttyin_set_flag: called");
    flag = true;
}


//*****************************************************************************
// Description : Return the state of the TTY buffer flag.
//  Parameters :
//     Returns : "true" if a character is available.
//    Comments : TEMP: always returns "true".
//*****************************************************************************

bool
ttyin_ready(void)
{
    vlog("ttyin_ready: returning %s", flag ? "true" : "false");
    return flag;
}


//*****************************************************************************
// Description : Return the available TTY character.
//  Parameters :
//     Returns : 
//    Comments : Always return the next available character.
//*****************************************************************************

BYTE
ttyin_get_char(void)
{
    // if "flag" is set or not, return the previous character
    return prev_byte;
}


//*****************************************************************************
// Description : Bump the internal TTYIN state.
//  Parameters : The number of elapsed clock cycles.
//     Returns : 
//    Comments : 
//*****************************************************************************

#define TTYIN_CYCLES_NEEDED  100

void
ttyin_tick(int cycles)
{
    // if no mounted file, do nothing
    if (! device_open_file)
    {
        return;
    }

    // bump the cycle counter since last character
    elapsed_cycles += cycles;

    // decide if time for another character
    if (elapsed_cycles > TTYIN_CYCLES_NEEDED)
    {
        elapsed_cycles = 0;  // elapsed_cycles % TTYIN_CYCLES_NEEDED;

        // read more characters if none remaining
        if (bytes_remaining <= 0)
        {
            bytes_remaining = fread(buffer, sizeof(BYTE),
                                       BUFFSIZE, device_open_file);
            ptr = buffer;

	    // if nothing to read it's EOF
            if (bytes_remaining < 0)
            {
                file_at_eof = true;
	        flag = false;
            }
        }

        // read a character, decrement "bytes_remaining",
        // set "flag" to show another char is ready to be read.
        prev_byte = *ptr++;
        --bytes_remaining;
        flag = true;

        vlog("ttyin_tick: read char: prev_byte=%02x, bytes_remaining=%d, flag=%s",
             prev_byte, bytes_remaining, flag ? "true" : "false");
    }

    vlog("ttyin_tick: elapsed_cycles=%d, prev_byte=%07o (%02x), flag=%s",
         elapsed_cycles, prev_byte, prev_byte, flag ? "true" : "false");
}
