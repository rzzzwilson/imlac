//*****************************************************************************
//  The functions here are used to log actions in a progran.
//*****************************************************************************

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>


char *LogFile = "imlac.log";
char *LogPrefix = NULL;
bool initialized = false;


//*****************************************************************************
// Description : Prepare the log file.
//  Parameters : 
//     Returns : 
//    Comments : 
//*****************************************************************************
void
vlog_init(void)
{
    FILE *fd = fopen(LogFile, "w");
    fclose(fd);
}

//*****************************************************************************
// Description : printf()-style log routine.
//  Parameters : like printf()
//     Returns : 
//    Comments : 
//*****************************************************************************
void
vlog(char *fmt, ...)
{
    va_list ap;
    char    buff[1024];
    FILE    *fd;
    double  now = (double) clock() / CLOCKS_PER_SEC;    // time for this message

    // if not initialized
    if (!initialized)
    {
        initialized = true;
	vlog_init();
    }

    // now log the message
    va_start(ap, fmt);
    vsprintf(buff, fmt, ap);
    fd = fopen(LogFile, "a");
    if (LogPrefix)
        fprintf(fd, "%11.6f|%s: %s\n", now, LogPrefix, buff);
    else
        fprintf(fd, "%11.6f|%s\n", now, buff);
    fflush(fd);
    fclose(fd);
    va_end(ap);
}
