#!/usr/bin/env python

"""
A small program to process an Imlac core file.

Usage: format_core.py [<options>] <corefilename>

where <options>      is one or more of:
          -h             print this help and stop
      <corefilename> name of the PTP file to produce
"""

import sys
import os.path
import getopt


def usage(msg=None):
    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)


def process(line, offset):
    line_list = [f"{offset:07o}: "]
    index = 0

    for x in range(len(line)//2):
        b1 = line[index]
        b2 = line[index + 1]
        index += 2
        value = (b1 << 8) + b2
        line_list.append(f"{value:06o} ")
    print("".join(line_list))

    return offset + 8

def format_core(filename):
    print(f"Core dump from file '{filename}'.")
    with open(filename, 'rb') as fd:
        offset = 0
        for line in iter(lambda: fd.read(16), ''):
            if len(line) == 0:
                return
            offset = process(line, offset)

# analyse options
try:
    opts, args = getopt.gnu_getopt(sys.argv, "h", ['help'])
except getopt.GetoptError:
    usage('Bad option')
    sys.exit(10)

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit(0)

if len(args) != 2:
    usage()
    sys.exit(10)

# check input file actually exists
filename = args[1]
if not os.path.exists(filename):
    print(f"Sorry, file '{filename}' doesn't exist.")
    sys.exit(10)

format_core(filename)
