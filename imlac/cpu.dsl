####################################################
# DSL code to test operation of the main CPU.
#
# Note that the test code zeros all of memory and
# AC+L at start.  The 0o000000 opcode is a HLT.
####################################################

# uncomment line below to stop on first error
#onerror ignore

# Need to test simple instructions like NOP and HLT
# first since they are used by other tests.

# NOP
setreg ac 0; setreg l 0; setmem 0100 [NOP]; run 0100 0101; checkcycles 1; checkcpu on; checkreg pc 0101
setreg ac 1; setreg l 1; setmem 0100 [NOP]; run 0100 0101; checkcycles 1; checkcpu on; checkreg pc 0101

# HLT
setmem 0100 [HLT];     run 0100;      checkcycles 1; checkcpu off; checkreg pc 0101
setmem 0100 [NOP|HLT]; run 0100;      checkcycles 2; checkcpu off; checkreg pc 0102
setmem 0100 [NOP];     run 0100 0101; checkcycles 1; checkcpu on;  checkreg pc 0101

# LAW
setreg ac 0177777; setreg l 1; setmem 0100 [LAW 0];    run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [LAW 0];    run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 1;       setreg l 1; setmem 0100 [LAW 0];    run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 1;       setreg l 0; setmem 0100 [LAW 0];    run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [LAW 0377]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0377
setreg ac 0;       setreg l 0; setmem 0100 [LAW 0377]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0377

# LWC
setreg l 1; setmem 0100 [LWC 0]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg l 0; setmem 0100 [LWC 0]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg l 1; setmem 0100 [LWC 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg l 0; setmem 0100 [LWC 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777

# JMP
setreg ac 012345; setreg l 1; setmem 0100 [JMP 0200];                    run 0100
    checkcycles 3; checkreg pc 0201
setreg ac 012345; setreg l 0; setmem 0100 [JMP 0200];                    run 0100
    checkcycles 3; checkreg pc 0201
setreg ac 012345; setreg l 1; setmem 0100 [JMP *0200]; setmem 0200 0120; run 0100
    checkcycles 4; checkreg pc 0121
# address 010 is incremented BEFORE indirect operation
setreg ac 012345; setreg l 0; setmem 0100 [JMP *010];  setmem 010 0200;  run 0100
    checkcycles 4; checkreg pc 0202; checkmem 010 0201
setreg ac 012345; setreg l 0; setmem 0100 [JMP *017];  setmem 017 0200;  run 0100
    checkcycles 4; checkreg pc 0202; checkmem 017 0201
# check address just outside range 010-017
setreg ac 012345; setreg l 0; setmem 0100 [JMP *007];  setmem 007 0200;  run 0100
    checkcycles 4; checkreg pc 0201; checkmem 007 0200
setreg ac 012345; setreg l 0; setmem 0100 [JMP *020];  setmem 020 0110;  run 0100
    checkcycles 4; checkreg pc 0111; checkmem 020 0110

# DAC
setreg ac 1;       setreg l 0; setmem 0100 [DAC 0110];                            run 0100
    checkcycles 3; checkreg pc 0102; checkmem 0110 1
setreg ac 1;       setreg l 1; setmem 0100 [DAC 0110];                            run 0100
    checkcycles 3; checkreg pc 0102; checkmem 0110 1
setreg ac 0177777; setreg l 0; setmem 0100 [DAC *0110];    setmem 0110 0120;      run 0100
    checkcycles 4; checkreg pc 0102; checkmem 0120 0177777
setreg ac 0177777; setreg l 1; setmem 0100 [DAC *010|HLT]; setmem 010 0120;       run 0100
    checkcycles 4; checkreg pc 0102; checkmem 0121 0177777; checkmem 010 0121
setreg ac 0;       setreg l 1; setmem 0100 [LAW 0120|DAC 010|LWC 1|DAC *010|HLT]; run 0100
    checkcycles 8; checkreg pc 0105; checkreg ac 0177777; checkmem 0121 0177777; checkmem 010 0121

# XAM
setreg ac 0; setreg l 0; setmem 0110 012345; setmem 0100 [XAM 0110];                    run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 012345; checkmem 0110 0
setreg ac 0; setreg l 1; setmem 0110 012345; setmem 0100 [XAM 0110];                    run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 012345; checkmem 0110 0
setreg ac 0; setreg l 1; setmem 0110 0200; setmem 0200 012345; setmem 0100 [XAM *0110]; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 012345; checkmem 0200 0
setreg ac 0123; setreg l 0; setmem 010 0200; setmem 201 0; setmem 0100 [XAM *010];      run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0201; checkmem 0201 0123

# ISZ
setreg ac 1; setreg l 0; setmem 0100 [ISZ 0110|HLT|HLT]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkmem 0110 1
setreg ac 1; setreg l 1; setmem 0100 [ISZ 0110];         setmem 0110 0177776; run 0100
    checkcycles 3; checkreg pc 0102; checkmem 0110 0177777
setreg ac 1; setreg l 1; setmem 0100 [ISZ 0110|HLT];     setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0103; checkmem 0110 0

setreg ac 0; setreg l 1; setmem 0100 [ISZ *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkmem 0120 1
setreg ac 0; setreg l 1; setmem 0100 [ISZ *0110]; setmem 0110 0120; setmem 0120 0177776; run 0100
    checkcycles 4; checkreg pc 0102; checkmem 0120 0177777
setreg ac 0; setreg l 1; setmem 0100 [ISZ *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0103; checkmem 0120 0

setreg ac 1; setreg l 0; setmem 0100 [ISZ *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkmem 010 0121; checkmem 0121 1
setreg ac 1; setreg l 1; setmem 0100 [ISZ *010]; setmem 010 0120; setmem 0121 0177776; run 0100
    checkcycles 4; checkreg pc 0102; checkmem 010 0121; checkmem 0121 0177777
setreg ac 1; setreg l 0; setmem 0100 [ISZ *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0103; checkmem 010 0121; checkmem 0121 0

# JMS
setreg ac 1; setreg l 0; setmem 0100 [JMS 0110];                                         run 0100
    checkcycles 3; checkreg pc 0112; checkmem 0110 0101
setreg ac 1; setreg l 1; setmem 0100 [JMS *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0122; checkmem 0120 0101
setreg ac 1; setreg l 0; setmem 0100 [JMS *010]; setmem 010 0120; setmem 0121 1;         run 0100
    checkcycles 4; checkreg pc 0123; checkmem 010 0121; checkmem 0121 0101
# test JMS call and return
setreg ac 0; setreg l 1; setmem 0100 [JMS 0110]; setmem 0110 [HLT|JMP *0110]; run 0100
    checkcycles 6; checkreg pc 0102; checkmem 0110 0101

# AND
setreg ac 0; setreg l 0; setmem 0100 [AND 0110]; setmem 0110 1; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 1; setmem 0100 [AND 0110]; setmem 0110 1; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0125252; setreg l 0; setmem 0100 [AND 0110]; setmem 0110 0052525; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [AND 0110]; setmem 0110 0052525; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0052525

setreg ac 0;       setreg l 0; setmem 0100 [AND *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 1; setmem 0100 [AND *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0125252; setreg l 0; setmem 0100 [AND *0110]; setmem 0110 0120; setmem 0120 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [AND *0110]; setmem 0110 0120; setmem 0120 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0052525

setreg ac 0;       setreg l 0; setmem 0100 [AND *010]; setmem 010 0110; setmem 0111 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0111
setreg ac 0177777; setreg l 1; setmem 0100 [AND *010]; setmem 010 0110; setmem 0111 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0111
setreg ac 0125252; setreg l 0; setmem 0100 [AND *010]; setmem 010 0110; setmem 0111 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0111
setreg ac 0177777; setreg l 0; setmem 0100 [AND *010]; setmem 010 0110; setmem 0111 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0052525; checkmem 010 0111

# IOR
setreg ac 0;       setreg l 0; setmem 0100 [IOR 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 0; setmem 0100 [IOR 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0177777; setreg l 1; setmem 0100 [IOR 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777
setreg ac 0125252; setreg l 0; setmem 0100 [IOR 0110]; setmem 0110 0052525; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777
setreg ac 0000777; setreg l 0; setmem 0100 [IOR 0110]; setmem 0110 0177000; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777

setreg ac 0;       setreg l 0; setmem 0100 [IOR *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 0; setmem 0100 [IOR *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0177777; setreg l 1; setmem 0100 [IOR *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 0125252; setreg l 0; setmem 0100 [IOR *0110]; setmem 0110 0120; setmem 0120 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 0000777; setreg l 0; setmem 0100 [IOR *0110]; setmem 0110 0120; setmem 0120 0177000; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777

setreg ac 0;       setreg l 0; setmem 0100 [IOR *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0;       setreg l 0; setmem 0100 [IOR *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 0177777; setreg l 1; setmem 0100 [IOR *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 0125252; setreg l 0; setmem 0100 [IOR *010]; setmem 010 0120; setmem 0121 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 0000777; setreg l 0; setmem 0100 [IOR *010]; setmem 010 0120; setmem 0121 0177000; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121

# XOR
setreg ac 0;       setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 1;       setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 1;       setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 1; setmem 0100 [XOR 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177776
setreg ac 0125252; setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 0052525; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777
setreg ac 0000777; setreg l 0; setmem 0100 [XOR 0110]; setmem 0110 0177000; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777

setreg ac 0;       setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 1;       setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 1;       setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 1; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177776
setreg ac 0125252; setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 0000777; setreg l 0; setmem 0100 [XOR *0110]; setmem 0110 0120; setmem 0120 0177000; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777

setreg ac 0;       setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0;       setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 1;       setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 1;       setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0177777; setreg l 1; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177776; checkmem 010 0121
setreg ac 0125252; setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 0052525; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 0000777; setreg l 0; setmem 0100 [XOR *010]; setmem 010 0120; setmem 0121 0177000; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121

# LAC
setreg ac 1; setmem 0100 [LAC 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0; setmem 0100 [LAC 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0; setmem 0100 [LAC 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777

setreg ac 1; setmem 0100 [LAC *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0; setmem 0100 [LAC *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0; setmem 0100 [LAC *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777

setreg ac 1; setmem 0100 [LAC *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0; setmem 0100 [LAC *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 0; setmem 0100 [LAC *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121

# ADD
setreg ac 0; setmem 0100 [ADD 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 1; setmem 0100 [ADD 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0; setmem 0100 [ADD 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 1; setmem 0100 [ADD 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 2
setreg ac 0; setmem 0100 [ADD 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777

setreg ac 1; setreg l 0; setmem 0100 [ADD 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0; checkreg l 1
setreg ac 1; setreg l 1; setmem 0100 [ADD 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0; checkreg l 0
setreg ac 2; setreg l 0; setmem 0100 [ADD 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1; checkreg l 1
setreg ac 2; setreg l 1; setmem 0100 [ADD 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1; checkreg l 0
setreg ac 2; setreg l 1; setmem 0100 [ADD 0110]; setmem 0110 0177775; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777; checkreg l 1

setreg ac 0; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 1; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 1; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 1; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 1; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 2

setreg ac 0; setreg l 0; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 1; setreg l 0; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkreg l 1
setreg ac 1; setreg l 1; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkreg l 0
setreg ac 2; setreg l 0; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkreg l 1
setreg ac 2; setreg l 1; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkreg l 0
setreg ac 2; setreg l 1; setmem 0100 [ADD *0110]; setmem 0110 0120; setmem 0120 0177775; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkreg l 1

setreg ac 0; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 1; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 0; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 1; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 2; checkmem 010 0121
setreg ac 0; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 1; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkreg l 1; checkmem 010 0121
setreg ac 1; setreg l 1; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkreg l 0; checkmem 010 0121
setreg ac 2; setreg l 0; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkreg l 1; checkmem 010 0121
setreg ac 2; setreg l 1; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkreg l 0; checkmem 010 0121
setreg ac 2; setreg l 1; setmem 0100 [ADD *010]; setmem 010 0120; setmem 0121 0177775; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkreg l 1; checkmem 010 0121

# SUB
setreg ac 0; setreg l 0; setmem 0100 [SUB 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0; setreg l 1; setmem 0100 [SUB 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0
setreg ac 0; setreg l 0; setmem 0100 [SUB 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777
setreg ac 0; setreg l 1; setmem 0100 [SUB 0110]; setmem 0110 1;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 0177777
setreg ac 1; setreg l 0; setmem 0100 [SUB 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 1; setreg l 1; setmem 0100 [SUB 0110]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0; setreg l 0; setmem 0100 [SUB 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1
setreg ac 0; setreg l 1; setmem 0100 [SUB 0110]; setmem 0110 0177777; run 0100
    checkcycles 3; checkreg pc 0102; checkreg ac 1

setreg ac 0; setreg l 0; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0; setreg l 1; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0
setreg ac 0; setreg l 0; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 0; setreg l 1; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777
setreg ac 1; setreg l 0; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 1; setreg l 1; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0; setreg l 0; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1
setreg ac 0; setreg l 1; setmem 0100 [SUB *0110]; setmem 0110 0120; setmem 0120 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1

setreg ac 0; setreg l 0; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0; setreg l 1; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0; checkmem 010 0121
setreg ac 0; setreg l 0; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 0; setreg l 1; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 1;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 0177777; checkmem 010 0121
setreg ac 1; setreg l 0; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 1; setreg l 1; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 0; setreg l 0; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121
setreg ac 0; setreg l 1; setmem 0100 [SUB *010]; setmem 010 0120; setmem 0121 0177777; run 0100
    checkcycles 4; checkreg pc 0102; checkreg ac 1; checkmem 010 0121

# SAM
setreg ac 1;       setmem 0100 [SAM 0110|HLT|HLT]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0102
setreg ac 0;       setmem 0100 [SAM 0110|HLT|HLT]; setmem 0110 0;       run 0100
    checkcycles 3; checkreg pc 0103
setreg ac 0177777; setmem 0100 [SAM 0110|HLT|HLT]; setmem 0110 0177776; run 0100
    checkcycles 3; checkreg pc 0102
setreg ac 0177776; setmem 0100 [SAM 0110|HLT|HLT]; setmem 0110 0177776; run 0100
    checkcycles 3; checkreg pc 0103

setreg ac 1;       setmem 0100 [SAM *0110|HLT|HLT]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0102
setreg ac 0;       setmem 0100 [SAM *0110|HLT|HLT]; setmem 0110 0120; setmem 0120 0;       run 0100
    checkcycles 4; checkreg pc 0103
setreg ac 0177777; setmem 0100 [SAM *0110|HLT|HLT]; setmem 0110 0120; setmem 0120 0177776; run 0100
    checkcycles 4; checkreg pc 0102
setreg ac 0177776; setmem 0100 [SAM *0110|HLT|HLT]; setmem 0110 0120; setmem 0120 0177776; run 0100
    checkcycles 4; checkreg pc 0103

setreg ac 1;       setmem 0100 [SAM *010|HLT|HLT]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0102; checkmem 010 0121
setreg ac 0;       setmem 0100 [SAM *010|HLT|HLT]; setmem 010 0120; setmem 0121 0;       run 0100
    checkcycles 4; checkreg pc 0103; checkmem 010 0121
setreg ac 0177777; setmem 0100 [SAM *010|HLT|HLT]; setmem 010 0120; setmem 0121 0177776; run 0100
    checkcycles 4; checkreg pc 0102; checkmem 010 0121
setreg ac 0177776; setmem 0100 [SAM *010|HLT|HLT]; setmem 010 0120; setmem 0121 0177776; run 0100
    checkcycles 4; checkreg pc 0103; checkmem 010 0121

# CLA
setreg ac 0;       setreg l 0; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0
setreg ac 1;       setreg l 0; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0
setreg ac 1;       setreg l 1; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0
setreg ac 0177777; setreg l 1; setmem 0100 [CLA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0

# CMA
setreg ac 0;       setmem 0100 [CMA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0177777
setreg ac 1;       setmem 0100 [CMA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0177776
setreg ac 0177777; setmem 0100 [CMA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0

# STA
setreg ac 0;       setmem 0100 [STA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0177777
setreg ac 0177777; setmem 0100 [STA]; run 0100 0101; checkcycles 1; checkreg pc 0101; checkreg ac 0177777

# IAC
setreg ac 0;       setreg l 0; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1;       checkreg l 0
setreg ac 1;       setreg l 1; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 2;       checkreg l 1
setreg ac 0177776; setreg l 0; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777; checkreg l 0
setreg ac 0177776; setreg l 1; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777; checkreg l 1
setreg ac 0177777; setreg l 0; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 1
setreg ac 0177777; setreg l 1; setmem 0100 [IAC]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0

# CIA
setreg ac 0;       setreg l 0; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 1
setreg ac 0;       setreg l 1; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0177777; checkreg l 0
setreg ac 1;       setreg l 1; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0177777; checkreg l 1
setreg ac 0177777; setreg l 0; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 1; checkreg l 0
setreg ac 0177777; setreg l 1; setmem 0100 [CIA]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 1; checkreg l 1

# CLL
setreg ac 0; setreg l 0; setmem 0100 [CLL]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0
setreg ac 0; setreg l 1; setmem 0100 [CLL]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0
setreg ac 7; setreg l 0; setmem 0100 [CLL]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0
setreg ac 7; setreg l 1; setmem 0100 [CLL]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0

# CML
setreg ac 0; setreg l 0; setmem 0100 [CML]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 1
setreg ac 0; setreg l 1; setmem 0100 [CML]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0
setreg ac 1; setreg l 0; setmem 0100 [CML]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 1
setreg ac 2; setreg l 1; setmem 0100 [CML]; run 0100; checkcycles 2; checkreg pc 0102; checkreg l 0

# CAL
setreg ac 0;       setreg l 0; setmem 0100 [CAL]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [CAL]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [CAL]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 0
setreg ac 0177777; setreg l 1; setmem 0100 [CAL]; run 0100 0101
    checkcycles 1; checkreg pc 0101; checkreg ac 0; checkreg l 0

# STL
setreg ac 0; setreg l 0; setmem 0100 [STL]; run 0100 checkcycles 2; checkreg pc 0102; checkreg l 1
setreg ac 0; setreg l 1; setmem 0100 [STL]; run 0100 checkcycles 2; checkreg pc 0102; checkreg l 1
setreg ac 7; setreg l 0; setmem 0100 [STL]; run 0100 checkcycles 2; checkreg pc 0102; checkreg l 1
setreg ac 7; setreg l 1; setmem 0100 [STL]; run 0100 checkcycles 2; checkreg pc 0102; checkreg l 1

# ODA
setreg ds 0;       setreg ac 0;       setmem 0100 [ODA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ds 0177777; setreg ac 0;       setmem 0100 [ODA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ds 0;       setreg ac 0177777; setmem 0100 [ODA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ds 0777;    setreg ac 0177070; setmem 0100 [ODA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777

# LDA
setreg ds 0;       setreg ac 0;       setmem 0100 [LDA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ds 0;       setreg ac 0177777; setmem 0100 [LDA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ds 1;       setreg ac 0;       setmem 0100 [LDA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1
setreg ds 0177000; setreg ac 0177777; setmem 0100 [LDA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177000
setreg ds 0177777; setreg ac 1;       setmem 0100 [LDA]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777

# SAL
setreg ac 0;       setreg l 0; setmem 0100 [SAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177776
setreg ac 0177777; setreg l 1; setmem 0100 [SAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177776
setreg ac 0100001; setreg l 0; setmem 0100 [SAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0100002

setreg ac 0;       setreg l 0; setmem 0100 [SAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177774
setreg ac 0177777; setreg l 1; setmem 0100 [SAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177774
setreg ac 0100001; setreg l 0; setmem 0100 [SAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0100004

setreg ac 0;       setreg l 0; setmem 0100 [SAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177770
setreg ac 0177777; setreg l 1; setmem 0100 [SAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177770
setreg ac 0100001; setreg l 0; setmem 0100 [SAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0100010

# SAR
setreg ac 0;       setreg l 0; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 0177777; setreg l 1; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 1;       setreg l 0; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 3;       setreg l 0; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1
setreg ac 0100001; setreg l 0; setmem 0100 [SAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0140000

setreg ac 0;       setreg l 0; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 0177777; setreg l 1; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 03;      setreg l 0; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 07;      setreg l 0; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1
setreg ac 0100001; setreg l 0; setmem 0100 [SAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0160000

setreg ac 0;       setreg l 0; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0;       setreg l 1; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 0177777; setreg l 0; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 0177777; setreg l 1; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177777
setreg ac 07;      setreg l 0; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0
setreg ac 017;     setreg l 0; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1
setreg ac 0100001; setreg l 0; setmem 0100 [SAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0170000

# RAL
setreg ac 0;       setreg l 0; setmem 0100 [RAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 1;       checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 2;       checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [RAL 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177776; checkreg l 1

setreg ac 0;       setreg l 0; setmem 0100 [RAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 2;       checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 4;       checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [RAL 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177775; checkreg l 1

setreg ac 0;       setreg l 0; setmem 0100 [RAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 4;       checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 010;     checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [RAL 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0177773; checkreg l 1

# RAR
setreg ac 0;       setreg l 0; setmem 0100 [RAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0100000; checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 1
setreg ac 0177777; setreg l 0; setmem 0100 [RAR 1]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0077777; checkreg l 1

setreg ac 0;       setreg l 0; setmem 0100 [RAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0040000; checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0100000; checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [RAR 2]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0137777; checkreg l 1

setreg ac 0;       setreg l 0; setmem 0100 [RAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0;       checkreg l 0
setreg ac 0;       setreg l 1; setmem 0100 [RAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0020000; checkreg l 0
setreg ac 1;       setreg l 0; setmem 0100 [RAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0040000; checkreg l 0
setreg ac 0177777; setreg l 0; setmem 0100 [RAR 3]; run 0100
    checkcycles 2; checkreg pc 0102; checkreg ac 0157777; checkreg l 1

# ASZ
setreg ac 1; setmem 0100 [ASZ|HLT]; run 0100
    checkcycles 2; checkreg pc 0102
setreg ac 0; setmem 0100 [ASZ|HLT]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0177777; setmem 0100 [ASZ|HLT]; run 0100
    checkcycles 2; checkreg pc 0102

# ASN
setreg ac 1; setmem 0100 [ASN]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0; setmem 0100 [ASN]; run 0100
    checkcycles 2; checkreg pc 0102
setreg ac 0177777; setmem 0100 [ASN]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0100000; setmem 0100 [ASN]; run 0100
    checkcycles 2; checkreg pc 0103

# ASP
setreg ac 1; setmem 0100 [ASP]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0; setmem 0100 [ASP]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0177777; setmem 0100 [ASP]; run 0100
    checkcycles 2; checkreg pc 0102
setreg ac 0100000; setmem 0100 [ASP]; run 0100
    checkcycles 2; checkreg pc 0102

# ASM
setreg ac 1; setmem 0100 [ASM]; run 0100
    checkcycles 2; checkreg pc 0102
setreg ac 0; setmem 0100 [ASM]; run 0100
    checkcycles 2; checkreg pc 0102
setreg ac 0177777; setmem 0100 [ASM]; run 0100
    checkcycles 2; checkreg pc 0103
setreg ac 0100000; setmem 0100 [ASM]; run 0100
    checkcycles 2; checkreg pc 0103

# LSZ
setreg l 0; setmem 0100 [LSZ]; run 0100
    checkcycles 2; checkreg pc 0103
setreg l 1; setmem 0100 [LSZ]; run 0100
    checkcycles 2; checkreg pc 0102

# LSN
setreg l 0; setmem 0100 [LSN]; run 0100
    checkcycles 2; checkreg pc 0102
setreg l 1; setmem 0100 [LSN]; run 0100
    checkcycles 2; checkreg pc 0103

# not tested here, interacting with other hardware
# DON
# DSF
# DSN
# KSF
# KSN
# RSF
# RSN
# TSF
# TSN
# SSF
# SSN
# HSF & HSN
setreg ac 0; mount ptr out_tests/test_out_10.ptp; device ptr on
        setmem 0100 [HON|HSF|JMP 0101|LAW 0|HRB|NOP|HSN|JMP 0106|JMP 0101]
        run 0100 0105; checkreg ac 1; checkreg pc 0105
        run 0100 0105; checkreg ac 2; checkreg pc 0105
        run 0100 0105; checkreg ac 3; checkreg pc 0105
        run 0100 0105; checkreg ac 4; checkreg pc 0105
        run 0100 0105; checkreg ac 5; checkreg pc 0105
        run 0100 0105; checkreg ac 6; checkreg pc 0105
        run 0100 0105; checkreg ac 7; checkreg pc 0105
        run 0100 0105; checkreg ac 8; checkreg pc 0105
# DLA
# CTB
# DOF
# KRB
# KCF
# KRC
# RRB
# RCF
# RRC
# TPR
# TCF
# TPC
# HRB
# HSF - read 7 bytes (values 1 to 7) from PTR file 'out_tests/test_out_10.ptp'
setreg ac 0; mount ptr out_tests/test_out_10.ptp; device ptr on
    setmem 0100 [HON|HSF|JMP 0101|LAW 0|HRB|NOP|HSN|JMP 0106|JMP 0101]
    run 0100 0105; checkreg ac 1
    run 0105 0105; checkreg ac 2
    run 0105 0105; checkreg ac 3
    run 0105 0105; checkreg ac 4
    run 0105 0105; checkreg ac 5
    run 0105 0105; checkreg ac 6
    run 0105 0105; checkreg ac 7
    run 0105 0105; checkreg ac 8
    checkreg pc 0105
# HOF
# HON
# STB
# SCF
# IOS
# PSF
# PPC
setreg ac 0; mount ptp test_out_3.ptp; device ptp on
    setmem 0100 [LAW 1|PSF|JMP 0101|PPC|IAC|JMP 0101]
    run 0100 0104; checkreg ac 1
    run 0104 0104; checkreg ac 2
    run 0104 0104; checkreg ac 3;
    checkreg pc 0104
    checkfile test_out_3.ptp out_tests/test_out_3.ptp

setreg ac 0; mount ptp test_punch_10.ptp; device ptp on
    setmem 0100 [LWC 10|DAC 1|LAW 1|PSF|JMP 0103|PPC|IAC|ISZ 1|JMP 0103|HLT]; run 0100
    checkreg ac 013; checkreg pc 0112
    checkfile test_punch_10.ptp out_tests/test_out_10.ptp

######
# more complicated tests using PTP/PTR
######

# read 5 values from out_tests/test_out_10.ptp and put into memory at address 0130-0134
setmem 0100 [HON|HSF|JMP 0101|LAW 0|HRB|DAC *010|HSN|JMP 0106|JMP 0101];
    setmem 010 0127; mount ptr out_tests/test_out_10.ptp
    run 0100 0110; checkmem 0130 01; checkmem 010 0130; checkreg ac 01
    run 0110 0110; checkmem 0131 02; checkmem 010 0131; checkreg ac 02
    run 0110 0110; checkmem 0132 03; checkmem 010 0132; checkreg ac 03
    run 0110 0110; checkmem 0133 04; checkmem 010 0133; checkreg ac 04
    run 0110 0110; checkmem 0134 05; checkmem 010 0134; checkreg ac 05
    dismount ptr; checkreg pc 0110 # test
    dumpmem test_memdump.txt 0,0140

# and lots of IOT instructions ...

# test operation of pyasm
include pyasm.dsl

# test CPU time
include cpu_time.dsl
