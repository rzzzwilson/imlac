This directory holds a simulation of the Imlac PDS-1 machine, mostly written
in C.  There will be object-like implementations of parts of the machine:

* main CPU (cpu.c and cpu.h)
* display CPU (dcpu.c and dcpu.h)
* memory (memory.c and memory.h)
* I/O devices (papertape reader/punch, etc) (devices.c and devices.h)

Main CPU
--------

This is designed to be a portable CPU simulation that will be called to
execute Imlac code that has already been loaded into memory.  This code
could be called by any other language that can call C code, eg, python.

The eventual aim is for the code to advertise as a set of entry points:

    run(int num_instructions, int breakpoint);

where "num_instructions" is the number of CPU instructions to execute
before returning, and "breakpoint" is a breakpoint address to return after
executing the instruction at that address.  This ignores the number of
display CPU instructions.

There will be other entry points to set/get register values, memory, etc.
