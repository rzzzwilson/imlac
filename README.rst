This directory holds software, documentation and code to simulate a
PDS-1 graphics minicomputer.
 
Directories are:

* idasm - an interactive disassembler
* imlac - the IMLAC simulator (cpu/dcpu/memory, etc)
* imlac_sdl - test of using SDL2 for the display
* pyasm - an assembler, written in python
* software - various bits of IMLAC software in various forms.
