pyasm Assembler
===============

The IMLAC external assembler, **pyasm**, is very simple.  It may be extended
later and possible extensions are described at the end of this document.

Overview
========

The assembler is a standard two-pass assembler.

Each line of input has the following form:

::

    label opcode field

A *label* must start in column 1, begin with an alphabetic character and is
followed by alphanumeric characters.

An *opcode* is either a pseudo-op or one of the opcodes in the
`Programming Guide <https://github.com/rzzzwilson/pyasm/blob/master/docs/PDS-1D_ProgrammingGuide.pdf>`_.

A *field* is an expression which evaluates to a numeric value.  A *field*
is required by some *opcodes* and not by others.

A *comment* starts with a '**;**' character and continues to the end of the line.

Everything handled by the assembler, except strings, are case-insensitive.

As an example, here is a small file from the pyasm test code:

::

    ;-------------------------------
    ; Just loop in place - CPU speed test.
    ;-------------------------------
            org 0100                ; 
    start   law start/2             ; 
            jmp start+1             ; 
    ;-------------------------------
            end start

Dot
===

Most assemblers have the idea of the current location in the assembled
code being represented by a special symbol.  In **pyasm** this symbol
is the *dot*, represented in the source as a '**.**' symbol.

We could rewrite the above example to use *dot*:

::

    ;-------------------------------
    ; Just loop in place - CPU speed test.
    ;-------------------------------
            org 0100                ; 
    start   law start/2             ; 
            jmp .                   ; jump to current address
    ;-------------------------------
            end start

Pseudo-ops
==========

An **opcode** may be a pseudo-op.  The acceptable pseudo-ops are:

::

    ORG         defines the assembly address for following code
    EQU         set a label to a value
    BSS         allocate uninitialized words
    DATA        generate a word with a value
    ASCII       generate an ASCII string
    ASCIIZ      generate an ASCII string zero byte terminated
    END         indicates the end of the assembly

ORG
---

Must not have a *label* and must have a *field* value.  Sets the assembler
**dot** value to the *field* expression:

::

            org     0100

sets *dot* to octal 0100 (decimal 64).

EQU
---

This pseudo-op must have a *label* and a *field* value.  It sets the label value
to be the *field* value:

::

    test    equ     4
            law     test    ;load the AC with 4

BSS
---

This pseudo-op allocates a *field* number of uninitialized words.  There may be
a *label* and that label has a value of the address of the first generated word:

::

            dac     buffer
    buffer  bss     10

DATA
----

Generates one word of data containing the *field* value.  May have a label:

::

            lac     test
    test    data    017777

ASCII
-----

This pseudo-op allocates a string packed into words.  The *field* must be a
single- or double-quoted string.  Each generated word contains two characters
from the *field* string.  The last generated word may contain a zero *pad-byte*
to fill out the word.

Escape characters in the string are of the usual *\X* form.  The accepted escape
sequences are:

=============== ========= =============
Escape Sequence Result    Value (octal)
--------------- --------- -------------
``\n``          newline   012
--------------- --------- -------------
``\\``          backslash 0134
=============== ========= =============

If the pseudo-op has a *label* that label has a value of the address of the first
generated word.

The string need **not** have a zero byte at the end!

::

            org     0100
    start   law     str
            jms     print
            hlt
    str     ascii   'Hello, world!\n'
            data    0
            end     start


ASCIIZ
------

This pseudo-op allocates a string packed into words with a guaranteed zero byte
after the last character of the string.  The *field* must be a single- or
double-quoted string.  Each generated word contains two characters from the
*field* string.  The last generated word will contain one or two zero bytes.

Escape characters in the string are of the usual *\X* form.  The accepted escape
sequences are:

=============== ========= =============
Escape Sequence Result    Value (octal)
--------------- --------- -------------
``\n``          newline   012
--------------- --------- -------------
``\\``          backslash 0134
=============== ========= =============

If the pseudo-op has a *label* that label has a value of the address of the first
generated word.

::

            org     0100
    start   law     str
            jms     print
            hlt
    str     asciiz  'Hello, world!'
            end     start


END
---

Indicates the end of the assembly.  If *field* is specified then that is the
program start address which may cause the blockloader to automatically start
the program when loaded.  A *label* cannot be used:

::

            org     0100
    start   jmp     .
            end     start

Usage
=====

::

    An assembler for the Imlac simulator.
    
    Usage: pyasm [ -h ] [ -l <listfile> ] [ -o <outputfile> ] <asmfile>
    
    Where <asmfile>     is the file to assemble,
          <outputfile>  is the output PTP file
          <listfile>    is the optional listing file

    If <outputfile> is not specified the output filename is the input
     <asmfile> with any extension removed and a .ptp extension added.

Listing File
============

The listing file contains the input assembler source adorned with line numbers,
generated words and addresses.

For example, this source file:

::

    ; an assembler test file
            org     0100
    
    test    equ     4
    print   equ     0200        ; print subroutine address
    
    start   law     test
            dac     save
            law     1
            add     save2
            dac     save2
            law     string
            jms     print
            hlt
    
    save    bss     1
    save2   data    3
    string  ascii   'Test'
            data    0
    
            end     start

generates this listing file:

::

                      0001: ; an assembler test file
              00100   0002:         org     0100
                      0003: 
              00004   0004: test    equ     4
              00200   0005: print   equ     0200        ; print subroutine address
                      0006: 
    0004004   00100   0007: start   law     test
    0020110   00101   0008:         dac     save
    0004001   00102   0009:         law     1
    0064111   00103   0010:         add     save2
    0020111   00104   0011:         dac     save2
    0004112   00105   0012:         law     string
    0034200   00106   0013:         jms     print
    0000000   00107   0014:         hlt
                      0015: 
                      0016: save    bss     1
    0000003   00111   0017: save2   data    3
    0052145   00112   0018: string  ascii   'Test'
    0071564   00113    
    0000000   00114   0019:         data    0
                      0020: 
              00100   0021:         end     start


Later Extensions
================

Literals
--------

The assembler that came with the Imlac at Sydney University had a rather nice
*literal* mechanism.  This code:

::

            org     0100
    
    start   lac     test
            hlt
    test    data    0177777
    
            end     start
    
simply loads the AC with the value 0177777.  Of course, we can't use the *LAW*
instruction to do this as that can't load the AC with a value needing 16 bits.
So we have to create a word somewhere, give it a label and load the value from
there with the *LAC* instruction.

The literal mechanism makes this simpler.  We can simply do:

::

            org     0100
    
    start   lac     (0177777)
            hlt
    
            end     start

The LAC field is now the value we want enclosed by parentheses.  This is a
**literal field**.  The literal mechanism takes the value inside the
parentheses, generates a word with that value somewhere in memory and replaces
the literal field with the address of the generated word.

A listing file from the assembler that handles literals would look something
like this:

::

              00100   0001:         org     0100
                      0002: 
    0060102   00100   0003: start   lac     (0177777)
    0000000   00101   0004:         hlt
                      0005: 
              00100   0006:         end     start

              ------- literals
    0177777   00102                 data    0177777     ; from line 3

I haven't decided when literals would be dumped.  Certainly on the END
pseudo-op, but possibly on ORG and other pseudo-ops.

But it was even cooler than that.  You could use expressions and even multiple
lines of assembler instructions:

::

            lac     (0177777)
            lac     (FRED + 2)
            lac     (LAW    012)
            law     (ASCIIZ 'Hello, World!')
            jms     (DATA   0
                     LAW    0
                     JMP    *.-2)   ; ugly!

This was super cool!  It's something I want **pyasm** to do, but I'm not quite
sure how to do it.  I'll probably try organizing the assembler so it can be
recursive and just call it passing all the text between the parentheses.

I also need to think about literals *within* literals.

I'll also need to think/worry about allowing labels in the literal text.  That
would take away the awkwardness above when the literal subroutine needs to
return.  In that case, how do we recognize a label?  It *can't* start in column
1!  Maybe ignore all text below and to the left of the opening parenthesis.
Then we could do:

::

            jms     (LOAD0  DATA    0
                            LAW     0
                            JMP     *LOAD0)

I could also add a special label that holds the address of the first word of a
literal.  Top-level code not inside a literal would see that label as equating
to the address of the previous ORG opcode.

::

            jms     (DATA    0
                     LAW     0
                     JMP     *$LIT)

Macros
------

Maybe.
