# IMLAC implementationn

These pages contain details of the IMLAC implementation.

## Boot ROM

The IMLAC machine had a boot ROM that was used to load papertapes, files from a
TTY, files from a cassette system or to boot a floppy system.  This ROM was
placed in the address space from 040 to 077 octal.  The ROM address space could
be write-protected or could be overwritten.

The IMLAC emulator tries to allow all types of boot code and also allows the
ROM memory addresses to be overwritten, if required.

## Executable Block Data

The existing IMLAC documentation and examples of binary files suggest that there
were at least two layouts of data on executable papertape files.

The *loading.pdf* document at IMLAC/images/IMLACdocs says that after the block
loader, data is arranged in blocks consisting of four parts:

* a byte containing the data word count
* a word of block base address
* one or more data words
* one word of checksum

where the checkum is the sum of all the data words modulo 077777.

Various blockloader source files such as IMLAC/misc/blkldr1.asm suggest the
above block format but say the checksum is the sum of all data words, but if
the sum of each word overlows 16 bits increment the sum.

Other source files (binldr.i and block-loader.asm in IMLAC/misc) say that a
block consists of:

* a **word** of base or load address
* a **complemented** word of data words count
* a word of checksum
* one or more data words

and the checksum is the complement of the sum of all data words.  This means
the blockloader code could add the checksum and all data words and should get
zero.

The latter layout appears to be more flexible and may be a more recent example
of how it was done, back in the day.  So write any code that generates or reads
PTP data to use this system.

There are lots of old PTP files around that could use any of the above schemes
or even something not documented but that shouldn't matter.  The only thing
that **must** work is the bootstrap code in ROM used to load the blockloader.
