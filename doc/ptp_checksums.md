# Checksums on papertape data

There are two types of papertape data formats (so far).

## c8lds

This format has, in order, this data:

* 1 8bit count of data words
* 1 16bit word of load address
* N "count" 16bit words of data
* 1 16bit word, the expected checksum

The checksum is calculated from the "count" data words alone.
The checksum value starts at 0, each data word is added to the checksum
and if there is a 16bit overflow 1 is added to the checksum value.
Then the checksum is masked to 0xffff, ready for the next data word.

## lc16sd

This format has, in order, this data:

*  1 16bit word of load address
*  1 16bit word of data word count (complemented)
*  1 16bit word of expected checksum
*  N 16bit words of data (N = "count", above)

The checksum...

