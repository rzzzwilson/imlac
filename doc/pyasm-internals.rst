IMLAC Assembler
===============

We will need an assembler, of course.  Written in python, of course.

Design
------

The assembler will be a two-pass assembler.  The first error report stops the
assembly.

With an eye to implementing Imlac-style literals, a code file will
be broken into *RELOC blocks* which will contain all the code between two ORG
pseudo-ops and be tagged with the address of the first ORG opcode.  There
will be one or more ORG blocks in a file.

A RELOC block without an address will be a literal block and the address will
be allocated before the backpatch phase.

RELOC Block
===========

A *RELOC* block will be generated for each code block found during assembly.
A code block can be:

* code between two ORG opcodes or an ORG and END opcode pair
* a single word literal
* a multi-word literal

A RELOC block is an instance of the RELOC class with attributes:

* base - address of the base of the block, **None** if not yet known (ie, a literal)
* code - a list of generated code words
* reloc - a list of relocation tuples: (offset, value)

The *reloc* attribute is a list of tuples containing:

* offset - the offset from the beginning of the *code* list for the relocation
* value - the value to plug into the offset word

**value** may be a binary value or a string containing the name of a symbol
in the symbol table.

RELOC examples
==============

Here we show a few different cases and the types of RELOC blocks used.

No relocation
-------------

::

        org 0100
        law 3
        hlt
        end

The RELOC block for this fragment of code will have:

* .base = 0100
* .code = [0040003, 0000000]
* .reloc = []

Local relocation
----------------

Here we have a forward reference to a label within the same RELOC block:

::

        org 0100
        jmp tom
        hlt
    tom hlt
        end

The RELOC block for this fragment of code will have:

* .base = 0100
* .code = [0100000, 0000000, 0000000]
* .reloc = [(0, 'tom')]

and the symbol table will have a value of 0102 for 'tom'.

Non-local relocation
--------------------

A forward reference to a label in another RELOC block:

::

        org 0100
        jmp tom
        org 0200
    tom hlt
        end

The first RELOC block will have:

* .base = 0100
* .code = [0100000]
* .reloc = [(0, 'tom')]

and the symbol table will have a value of 0200 for 'tom'.

Single word literal
-------------------

The single-word literal can look like this:

::

        org 0100
        lac (data 0177777)
        hlt
        end

The RELOC block for the code block at 0100 will be:

* .base = 0100
* .code = [0600000, 0000000]
* .reloc = [(0, '$L1')]

The literal RELOC block will be:

* .base = None
* .code = [0177777]
* .reloc = []

The **$L1** label will be added to the symbol table with an unknown value
which will be defines when literals are declared.  Note that the label *$L1* is
not a legal label that can be used by the user.

Multi word literal
------------------

The multiple-word literal can look like this:

::

        org 0100
        jms (test   data    0
                    law     2
                    jmp     *test)
        hlt
        end

The RELOC block for the code block at 0100 will be:

* .base = 0100
* .code = [0340000, 0000000]
* .reloc = [(0, '$L1')]

The literal RELOC block will be:

* .base = None
* .code = [0000000, 0040002, 0110000]
* .reloc = [(2, 'test')]

Phases
------

The assembler phases are:

0. Read all file lines into memory
1. Break code into source blocks between ORG/END pseudo-ops
1. Create RELOC blocks, one or more per source blocks
2. Check for undefined things in the symbol table
3. Allocate addresses to literal CODE blocks
4. Fix relative addresses in literal blocks
5. Backpatch all code blocks
6. Emit PTP data


Symbol Table
------------

There will be a global symbol table implemented as a dictionary:

::

    symtab = {<name>: [<value>, <line#>], ... }

The **<value>** field is the value associated with the **<name>** symbol, and
the **<line#>** field is the line number of the first reference to the <name>.

If the <name> first reference is not a definition of <name>, then <value> will
be None.  When the <name> **is** defined the <value> field will be filled with
the value.

Internal Structure
------------------

To allow easy implementation of literals, the actual code that assembles a
block must be callable **while assembling a block**.  This is because while we
are assembling a block we might run across a literal and need to assemble that
literal without upsetting the assembly of the enclosing block.

The inputs to the low-level assembler function will be:

* code - a list of lines of source block
* line - line number of the first line in *code*
* indent - a number indicating where the assumed left column is

The *indent* parameter is normally **0**.  When a literal is being assembled
this number will be non-zero and is the column number of the initial opening
parenthesis.  This number allows the assembler to identify labels in literal
code.  Note that **ANY** non-space text to the left of the indent column will
be marked as an error.

Assembled Code Block list
-------------------------

Assembled code blocks will be kept in a list called **CodeBlocks** in the order
they are assembled.  Assembled code blocks may or may not have a block base
address.

If code blocks *overlap*, later blocks will overwrite earlier blocks.  Note that
this situation will be arranged by the programmer (accidentally or on purpose)
as literal blocks will not (by definition) overlap any other block.

Literals
--------

We won't implement this initially, but the way literals will work is defined.

The simplest literal is:

::

            LAC     [0177775]
            JMP     TEST

Note that we can't use the **LAW** instruction as the immediate value has a
maximum of 12 bits.  Normally we would do something like:

::

           LAC     BIGVAL
           JMP     TEST
           ...
    BIGVAL DATA    0177775

The literal construct allows the programmer to easily do the same thing except
the programmer:

1. doesn't need to generate a unique label
2. doesn't need to create the DATA line

The possible downside is that the programmer has no idea where the data will
be created.

This literal idea can be extended to something like:

::

            ISZ     COUNT
            JMS     [ENTRY   DATA    0      ; return address stored here
                             LAW     1
                             ...            ; all sorts of code here
                             JMP     *ENTRY]
            ...

A problem with the above literal style is that we have to handle labels.  Do
we behave like python and note which column the starting '[' is in and ignore
any text before that in following lines?  Or not allow labels in a literal?

One nice side effect of literals (if we can manage it) is for the symbols
defined within the literal to be not entered into the global symbol table.
Or is it better not to do that and have literal symbols visible globally?

A literal block will have an entry in the symbol table but the form of the name
will be one that is illegal for the programmer to use.  Maybe **$L0001**?

Do we allow the programmer to access the base address in literal code?  This
would allow the multi-line literal above to be written in a simpler way:

::

            ISZ     COUNT
            JMS     [        DATA    0      ; return address stored here
                             LAW     1
                             ...            ; all sorts of code here
                             JMP     *$ORG]
            ...

This could also be used in normal code blocks - the **$ORG** thing would
evaluate to the ORG address of the block.  A literal block has an assumed ORG
address that is not known until the backpatch stage.  The $ORG symbol would
simply be replaced with the base address of the current code block.  This would
be the hidden label **$L0001** that would be in the symbol table.  Each literal
block would have a unique label of this form.

Did this find any use in **real** code?  Yes, I have used this literal block
approach in actual production code: the TRAC interpreter on the Sydney
University PDS-4.

Literal Blocks
--------------

Literal blocks are just like ORG blocks except their base address is unknown.
So instructions like **JMP .-1** can't be finalized as the address "." is
currently unknown.  This problem exists for all relative addresses in the
literal.

We know that "." is block base adress plus offset in the block.  The offset is
known but the base is not.  So we need to remember enough information to fix
the generated code during backpatch.  We do this by keeping a special list of
literal patches:

::

    litpatch = [[coderef, patch_offset, addr_offset], ... ]

Here **coderef** references the literal code block we want to patch,
**patch_offset** is the offset in the code block of the word to patch and
**addr_offset** is the relative address from the patched word.

Literal use within a literal block are handled in the usual way with the
backpatch list.

Address allocation
------------------

This stage consists of giving all blocks without a base address a physical
address.  The user will have fitted all her code into ORG blocks, so those
addresses are known.  We must fit all the literal blocks into unused space.

This stage could raise an error if we exhaust all unused space and have one
or more literal blocks unplaced.  This is the 4K limit.

Macros
------

Assembler macros would not be implemented at first.

Need to investigate the existing assembler doc to find out how macros were used.

Failing that, make up the macro syntax.

Assembler Syntax
----------------

I rememember the assembler on the PDS-4 Imlac at Sydney University in 1977/1978.
The original assembler source code samples I have are for a much earlier version
of the assembler and the syntax differs remarkably.  I'll implement what I can
remember of the PDS-4 assembler.

The assembler operates on ASCII files (no Unicode!).  Except within string
constants, case is immaterial.

A line has the form:

::

    label   opcode  address         comment

Where all fields are optional.

A **label** starts in column 1 and consists of 1 to 6 printing characters,
starting with an alphabetic.  The remaining characters are alphanumeric.

The **opcode** may be a true opcode such as **LAW** or a pseudo-opcode like
**ORG**.  The accepted pseudo-opcodes are:

::

            ORG  address     start of new code block with address "address"
            DATA value       a data word containing a "value" (number or 'delimited string)
    label   EQU  value       defines "label" to be "value"
            END  address     end of the assembler file, with optional start address


Numbers that start with a "0" are octal numbers, other numbers are decimal.

The **address** field (if present) may be a numeric constant, label reference,
or any simple expression containing numeric constants and/or label references.
The special symbol "." means the current address in the code block.  An address
field may have a prefix "*" which means the address is indirect.

The BNF description of an address field is:

::

    <address>    ::-  ["*"] <expression>
    <expression> ::-  <constant> | <value> [ ('+'|'-') <constant> ]
    <value>      ::-  '.' | <label> | $ORG
    <constant>   ::-  (0-9)*

For example:

::

    2
    -3
    FRED
    TABLE + 2
    .+1
    .-2
    OFFSET - 2

The **comment** field starts with a ";" character and extends to the end of
the line.

A line that has none of the above fields or only a comment field is ignored.

Listing File
------------

If requested, a listing file will have the following form:

::

                      0001:         ORG     0100
                      0002:
    yyyyyyy   xxxxx   0003: START   LAW     1              ;
    yyyyyyy   xxxxx   0004:         HLT                    ;
                      0005:
              xxxxx   0006:         END     start          ; xxxxx is address of START

where **xxxxx** is the address of the instruction.  In the code above the
address of the LAW instruction is 00100 (the ORG address).

The **yyyyyyy** field is the generated code at address xxxxx.

The END statement **xxxxx** value is the start address (if one is given).

Assembler Errors
----------------

The assembler will abort on the following errors:

* Invalid syntax
* Address field overflow
* Out of literal space
* Missing input file
* Something else (system errors, eg, unrecognized option, can't write output file)

PROBLEMS
--------

How do we handle this forward references?

::

            JMS     *TABLE+2
            ...
    TABLE   DATA    func1
            DATA    func2
            DATA    func3
            DATA    func4

That is, how do we handle forward references that are an expression?  We need to
remember the offset as well (+2, in this case).

