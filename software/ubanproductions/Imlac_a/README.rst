The files here are are definitely short vector programs and such:

* Imlac_a/binldr.i - Source for the short vector binary loader - binldr.i (1861)
* Imlac_a/binldr.list - Assembled source for the short vector binary loader - binldr.list (4059)
* Imlac_a/check - A short vector game of checkers - check (5804)
* Imlac_a/check.doc - Documentation for the short vector game of checkers - check.doc (2055)
* Imlac_a/snarf - A short vector tiger/prey game - snarf (3066)
* Imlac_a/snarf2.i - A patch to the short vector tiger/prey game - snarf2.i (475)
* Imlac_a/sts - A short vector terminal program - sts (3972)
* Imlac_a/stsold - An older version of the short vector terminal program - stsold (3820)
* Imlac_a/tank - A short vector tank battle game - tank (3584)
* Imlac_a/tank.doc - Documentation for the short vector tank battle game - tank.doc(1784)
* Imlac_a/tank2.i - A patch to the short vector tank battle game - tank2.i(162)
