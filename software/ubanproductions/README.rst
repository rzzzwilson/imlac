The software here was copied from http://ubanproductions.com/imlac.html .

The directories here mirror the directories Tom Uban uses to store files:

* Imlac - programs (short and long vector)
* Imlac_a - short vector programs
* ImlacPTP - paper tape image files
* ImlacDocs - some documentation files
