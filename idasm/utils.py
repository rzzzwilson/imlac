#!/usr/bin/env python

"""
Various utility routines.
"""

import sys


def warn(msg):
    print(msg)

def abort(msg):
    print(msg)
    sys.exit(1)

if __name__ == '__main__':
    warn("A warning message.")
    abort("An abort message.")
