#!/usr/bin/env python

"""
Import an imlac binary file.
"""

import sys
import getopt
import struct
import PyQt5 as pyqt
import mem
import disasmdata
import utils


# address where next word will be loaded into memory
Dot = 0

# dictionary of memory "chunks"
# {<base_address>: [word1, word2, ...], ...}
Memory = {}

# size of memory configured
MemSize = 0o4000

# size of block loader
LoaderSize = 0o100


def doblockloader(f, word, mymem, save):
    """Read block loader into high memory.

    f      file handle to read from
    word   the word we have already read
    mymem  Mem() object to store loader in
    save   True if blockloader is to be stored in "mymem"

    "mymem" is updated.
    """

    ldaddr = MemSize - LoaderSize
    if save:
        mymem.add(ldaddr, word)
    ldaddr += 1
    numwords = LoaderSize - 1     # have already read one word in
    if save:
        mymem.add(ldaddr, word)
    while numwords > 0:
        word = readword(f)
        if save:
            mymem.add(ldaddr, word)
        ldaddr += 1
        numwords = numwords - 1

def calc_checksum(csum, word):
    """Calculate new checksum from word value.

    csum    old checksum value
    word  new word to include in checksum

    Returns the new checksum value.
    """

    csum += word
    return csum & 0xffff


def pyword(word):
    """Convert a 16bit value to a real python integer.

    That is, convert 0xFFFF to -1.
    """

    body = word & 0x7fff
    if word & 0x8000:
        return -body
    return body


def update_checksum(csum, word):
    """Update checksum with "word"."""

    csum += word
    if csum & 0x10000:
        csum = (csum & 0xffff) + 1;
    return csum

def dobody_c8lds(f, mymem, save):
    """Read all of file after block loader.  Only handles C8LDS data.

    f      input file handle
    mymem  the mem.Mem() dict to store data in
    save   True if body code is to be saved in "mymem"

    Returns an updated mem.Mem() object containing the input code
    and a start address: (mem, start, ac).
    If a start address is not specified, "start" and "ac" are both None.

    If there was a checksum error, just return None.

    Reads "c8lds" format files:
        * zeroes
        * blockloader
        *     zeroes        # zero or more
        *     c8lds body    #     of these
        * zeroes
        * EOF body          # any size block with load address 0177777
    """

    start_address = None

    # read block(s)
    while True:
        # skip zeros and get first 8bit byte, the body data count
        count = skipzeros(f)

        # then get load address
        # negative load address is end-of-file
        ldaddr = readword(f)
        if ldaddr & 0x8000:
            break

        # read data block, calculating checksum
        csum = 0
        csum = update_checksum(csum, ldaddr)
        csum = update_checksum(csum, count)
#        csum = ldaddr                           # start checksum with base address
#        csum = (ldaddr + count) & 0xffff        # start checksum addition
        print(f"start of data block, {count=:04o} (0x{count:03x}), {ldaddr=:06o} (0x{ldaddr:04x})i, {csum=:06o} (0x{csum:04x})")
        for _ in range(count):
            word = readword(f)
            csum = update_checksum(csum, word)
            print(f"data word: {ldaddr=:06o}, {word=:06o} (0x{word:04x}), {csum=:06o} (0x{csum:04x})")
#            csum = (csum + word) & 0xffff
            if save:
                mymem.add(ldaddr, word)
            (op, fld) = disasmdata.disasmdata(word)
            mymem.putOp(ldaddr, op)
            mymem.putFld(ldaddr, fld)
            ldaddr += 1

        # read and check the checksum
        csum_word = readword(f)
        print(f"{csum_word=:06o} (0x{csum_word:04x}), {csum=:06o} (0x{csum:04x})")
        csum = update_checksum(csum, csum_word)
#        csum = (csum + csum_word) & 0xffff      # add checksum word
        if csum != 0:
            utils.warn(f"Checksum error, got {csum:06o} (0x{csum:04x}), expected 0")
            return None

    # check for real start address
    if ldaddr != 0o177777:
        # actual start address
        ac = readword(f)
        return (mymem, ldaddr & 0x7ffff, ac)

    return (mymem, None, None)

def ptpimport(filename, blockloader, code):
    """Import data from PTP file into memory.

    filename     the PTP file to read
    blockloader  True if blockloader is returned
    code         True if body code is returned

    Returns a memory object containing blockloader data or body code
    data, or both.
    """

    try:
        f = open(filename, "rb")
    except IOError as e:
        print(e)
        return 3

    # create Mem() object to store data
    mymem = mem.Mem()

    # find and save the block loader, if required
    byte = skipzeros(f)
    word = (byte << 8) + readbyte(f)
    doblockloader(f, word, mymem, save=blockloader)

    # now read all the data blocks
    result = dobody_c8lds(f, mymem, save=code)
    mymem.dump("After dobody_c8lds:")
    print(f"dobody_c8lds() returned {result}")
    if result is None:
        return (mymem, None, None)

    return result

def readbyte(f):
    global Dot

    try:
        byte = f.read(1)
    except IOError as e:
        print(e)
        sys.exit(10)
    except EOFError as e:
        print(e)
        sys.exit(11)
    Dot += 1
    if len(byte) > 0:
        val = struct.unpack("B", byte)
        return val[0]
    else:
        return 0

def readword(f, first_byte=None):
    """Return the next word from the input file.

    f           handle of the input file
    first_byte  value of first byte of word, if any

    Convert 16bit values to python integers
    """

    if first_byte is None:
        first_byte = readbyte(f)

    return (first_byte << 8) + readbyte(f)

def skipzeros(f):
    while True:
        val = readbyte(f)
        if val != 0:
            return val


if __name__ == "__main__":
    print("Processing file 40tp_simpleDisplay.ptp:")
    result = ptpimport("40tp_simpleDisplay.ptp", False, True)
    if result is None:
        print("Error reading input file.")
        sys.exit(10)
    print(f"{result=}")
    sys.exit(0)

    (themem, start, ac) = result
    print(f"{str(dir(themem))=}" % str(dir(themem)))
    if start is not None:
        print("{start=:06o}, {ac=:06o}")
    else:
        print("start=None, ac=None")

    addrlist = themem.keys()
    addrlist.sort()
    for addr in addrlist:
        (word, cycle, type, lab, ref) = mymem[addr]
        print(f"{addr} {type}: {word:05o}")
