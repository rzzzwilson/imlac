#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module takes a memory dictionary of the form:

    { <memstr>: ( <type>, <wordvalue> ), ...}

and a start address and returns a processed dictionary, where the <type>
is changed to one of "main", "mainref", "disp", "dispref" or "ref".
"""


import mem
import disasmd


dispQ = []

def dequeDisp():
    result = None
    if len(dispQ) > 0:
        result = dispQ[0]
        del dispQ[0]
    print(f"processdisplay.enqueDisp: dequed {result:06o}")
    return result

def enqueDisp(address):
    dispQ.append(address)
    print(f"processdisplay.enqueDisp: enqued {address:06o}")

def effAddress(addr, offset):
    return (addr & 0o174000) + offset

def getTargetAddress(code):
    return code & 0o7777

def isDHLT(code):
    return code == 0o000000

def isDEIM(code):
    opcode = code & 0o070000
    return opcode == 0o030000

def isDEIMExit(code):
    hibyte = (code >> 8) & 0o377
    lobyte = code & 0o377
    return isDEIMExitByte(hibyte) or isDEIMExitByte(lobyte)

def isDEIMReturn(code):
    hibyte = (code >> 8) & 0o377
    lobyte = code & 0o377
    return isDEIMReturnByte(hibyte) or isDEIMReturnByte(lobyte)

def isDEIMExitByte(byte):
    return (byte & 0o300) == 0o100

def isDEIMReturnByte(byte):
    return (byte & 0o240) == 0o040

def isDJMP(code):
    opcode = code & 0o070000
    return opcode == 0o060000

def isDJMS(code):
    opcode = code & 0o070000
    return opcode == 0o050000

def isDRJM(code):
    return code == 0o004040

def process(mem, addrlist, newcycle):
    print(f"processdisplay.process: {addrlist=}, {newcycle=}")

    mode = "normal"
    for addr in addrlist:
        enqueDisp(addr)
    while len(dispQ) > 0:
        address = dequeDisp()
        print(f"top of while loop: {address=:06o}, {dispQ=}")

        # check if address is in "defined" memory, ignore if not
        if not mem.haveMem(address):
            print(f"Address {address:06o} not defined, ignored.")
            continue

        if mem.getCycle(address) == newcycle:   # seen it already
            continue

        mem.putCycle(address, newcycle)

        code = mem.getCode(address)
        (op, fld) = disasmd.disasmd(code)
        mem.putOp(address, op)
        mem.putFld(address, fld)
        if mode == "deim":
            if isDEIMExit(code):
                mode = "normal"
            if isDEIMReturn(code):
                break
        else:
            if mem.getRef(address):
                mem.decLab(getTargetAddress(code))
            if isDJMP(code):
                memref = effAddress(address, getTargetAddress(code))
                mem.setRef(address)
                mem.incLab(memref)
                enqueDisp(memref)
                print(f"was DJMP, enqueing {memref:06o}")
            elif isDRJM(code):
                print(f"was DRJM, not enqueing")
            elif isDJMS(code):
                memref = effAddress(address, getTargetAddress(code))
                mem.setRef(address)
                mem.incLab(memref)
                enqueDisp(memref)
                enqueDisp(address + 1)
                print(f"was DJMS, enqueing {memref:06o} and {address+1:06o}")
            elif isDEIM(code):
                mode = "deim"
                enqueDisp(address + 1)
                print(f"was DEIM, enqueing {address+1:06o}")
            elif isDHLT(code):
                print(f"was DHLT, not enqueing")
