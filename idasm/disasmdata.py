#!/usr/bin/env python

"""
This module disassembles one imlac machine word into a data instruction
string.
"""

def disasmdata(word):
    result = ('DATA', f'{word:06o}')
    return result

if __name__ == '__main__':
    for word in range(0o177777):
        (op, fld) = disasmdata(word)
        print(f'{word:07o}: {op} {fld}')
