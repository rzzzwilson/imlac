#!/usr/bin/env python

"""
Process main instructions from a start address.
"""


import mem
import disasmm


mainQ = []


def dequeMain():
    result = None
    if len(mainQ) > 0:
        result = mainQ[0]
        del mainQ[0]
    return result

def effAddress(addr, offset):
    return (addr & 0o174000) + offset

def enqueMain(address):
    mainQ.append(address)

def getTargetAddress(code):
    return code & 0o3777

def isHLT(code):
    opcode = code & 0o177700
    return opcode == 0o000000

def isISZ(code):
    opcode = code & 0o174000
    return opcode == 0o030000 or opcode == 0o130000

def isJMP(code):
    opcode = code & 0o174000
    return opcode == 0o010000 or opcode == 0o110000

def isJMS(code):
    opcode = code & 0o174000
    return opcode == 0o034000 or opcode == 0o134000

def isMemRef(code):
    opcode = code & 0o174000
    if opcode == 0o020000 or opcode == 0o120000: return True
    if opcode == 0o024000 or opcode == 0o124000: return True
    if opcode == 0o044000 or opcode == 0o144000: return True
    if opcode == 0o050000 or opcode == 0o150000: return True
    if opcode == 0o054000 or opcode == 0o154000: return True
    if opcode == 0o060000 or opcode == 0o160000: return True
    if opcode == 0o064000 or opcode == 0o164000: return True
    if opcode == 0o070000 or opcode == 0o170000: return True
    if opcode == 0o074000 or opcode == 0o174000: return True
    return False

def isSAM(code):
    opcode = code & 0o174000
    return opcode == 0o074000 or opcode == 0o174000

def isSkip(code):
    opcode = code & 0o077777
    if opcode in [0o002001, 0o002002, 0o002004, 0o002010, 0o002020,
                  0o002040, 0o002100, 0o002200, 0o002400]:
        return True
    return False

def process(mem, addrlist, newcycle):
    """Mark address in 'addrlist' as MAIN instructions.

    mem       the Mem() object holding memory contents
    addrlist  list of addresses to mark
    newcycle  a new cycle number

    Is smart and adds targets of JMP, ISZ, etc, to addrlist.
    """

    for addr in addrlist:
        enqueMain(addr)

    while len(mainQ) > 0:
        address = dequeMain()
        while True:
            if mem.getCycle(address) == newcycle:   # seen it already
                break
            mem.putCycle(address, newcycle)

            code = mem.getCode(address)
            if mem.getRef(address):
                mem.decLab(getTargetAddress(code))
            (op, fld) = disasmm.disasmm(code)
            mem.putOp(address, op)
            mem.putFld(address, fld)
            if isJMP(code):
                memref = effAddress(address, getTargetAddress(code))
                mem.putFld(address, f"L{memref:05o}")
                mem.incLab(memref)
                enqueMain(memref)
                break
            elif isJMS(code):
                memref = effAddress(address, getTargetAddress(code))
                mem.putFld(address, f"L{memref:05o}")
                mem.incLab(memref)
                enqueMain(memref+1)
            elif isISZ(code) or isSAM(code):
                memref = effAddress(address, getTargetAddress(code))
                if memref < 0o100:
                    mem.putFld(address, f"0{memref:02o}")
                else:
                    mem.putFld(address, f"L{memref:05o}")
                    mem.incLab(memref)
                enqueMain(address+2)
            elif isMemRef(code):
                memref = effAddress(address, getTargetAddress(code))
                mem.setRef(address)
                mem.incLab(memref)
            elif isSkip(code):
                enqueMain(address+2)
            elif isHLT(code):
                break
            address = address + 1
