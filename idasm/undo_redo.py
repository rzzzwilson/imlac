#!/usr/bin/env python

"""
An Undo/Redo class.

A point to note is that in controlled code the underlying model data changes
_before_ any event is raised to controlling code.
THIS MUST BE KEPT IN MIND WHEN USING THIS MODULE!

The basic data structures are:
    .undo_list  list of undo objects
    .current    current application state
    .redo_list  list of redo objects

On a .save() call, put current on end of undo_list, new state goes to current.
Also truncate the redo_list.

On .undo() save current to redo_list, pop youngest state from undo_list, save
in current and return it.

On .redo() save current to undo_list, pop next redo from redo_list, save in
current and return it.

Note that we use deepcopy of states, as we cannot use originals, which will
change.

Methods:
    obj = UndoRedo(state)
        Create undo/redo object, initially contains 'state'
    obj.save(state1)
        Save 'state1' to system
    obj.refresh(state1)
        Change latest save to reflect 'state' - used to 'telescope' changes
    state = obj.undo()
        Pop UNDO state, return None if no UNDO possible
    state = obj.redo()
        Pop REDO state, return None if no REDO possible
    bool = obj.can_undo()
        Return True if UNDO is possible
    bool = obj.can_redo()
        Return True if REDO is possible
    state = obj.peek_undo()
        Get the state we would UNDO to, disturb nothing, None if no UNDO
    state = obj.peek_redo()
        Get the state we would REDO to, disturb nothing, None if no REDO
    str = obj.dump()
        Returns a debug string of module state

The .refresh() method above is used to 'telescope' similar changes into one
undo object.  An example is single-character changes to a name field.  We don't
really want to have many undos, one for each character.  So we telescope the
changes into one undo object by saving the first change character as an undo
and replacing the undo with the new state on each successive character change.

That is, in example code, we have something like the following code in
the handler for BT CHANGE code:

    if self.last_event_type == CHANGE:
        self.undoredo.refresh(state)
    else:
        self.undoredo(save(state))
    self.last_event_type = CHANGE
"""

import copy


class UndoRedo:
    """A class that encapsulates the undo/redo mechanism."""

    def __init__(self, state):
        """Instantiate the undo/redo object.

        state  current state object for undo/redo start
        """

        self.current = copy.deepcopy(state)     # the 'current' state
        self.undo_list = []                     # list of undo states
        self.redo_list = []                     # list of redo states

    def save(self, state):
        """Save a new current state.

        state  the new application state
        """

        self.undo_list.append(self.current)
        self.current = copy.deepcopy(state)
        self.redo_list = []                     # drop any pending redos

    def refresh(self, state):
        """Swap latest current undo with new state.

        state  the new application state

        We need to do this if we are 'telescoping' in calling code.  We
        telescope as we don't want typing in 3 letters to a textbox (for
        example) to be seen as three separate undo steps, just one.
        """

        self.current = copy.deepcopy(state) 

    def undo(self):
        """Do an undo.

        Return any pending undo, or None if undo not possible.
        """

        if not self.undo_list:
            return None

        self.redo_list.append(self.current)
        self.current = self.undo_list.pop()
        result = copy.deepcopy(self.current)

        return result

    def redo(self):
        """Do a redo.

        Return any pending redo, or None if redo not possible.
        """

        if not self.redo_list:
            return None

        self.undo_list.append(self.current)
        self.current = self.redo_list.pop()

        return copy.deepcopy(self.current)

    def can_undo(self):
        """Can we do an Undo?"""

        return self.undo_list

    def can_redo(self):
        """Can we do an Undo?"""

        return self.redo_list

    def peek_undo(self):
        """Get state that we would UNDO to, None if no UNDO."""

        try:
            return self.undo_list[-1]
        except IndexError:
            return None

    def peek_redo(self):
        """Get state that we would REDO to, None if no REDO."""

        try:
            return self.redo_list[-1]
        except IndexError:
            return None

    def peek_current(self):
        """Get the current state, disturb nothing."""

        return self.current

    def dump(self):
        """Return system state as a string."""

        result = "UndoRedo object:\n"

        if self.undo_list:
            prefix = "   Undo"
            for (i, s) in enumerate(self.undo_list):
                result += "%s %02d: %s\n" % (prefix, i, str(s))
                prefix = " " * len(prefix)
        else:
            result += "   Undo   : <none>\n"

        result += "Current   : %s\n" % str(self.current)

        if self.redo_list:
            prefix = "   Redo"
            for (i, s) in enumerate(self.redo_list):
                result += "%s %02d: %s\n" % (prefix, i, str(s))
                prefix = " " * len(prefix)
        else:
            result += "   Redo   : <none>\n"

        print(f"{result=}")
        return result
