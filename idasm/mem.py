#!/usr/bin/env python

"""
This module implements the 'mem' object and access routines.

A 'mem' object is a dictionary with the form:

{ address: (code, op, fld, comment, labcount, ref, type, cycle),
  address: (code, op, fld, comment, labcount, ref, type, cycle),
   ...
}

where address    is the address of the data (integer)
      code       is the code value at the address (integer)
      op         is the opcode (string)
      fld        is the instruction field (string)
      comment    is the comment for the address
      labcount   is the number of references to this address (integer)
      ref        is a flag to show if this word references another (boolean)
      type       is type of processed instruction 'm' for main
                                                  'd' for display
                                                  None for none
      cycle      is the cycle number when this word was processed
"""

import json


class Mem(object):
    def __init__(self, oldmem=None):
        """Init a Mem object.

        oldmem  optional dict: {addr: code, ...}
        """

        self.memory = {}
        self.start_address = None

        # handle optional initial values
        if oldmem:
            for (address, code) in oldmem.items():
                self.add(address, code)

    def dump(self, msg=None):
        items = list(self.memory.items())
        items.sort()
        if msg:
            print(msg)
        else:
            print(f"Mem object at {id(self):08x}, length={self.len()}")
        for (addr, data) in items:
            print(f"    {addr}: {data}")
        if self.start_address != None:
            print(f"Start address: {self.start_address:06o}")

    def getStart(self):
        return self.start_address

    def putStart(self, start):
        self.start_address = start

    def add(self, address, code):
        self.putMem(address, (code, "", "", "", 0, False, None, 0))

    def __len__(self):
        return len(self.memory)

    def haveMem(self, address):
        """Returns True if "address" is defined."""

        addrstr = f"{address:05o}"
        result = self.memory.get(addrstr, None)
        return result is not None

    def getMem(self, address):
        addrstr = f"{address:05o}"
        result = self.memory.get(addrstr, None)
        print(f"mem.getMem: {address=:06o}, result of .get() = {result}")
        if not result:
            result = (0, "", "", "", 0, False, None, 0)
            self.putMem(address, result)
        return result

    def getCode(self, address):
        (code, _, _, _, _, _, _, _) = self.getMem(address)
        return code

    def getOp(self, address):
        (_, op, _, _, _, _, _, _) = self.getMem(address)
        return op

    def getFld(self, address):
        (_, _, fld, _, _, _, _, _) = self.getMem(address)
        return fld

    def getComment(self, address):
        (_, _, _, comment, _, _, _, _) = self.getMem(address)
        return comment

    def getLabcount(self, address):
        (_, _, _, _, labcount, _, _, _) = self.getMem(address)
        return labcount

    def getRef(self, address):
        (_, _, _, _, _, ref, _, _) = self.getMem(address)
        return ref

    def getType(self, address):
        (_, _, _, _, _, _, type, _) = self.getMem(address)
        return type

    def getCycle(self, address):
        (_, _, _, _, _, _, _, cycle) = self.getMem(address)
        return cycle

    def putMem(self, address, entry):
        addrstr = f"{address:05o}"
        self.memory[addrstr] = entry

    def putCode(self, address, code):
        (_, op, fld, comment, labcount, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putOp(self, address, op):
        (code, _, fld, comment, labcount, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putFld(self, address, fld):
        (code, op, _, comment, labcount, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putComment(self, address, comment):
        (code, op, fld, _, labcount, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def incLabcount(self, address):
        (code, op, fld, comment, labcount, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount+1, ref, type, cycle))

    def putLabcount(self, address, labcount):
        (code, op, fld, comment, _, ref, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putRef(self, address, ref):
        (code, op, fld, comment, labcount, _, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putType(self, address, type):
        (code, op, fld, comment, labcount, ref, _, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def putCycle(self, address, cycle):
        (code, op, fld, comment, labcount, ref, type, _) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def decLab(self, address):
        (code, op, fld, comment, labcount, ref, type, cycle) = self.getMem(address)
        labcount -= 1
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def incLab(self, address):
        (code, op, fld, comment, labcount, ref, type, cycle) = self.getMem(address)
        labcount += 1
        self.putMem(address, (code, op, fld, comment, labcount, ref, type, cycle))

    def clearRef(self, address):
        (code, op, fld, comment, labcount, _, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, False, type, cycle))

    def setRef(self, address):
        (code, op, fld, comment, labcount, _, type, cycle) = self.getMem(address)
        self.putMem(address, (code, op, fld, comment, labcount, address, type, cycle))

    def keys(self):
        return list(self.memory.keys())

    def serialize(self):
        """Produce an object serializable by JSON."""

        return json.dumps((self.start_address, self.memory))

    def deserialize(self, serial_data):
        """Replace internal state by JSON data provided."""

        (self.start_address, self.memory) = json.loads(serial_data)
#        self.dump("After deserialize")


if __name__ == "__main__":
    # simple test
    print("Simple test:")
    mem = Mem()
    mem.putStart(0o100)
    mem.dump("Empty Mem() object")
