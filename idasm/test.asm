;------------------------
; Test code to exercise idasm.
;------------------------
	org	0100	;
start			;
	jmp	fred	;
			;
	org	0110	;
fred	isz	tom	;
	jmp	fred	;
	hlt		;
			;
tom	data	0	;
dick	bss	5	;
	law	1	;
harry   data	037777	;
			;
	end	start	;
