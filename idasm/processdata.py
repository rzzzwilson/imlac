#!/usr/bin/env python

"""
This module takes one word value and processes it as a DATA word.
"""


import disasmdata


dataQ = []

def dequeData():
    result = None
    if len(dataQ) > 0:
        result = dataQ[0]
        del dataQ[0]
    return result

def enqueData(address):
    dataQ.append(address)


def process(mem, addrlist, cycle):
    for addr in addrlist:
        code = mem.getCode(addr)
        mem.putOp(addr, "DATA")
        if code == 0:
            mem.putFld(addr, f"0")
        else:
            mem.putFld(addr, f"0{code:06o}")
        mem.putCycle(addr, cycle)
