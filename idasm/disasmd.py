#!/usr/bin/env python

"""
This module disassembles one imlac machine word into a display processor
instruction string.

Note that this routine has state - if a DEIM instruction is found, the *next*
instruction is treated as part of the DEIM.
"""

mode = "NONE"


def disasmd(word):
    global mode
    result = None
    if mode == "DEIM":
        return deimword(word)
    else:
        opcode = word & 0o0070000
        if   opcode == 0o0000000: result = doGrpZero(word)
        elif opcode == 0o0010000: result = ("DLXA", f"{(word & 0o07777):05o}")
        elif opcode == 0o0020000: result = ("DLYA", f"{(word & 0o07777):05o}")
        elif opcode == 0o0030000:
            mode = "DEIM"
            result = ("DEIM", f"{deimbyte(word & 0o377)}")
        elif opcode == 0o0040000: result = ("DLVH", f"{(word & 0o07777):05o}")
        elif opcode == 0o0050000: result = ("DJMS", f"{(word & 0o07777):05o}")
        elif opcode == 0o0060000: result = ("DJMP", f"{(word & 0o07777):05o}")
        else: result = ("DATA", f"{word:06o}")
    return result

def doGrpZero(word):
    if   word == 0o000000: return ("DHLT", "")
    elif word == 0o004000: return ("DNOP", "")
    elif word == 0o004004: return ("DSTS", "0")
    elif word == 0o004005: return ("DSTS", "1")
    elif word == 0o004006: return ("DSTS", "2")
    elif word == 0o004007: return ("DSTS", "3")
    elif word == 0o004010: return ("DSTB", "0")
    elif word == 0o004011: return ("DSTB", "1")
    elif word == 0o004020: return ("DDSP", "")
    elif word == 0o005000: return ("DIXM", "")
    elif word == 0o004400: return ("DIYM", "")
    elif word == 0o004040: return ("DRJM", "")
    elif word == 0o004200: return ("DDXM", "")
    elif word == 0o004100: return ("DDYM", "")
    elif word == 0o006000: return ("DHVC", "")
    elif word == 0o004015: return ("DOPR", "15")
    elif word == 0o004014: return ("DOPR", "14")
    else: return ("DATA", f"{word:06o}")
    return None

def deimbyte(byte):
    global mode
    byte = byte & 0o0377
    if byte & 0o0200:
        beam = 'D'
        xsign = '+'
        ysign = '+'
        if byte & 0o0100: beam = 'B'
        if byte & 0o0040: xsign = '-'
        if byte & 0o0004: ysign = '-'
        return f"{beam}{xsign}{byte & 0o030}{ysign}{byte & 0o003}"
    else:
        esc = '_'
        ret = '_'
        xinc = '_'
        xzero = '_'
        yinc = '_'
        yzero = '_'
        if byte & 0o100:
            mode = "NONE"
            esc = 'F'
        if byte & 0o040:
            mode = "NONE"
            ret = 'R'
        if byte & 0o020: xinc = '+'
        if byte & 0o010: xzero = '0'
        if byte & 0o002: yinc = '+'
        if byte & 0o001: yzero = '0'
#        return "%c%c%c%c%c%c" % (esc, ret, xinc, xzero, yinc, yzero)
        return f'{esc}{ret}{xinc}{xzero}{yinc}{yzero}'

def deimword(word):
#    result = ("%s" % deimbyte(word >> 8), "%s" % deimbyte(word & 0377))
#    return result
    return (f'{deimbyte(word >> 8)}', f'{deimbyte(word & 0o00377)}')


if __name__ == "__main__":
    for word in range(0o177777 + 1):
        result = disasmd(word)
        print(f"{word=:06o} {result=}")
