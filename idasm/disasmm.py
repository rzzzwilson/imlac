#!/usr/bin/env python

"""
This module disassembles one imlac machine word into a main processor
instruction string.  If the word isn't a legal instruction, return DATA.
"""


def disasmm(word):
    result = ('', '')
    opcode = word & 0o174000
    if   opcode == 0o000000: result = doGrpZero(word)
    elif opcode == 0o004000: result = ('LAW', f'{(word % 0o03777):05o}')
    elif opcode == 0o104000: result = ('LWC', f'{(word % 0o03777):05o}')
    elif opcode == 0o010000: result = ('JMP', f'{(word % 0o03777):05o}')
    elif opcode == 0o110000: result = ('JMP', f'*{(word & 0o03777):05o}')
    elif opcode == 0o020000: result = ('DAC', f'{(word % 0o03777):05o}')
    elif opcode == 0o120000: result = ('DAC', f'*{(word & 0o03777):05o}')
    elif opcode == 0o024000: result = ('XAM', f'{(word % 0o03777):05o}')
    elif opcode == 0o124000: result = ('XAM', f'*{(word & 0o03777):05o}')
    elif opcode == 0o030000: result = ('ISZ', f'{(word % 0o03777):05o}')
    elif opcode == 0o130000: result = ('ISZ', f'*{(word & 0o03777):05o}')
    elif opcode == 0o034000: result = ('JMS', f'{(word % 0o03777):05o}')
    elif opcode == 0o134000: result = ('JMS', f'*{(word & 0o03777):05o}')
    elif opcode == 0o044000: result = ('AND', f'{(word % 0o03777):05o}')
    elif opcode == 0o144000: result = ('AND', f'*{(word & 0o03777):05o}')
    elif opcode == 0o050000: result = ('IOR', f'{(word % 0o03777):05o}')
    elif opcode == 0o150000: result = ('IOR', f'*{(word & 0o03777):05o}')
    elif opcode == 0o054000: result = ('XOR', f'{(word % 0o03777):05o}')
    elif opcode == 0o154000: result = ('XOR', f'*{(word & 0o03777):05o}')
    elif opcode == 0o060000: result = ('LAC', f'{(word % 0o03777):05o}')
    elif opcode == 0o160000: result = ('LAC', f'*{(word & 0o03777):05o}')
    elif opcode == 0o064000: result = ('ADD', f'{(word % 0o03777):05o}')
    elif opcode == 0o164000: result = ('ADD', f'*{(word & 0o03777):05o}')
    elif opcode == 0o070000: result = ('SUB', f'{(word % 0o03777):05o}')
    elif opcode == 0o170000: result = ('SUB', f'*{(word & 0o03777):05o}')
    elif opcode == 0o074000: result = ('SAM', f'{(word % 0o03777):05o}')
    elif opcode == 0o174000: result = ('SAM', f'*{(word & 0o03777):05o}')
    elif opcode == 0o100000: result = doMicroCode(word)
    else: result = ('DATA', f'{word:07o}')

    return result

def doGrpZero(word):
    opcode = word & 0o177000
    if opcode == 0o000000:
        if word > 0:
            return ('HLT', f'{word:d}')
        return ('HLT', '')
    elif opcode == 0o003000: return doShRot(word)
    elif opcode == 0o002000: return doSkipFlag(word)
    elif opcode == 0o001000: return doIOT(word)
    else: print('DEFAULT'); return ('DATA', f'{word:06o}')
    

def doShRot(word):
    opcode = word & 0o177770
    if   opcode == 0o003040: return ('SAL', f'{(word & 0o7):d}')
    elif opcode == 0o003060: return ('SAR', f'{(word & 0o7):d}')
    elif opcode == 0o003100: return ('DON', '')
    elif opcode == 0o003000: return ('RAL', f'{(word & 0o7):d}')
    elif opcode == 0o003020: return ('RAR', f'{(word & 0o7):d}')
    else: return ('DATA', '%06o' % word)
    return None

def doSkipFlag(word):
    if   word == 0o02001: return ('ASZ', '')
    elif word == 0o02002: return ('ASP', '')
    elif word == 0o02004: return ('LSZ', '')
    elif word == 0o02010: return ('DSF', '')
    elif word == 0o02020: return ('KSF', '')
    elif word == 0o02040: return ('RSF', '')
    elif word == 0o02100: return ('TSF', '')
    elif word == 0o02200: return ('SSF', '')
    elif word == 0o02400: return ('HSF', '')
    else: return ('DATA', '%06o' % word)
    return None

def doIOT(word):
    if   word == 0o01003: return ('DLA', '')
    elif word == 0o01011: return ('CTB', '')
    elif word == 0o01012: return ('DOF', '')
    elif word == 0o01021: return ('KRB', '')
    elif word == 0o01022: return ('KCF', '')
    elif word == 0o01023: return ('KRC', '')
    elif word == 0o01031: return ('RRB', '')
    elif word == 0o01032: return ('RCF', '')
    elif word == 0o01033: return ('RRC', '')
    elif word == 0o01041: return ('TPR', '')
    elif word == 0o01042: return ('TCF', '')
    elif word == 0o01043: return ('TPC', '')
    elif word == 0o01051: return ('HRB', '')
    elif word == 0o01052: return ('HOF', '')
    elif word == 0o01061: return ('HON', '')
    elif word == 0o01062: return ('STB', '')
    elif word == 0o01071: return ('SCF', '')
    elif word == 0o01072: return ('IOS', '')
    elif word == 0o01274: return ('PSF', '')
    elif word == 0o01271: return ('PPC', '')
    elif word == 0o01101: return ('IOT', '0101')
    elif word == 0o01141: return ('IOT', '0141')
    elif word == 0o01161: return ('IOT', '0161')
    elif word == 0o01162: return ('IOT', '0162')
    elif word == 0o01131: return ('IOT', '0131')
    elif word == 0o01132: return ('IOT', '0132')
    elif word == 0o01134: return ('IOT', '0134')
    else: return ('DATA', '%06o' % word)
    return None

def doMicroCode(word):
    if   word == 0o100000: return ('NOP', '')
    elif word == 0o100001: return ('CLA', '')
    elif word == 0o100002: return ('CMA', '')
    elif word == 0o100003: return ('STA', '')
    elif word == 0o100004: return ('IAC', '')
    elif word == 0o100005: return ('COA', '')
    elif word == 0o100006: return ('CIA', '')
    elif word == 0o100010: return ('CLL', '')
    elif word == 0o100020: return ('CML', '')
    elif word == 0o100011: return ('CAL', '')
    elif word == 0o100030: return ('STL', '')
    elif word == 0o100040: return ('ODA', '')
    elif word == 0o100041: return ('LDA', '')
    elif word == 0o102001: return ('ASN', '')
    elif word == 0o102002: return ('ASM', '')
    elif word == 0o102004: return ('LSN', '')
    elif word == 0o102010: return ('DSN', '')
    elif word == 0o102020: return ('KSN', '')
    elif word == 0o102040: return ('RSN', '')
    elif word == 0o102100: return ('TSN', '')
    elif word == 0o102200: return ('SSN', '')
    elif word == 0o102400: return ('HSN', '')
    else: return ('DATA', '%06o' % word)
    return None


if __name__ == '__main__':
    for word in range(0o177777 + 1):
        (op, fld) = disasmm(word)
        print(f'{word:07o}: {op} {fld}')
