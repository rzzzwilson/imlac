/*****************************************************************************\
 * This is a test program to see how SDL2 can be used as the display for an
 * Imlac simulator.
 *
 * The display uses the idea of a "drawlist" that is created between display
 * writes.  This is similar to how the Imlac main processor writes display
 * code that is executed by the display processor.  Some attempt has been made
 * to lock display updates to a fixed rate.
 *
 * Draw some text, a pentagram and a few Thai numerals (0, 1, 2 and 3).
\******************************************************************************/

#include <stdbool.h>
#include <SDL.h>
#include <SDL_image.h>


#define WIDTH     1024
#define HEIGHT    1024

#define SAVEFILE  "snapshot.png"

#define BGND_RED         0
#define BGND_GREEN       0
#define BGND_BLUE        0

#define FGND_RED         0
#define FGND_GREEN       200
#define FGND_BLUE        0

// stuff for the drawlist
enum BEAM {OFF, ON};     // beam ON/OFF
enum OP {ABS, REL};      // absolute/relative moves

typedef struct
{
    enum OP type;
    enum BEAM beam;
    int dx;
    int dy;
} DLine;

#define DRAWLIST_SIZE    1000

// index of drawlist entry AFTER last instruction
unsigned int drawlist_end = 0;

//-------------------------
// Define some characters with relative moves
//-------------------------

DLine Char_I[] = {{REL, OFF, 0, -6},
                  {REL, ON, 2, 0},
                  {REL, OFF, -1, 0},
                  {REL, ON, 0, 6},
                  {REL, OFF, -1, 0},
                  {REL, ON, 2, 0},
                 };
int len_Char_I = (sizeof(Char_I) / sizeof(Char_I[0]));

DLine Char_m[] = {{REL, ON, 0, -3},
                  {REL, ON, 1, -1},
                  {REL, ON, 1, 1},
                  {REL, ON, 0, 1},
                  {REL, OFF, 0, -1},
                  {REL, ON, 1, -1},
                  {REL, ON, 1, 1},
                  {REL, ON, 0, 3},
                 };
int len_Char_m = (sizeof(Char_m) / sizeof(Char_m[0]));

DLine Char_l[] = {{REL, OFF, 1, 0},
                  {REL, ON, -1, -1},
                  {REL, ON, 0, -5},
                 };
int len_Char_l = (sizeof(Char_l) / sizeof(Char_l[0]));

DLine Char_a[] = {{REL, OFF, 0, -1},
                  {REL, ON, 0, -2},
                  {REL, ON, 1, -1},
                  {REL, ON, 2, 0},
                  {REL, ON, 1, 1},
                  {REL, ON, 0, 3},
                  {REL, OFF, -4, -1},
                  {REL, ON, 1, 1},
                  {REL, ON, 1, 0},
                  {REL, ON, 2, -2},
                 };
int len_Char_a = (sizeof(Char_a) / sizeof(Char_a[0]));

DLine Char_c[] = {{REL, OFF, 3, -1},
                  {REL, ON, -1, 1},
                  {REL, ON, -1, 0},
                  {REL, ON, -1, -1},
                  {REL, ON, 0, -2},
                  {REL, ON, 1, -1},
                  {REL, ON, 1, 0},
                  {REL, ON, 1, 1},
                 };
int len_Char_c = (sizeof(Char_c) / sizeof(Char_c[0]));

DLine Pentagram[] = {{REL, ON, 60, 0},
                     {REL, ON, -50, 30},
                     {REL, ON, 20, -50},
                     {REL, ON, 20, 50},
                     {REL, ON, -50, -30},
                    };
int len_Pentagram = (sizeof(Pentagram) / sizeof(Pentagram[0]));

DLine Suun[] = {{REL, OFF, 1, 0},
                {REL, ON, -1, -1},
                {REL, ON, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 2, 0},
                {REL, ON, 1, 1},
                {REL, ON, 0, 1},
                {REL, ON, -1, 1},
                {REL, ON, -2, 0},
               };
int len_Suun = (sizeof(Suun) / sizeof(Suun[0]));

DLine Neung[] = {{REL, OFF, 1, -1},
                 {REL, ON, 1, -1},
                 {REL, ON, 1, 1},
                 {REL, ON, -1, 1},
                 {REL, ON, -1, 0},
                 {REL, ON, -1, -1},
                 {REL, ON, 0, -1},
                 {REL, ON, 1, -1},
                 {REL, ON, 2, 0},
                 {REL, ON, 1, 1},
                 {REL, ON, 0, 1},
                 {REL, ON, -1, 1},
                };
int len_Neung = (sizeof(Neung) / sizeof(Neung[0]));

DLine Soong[] = {{REL, OFF, 1, -1},
                 {REL, ON, 1, 0},
                 {REL, ON, 0, 1},
                 {REL, ON, -1, 0},
                 {REL, ON, -1, -1},
                 {REL, ON, 0, -1},
                 {REL, ON, 1, -1},
                 {REL, ON, 1, 0},
                 {REL, ON, 1, 1},
                 {REL, ON, 0, 1},
                 {REL, OFF, 0, -1},
                 {REL, ON, 1, -1},
                 {REL, ON, 1, 1},
                 {REL, ON, 0, 2},
                 {REL, ON, -1, 1},
                 {REL, ON, -4, 0},
                 {REL, ON, -1, -1},
                 {REL, ON, 0, -4},
                };
int len_Soong = (sizeof(Soong) / sizeof(Soong[0]));

DLine Saam[] = {{REL, OFF, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 0, 1},
                {REL, ON, -1, 0},
                {REL, ON, -1, -1},
                {REL, ON, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 1, 1},
                {REL, ON, 0, 1},
                {REL, OFF, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 1},
                {REL, ON, 0, 2},
                {REL, ON, -1, 1},
               };
int len_Saam = (sizeof(Saam) / sizeof(Saam[0]));

DLine Si[] = {{REL, OFF, 3, -1},
              {REL, ON, 1, -1},
              {REL, ON, -1, -1},
              {REL, ON, -1, 1},
              {REL, ON, 3, 3},
              {REL, OFF, -1, -1},
              {REL, ON, -2, 0},
              {REL, ON, -2, -2},
              {REL, ON, 3, -3},
              {REL, ON, 0, -1},
             };
int len_Si = (sizeof(Si) / sizeof(Si[0]));

DLine Ha[] = {{REL, OFF, 3, -1},
              {REL, ON, 1, -1},
              {REL, ON, -1, -1},
              {REL, ON, -1, 1},
              {REL, ON, 3, 3},
              {REL, OFF, -1, -1},
              {REL, ON, -2, 0},
              {REL, ON, -2, -2},
              {REL, ON, 2, -2},
	      {REL, ON, -1, -1},
	      {REL, ON, -1, 1},
	      {REL, ON, 1, 1},
	      {REL, OFF, 1, -1},
	      {REL, ON, 1, -1},
              {REL, ON, 0, -1},
             };
int len_Ha = (sizeof(Ha) / sizeof(Ha[0]));

DLine Hok[] = {{REL, OFF, 1, -1},
               {REL, ON, 1, 0},
               {REL, ON, 0, -1},
               {REL, ON, -1, 0},
               {REL, ON, 0, 1},
               {REL, ON, 1, 1},
               {REL, ON, 1, 0},
               {REL, ON, 1, -1},
               {REL, ON, 0, -1},
	       {REL, ON, -1, -1},
	       {REL, ON, -1, 0},
	       {REL, ON, -1, -1},
              };
int len_Hok = (sizeof(Hok) / sizeof(Hok[0]));

DLine Chet[] = {{REL, OFF, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 0, 1},
                {REL, ON, -1, 0},
                {REL, ON, -1, -1},
                {REL, ON, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 1, 1},
                {REL, ON, 0, 1},
                {REL, OFF, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 1},
                {REL, ON, 0, 2},
                {REL, ON, 1, -1},
                {REL, ON, 0, -2},
                {REL, ON, -1, -1},
               };
int len_Chet = (sizeof(Chet) / sizeof(Chet[0]));

DLine Paet[] = {{REL, OFF, 1, 0},
                {REL, ON, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 2, 2},
                {REL, ON, 1, 0},
                {REL, ON, 1, -1},
                {REL, ON, -1, -1},
                {REL, ON, -1, 1},
                {REL, OFF, 1, -3},
                {REL, ON, -1, 1},
                {REL, ON, -4, 0},
                {REL, ON, -1, 1},
                {REL, ON, 0, 1},
                {REL, ON, 1, 1},
               };
int len_Paet = (sizeof(Paet) / sizeof(Paet[0]));

DLine Gaao[] = {{REL, OFF, 1, 0},
                {REL, ON, 1, 0},
                {REL, ON, 0, -1},
                {REL, ON, -1, 0},
                {REL, ON, 0, 1},
                {REL, ON, -1, -1},
                {REL, ON, 0, -1},
                {REL, ON, 1, -1},
                {REL, ON, 1, 0},
                {REL, ON, 2, 2},
                {REL, ON, 0, 1},
                {REL, OFF, -1, -2},
                {REL, ON, 0, -1},
                {REL, ON, 1, 0},
                {REL, ON, 1, -1},
               };
int len_Gaao = (sizeof(Gaao) / sizeof(Gaao[0]));

int Magnification = 4;


#define DISPLAY_FPS  40
#define MS_FRAME     (1000 / DISPLAY_FPS)

unsigned int TimeNextFrame;


/***********************************************
 * Error routine.  Print message and quit.
 ***********************************************/
void
imlac_error(char *msg)
{
    printf("%s\n", msg);
    exit(1);
}


/***********************************************
 * Returns the time left before the next frame
 * should be displayed.
 ***********************************************/
unsigned int
time_left(void)
{
    unsigned int now = SDL_GetTicks();

    return (TimeNextFrame <= now) ? 0 : TimeNextFrame - now;
}


/***********************************************
 * Append one drawlist "draw" instructions to
 * a drawlist.
 *
 * Also check if drawlist has overflowed.
 ***********************************************/
unsigned int
append_abs_move(DLine drawlist[], unsigned int dindex, enum BEAM beam,
	       	unsigned int x, unsigned int y, int maxsize)
{
    DLine dline = {ABS, beam, x, y};

    memcpy(&drawlist[dindex], &dline, sizeof(DLine));
    ++dindex;
    if (dindex >= maxsize)
	imlac_error("Draw list overflow");
    return dindex;
}


/***********************************************
 * Append one or more drawlist instructions to
 * a drawlist.
 *
 * Also check if drawlist has overflowed.
 ***********************************************/
unsigned int
append_drawlist(DLine drawlist[], unsigned int dindex, DLine *data, int arraylen, int maxsize)
{
    memcpy(&drawlist[dindex], data, arraylen * sizeof(DLine));
    dindex += arraylen;
    if (dindex >= maxsize)
	imlac_error("Draw list overflow");
    return dindex;
}


/***********************************************
 * Create the next drawlist.
 * All objects static except for the moving text
 * "Imlac".
 ***********************************************/
unsigned int
make_drawlist(DLine drawlist[], int maxsize)
{
    // where to place drawlist instructions in "drawlist" array
    unsigned int dindex = 0;

    static int x_origin = 10;
    static int y_origin = 45;
    static int deltax = 2;
    static int deltay = 1;

    // draw text at moving position: "Imlac"
    dindex = append_abs_move(drawlist, dindex, OFF, x_origin, y_origin, maxsize);
    dindex = append_drawlist(drawlist, dindex, Char_I, len_Char_I, maxsize);
    dindex = append_abs_move(drawlist, dindex, OFF, x_origin+4, y_origin, maxsize);
    dindex = append_drawlist(drawlist, dindex, Char_m, len_Char_m, maxsize);
    dindex = append_abs_move(drawlist, dindex, OFF, x_origin+10, y_origin, maxsize);
    dindex = append_drawlist(drawlist, dindex, Char_l, len_Char_l, maxsize);
    dindex = append_abs_move(drawlist, dindex, OFF, x_origin+13, y_origin, maxsize);
    dindex = append_drawlist(drawlist, dindex, Char_a, len_Char_a, maxsize);
    dindex = append_abs_move(drawlist, dindex, OFF, x_origin+19, y_origin, maxsize);
    dindex = append_drawlist(drawlist, dindex, Char_c, len_Char_c, maxsize);

    // move origins
    x_origin += deltax;
    y_origin += deltay;

    if (x_origin > 200 || x_origin < 10)
    {
        deltax = -deltax;
    }

    if (y_origin > 230 || y_origin < 10)
    {
        deltay = -deltay;
    }

    // draw a pentagram
    dindex = append_abs_move(drawlist, dindex, OFF, 50, 120, maxsize);
    dindex = append_drawlist(drawlist, dindex, Pentagram, len_Pentagram, maxsize);

    // draw "suun"
    dindex = append_abs_move(drawlist, dindex, OFF, 38, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Suun, len_Suun, maxsize);

    // draw "neung"
    dindex = append_abs_move(drawlist, dindex, OFF, 44, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Neung, len_Neung, maxsize);

    // draw "soong"
    dindex = append_abs_move(drawlist, dindex, OFF, 51, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Soong, len_Soong, maxsize);

    // draw "saam"
    dindex = append_abs_move(drawlist, dindex, OFF, 58, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Saam, len_Saam, maxsize);

    // draw "si"
    dindex = append_abs_move(drawlist, dindex, OFF, 65, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Si, len_Si, maxsize);

    // draw "ha"
    dindex = append_abs_move(drawlist, dindex, OFF, 71, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Ha, len_Ha, maxsize);

    // draw "hok"
    dindex = append_abs_move(drawlist, dindex, OFF, 77, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Hok, len_Hok, maxsize);

    // draw "chet"
    dindex = append_abs_move(drawlist, dindex, OFF, 83, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Chet, len_Chet, maxsize);

    // draw "paet"
    dindex = append_abs_move(drawlist, dindex, OFF, 91, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Paet, len_Paet, maxsize);

    // draw "gaao"
    dindex = append_abs_move(drawlist, dindex, OFF, 100, 50, maxsize);
    dindex = append_drawlist(drawlist, dindex, Gaao, len_Gaao, maxsize);

    return dindex;
}


/***********************************************
 * Routine to save the SDL screen to a PNG file.
 ***********************************************/
bool
saveScreenshotBMP(char *savefile, SDL_Window* window, SDL_Renderer* renderer)
{
    SDL_Surface* save_surface = NULL;
    SDL_Surface* info_surface = NULL;

    info_surface = SDL_GetWindowSurface(window);
    if (info_surface == NULL)
    {
        printf("Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - %s\n", SDL_GetError());
    }
    else
    {
        unsigned char * pixels = malloc(info_surface->w * info_surface->h * info_surface->format->BytesPerPixel);
        if (pixels == 0)
        {
            printf("Unable to allocate memory for screenshot pixel data buffer!\n");
            return false;
        }
        else
        {
            if (SDL_RenderReadPixels(renderer, &info_surface->clip_rect, info_surface->format->format,
                                     pixels, info_surface->w * info_surface->format->BytesPerPixel) != 0)
            {
                printf("Failed to read pixel data from SDL_Renderer object. SDL_GetError() - %s\n", SDL_GetError());
                free(pixels);
                return false;
            }
            else
            {
                save_surface = SDL_CreateRGBSurfaceFrom(pixels, info_surface->w, info_surface->h,
                                                       info_surface->format->BitsPerPixel,
                                                       info_surface->w * info_surface->format->BytesPerPixel,
                                                       info_surface->format->Rmask, info_surface->format->Gmask,
                                                       info_surface->format->Bmask, info_surface->format->Amask);
                if (save_surface == NULL)
                {
                    printf("Couldn't create SDL_Surface from renderer pixel data. SDL_GetError() - %s\n", SDL_GetError());
                    free(pixels);
                    return false;
                }
                IMG_SavePNG(save_surface, SAVEFILE);
                SDL_FreeSurface(save_surface);
                save_surface = NULL;
            }

            free(pixels);
        }
        SDL_FreeSurface(info_surface);
        info_surface = NULL;
    }

    return true;
}


/***********************************************
 * Drive the test screen.
 ***********************************************/
int
main(int argc, char **argv)
{
    // the drawlist buffer
    DLine drawlist[DRAWLIST_SIZE];

    // variables
    bool quit = false;
    SDL_Surface* icon = IMG_Load("icon.png");

    // the beam position coordinates
    unsigned int beam_x = 0;
    unsigned int beam_y = 0;

    // init SDL
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window * window = SDL_CreateWindow("SDL2 line drawing",
                                           SDL_WINDOWPOS_UNDEFINED,
                                           SDL_WINDOWPOS_UNDEFINED,
                                           WIDTH, HEIGHT, SDL_WINDOW_BORDERLESS);
    SDL_SetWindowIcon(window, icon);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    // set next tick time
    TimeNextFrame = SDL_GetTicks() + MS_FRAME;
 
    // handle events
    while (!quit)
    {
        // create the next drawlist
        drawlist_end = make_drawlist(drawlist, DRAWLIST_SIZE);

        // handle events, if any
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
// Can't happen in a BORDERLESS display
//                case SDL_QUIT:
//                    quit = true;
//                    break;
                case SDL_MOUSEBUTTONDOWN:
                    switch (event.button.button)
                    {
                        case SDL_BUTTON_LEFT:
                            // do nothing at the moment
                            break;
                        case SDL_BUTTON_MIDDLE:
                            // middle button saves a screen snapshot
                            saveScreenshotBMP(SAVEFILE, window, renderer);
                            break;
                        case SDL_BUTTON_RIGHT:
                            // right button closes the program
                            quit = true;
                            break;
                    }
                    break;
            }
        }
 
        // wait until next frame should be shown
        SDL_Delay(time_left());
        TimeNextFrame += MS_FRAME;

        // start drawing new screen
        // clear window to background colour
        SDL_SetRenderDrawColor(renderer, BGND_RED, BGND_GREEN, BGND_BLUE, 255);
        SDL_RenderClear(renderer);
 
        // set draw colour - foreground
        SDL_SetRenderDrawColor(renderer, FGND_RED, FGND_GREEN, FGND_BLUE, 255);

        // set initial beam position to (0, 0)
        beam_x = 0;
        beam_y = 0;

        // render the drawlist
        for (int dindex = 0; dindex < drawlist_end; ++dindex)
        {
            enum OP type = drawlist[dindex].type;
            enum BEAM beam = drawlist[dindex].beam;
            int dx = drawlist[dindex].dx * Magnification;
            int dy = drawlist[dindex].dy * Magnification;

            unsigned int new_x;
            unsigned int new_y;

            if (type == ABS)
            {
                new_x = dx;
                new_y = dy;

                if (beam == ON)
                {
                    SDL_RenderDrawLine(renderer, beam_x, beam_y, new_x, new_y);
                }
            }
            else
            {
                new_x = beam_x + dx;
                new_y = beam_y + dy;

                if (beam == ON)
                {
                    SDL_RenderDrawLine(renderer, beam_x, beam_y, new_x, new_y);
                }
            }

            beam_x = new_x;
            beam_y = new_y;
        }

        // render window
        SDL_RenderPresent(renderer);
    }

    // cleanup SDL
 
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
 
    return 0;
}

