#include <stdbool.h>
#include <SDL.h>

#define WIDTH	1024
#define HEIGHT	1024
 
int main(int argc, char ** argv)
{
    // variables
    bool quit = false;
    SDL_Event event;
 
    // init SDL
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window * window = SDL_CreateWindow("SDL2 line drawing",
                                           SDL_WINDOWPOS_UNDEFINED,
					   SDL_WINDOWPOS_UNDEFINED,
					   WIDTH, HEIGHT, SDL_WINDOW_BORDERLESS);
    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);

    // handle events
    while (!quit)
    {
        SDL_Delay(10);
        SDL_PollEvent(&event);
 
        switch (event.type)
        {
            case SDL_QUIT:
                quit = true;
                break;
	    case SDL_MOUSEBUTTONUP:
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        quit = true;
                        break;
                }
                break;
        }
 
        // clear window
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
 
        // TODO rendering code goes here
        SDL_SetRenderDrawColor(renderer, 200, 255, 200, 255);
        SDL_RenderDrawLine(renderer, 0, 0, WIDTH, HEIGHT);
        SDL_RenderDrawLine(renderer, WIDTH, 0, 0, HEIGHT);
 
        // render window
 
        SDL_RenderPresent(renderer);
    }
 
    // cleanup SDL
 
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
 
    return 0;
}

