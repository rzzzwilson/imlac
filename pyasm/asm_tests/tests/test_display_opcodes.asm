; test the display processor opcodes
;**************************
; display processor orders (partial listing)
;**************************
        org     0100

        dhlt
        dnop
;|$1 0o000000
;|   0o004000
        dsts    0
        dsts    1
        dsts    2
        dsts    3
;|   0o004004
;|   0o004005
;|   0o004006
;|   0o004007
        dstb    0
        dstb    1
        dstb    2
        dstb    3
;|   0o004010
;|   0o004011
;|   0o004012
;|   0o004013
        ddsp
;|   0o004020
        dixm
;|   0o005000
        diym
;|   0o004400
        drjm
;|   0o004040
        ddxm
;|   0o004200
        ddym
;|   0o004100
        dhvc            ; early PDS-1 only
;|   0o006000
        dopr    015
        dopr    014
;|   0o004015
;|   0o004014
        dlxa    0
        dlxa    1
        dlxa    01777
;|   0o010000 + 0
;|   0o010000 + 1
;|   0o010000 + 0o1777
        dlya    0
        dlya    1
        dlya    01777
;|   0o020000 + 0
;|   0o020000 + 1
;|   0o020000 + 0o1777
        inc     e,f
;|   0o030171
        inc     e,n
;|   0o030111
        inc     e,r
;|   0o030151
        inc     e,p
        inc     p,f
;|   0o030200
;|   0o100171
        inc     e,a0377
        inc     f,f
;|   0o030377
;|   0o074571
        inc     e,d+3+3
        inc     d+2+1,b-3+2
        inc     b-2-2,b+0-3
        inc     b+2-1,b+3+2
        inc     D+0-2,b+0+3
        inc     b+0+3,B+0+3
        inc     f,f
;|   0o030233
;|   0o110772
;|   0o173307
;|   0o152732
;|   0o103303
;|   0o141703
;|   0o074571
        inc     e,d+1+0
        inc     b+1+3,b+1+3
        inc     b+1+3,d+3+0
        inc     b-1-3,b-1-3
        inc     b-1-3,d+2+3
        inc     b-3+0,b-3+0
        inc     d+2+3,b+3+0
        inc     b+3+0,d-3-3
        inc     f,a000
;|   0o030210
;|   0o145713
;|   0o145630
;|   0o167757
;|   0o167623
;|   0o174370
;|   0o111730
;|   0o154277
;|   0o074400
        djms    0
        djms    1
        djms    01777
;|   0o050000 + 0
;|   0o050000 + 1
;|   0o050000 + 0o1777
        djmp    0
        djmp    1
        djmp    01777
;|   0o060000 + 0
;|   0o060000 + 1
;|   0o060000 + 0o1777
        dhlt
;|   0o000000
        dnop
;|   0o004000
        dsts    0
        dsts    1
        dsts    2
        dsts    3
;|   0o004004
;|   0o004005
;|   0o004006
;|   0o004007
        dstb    0
        dstb    1
        dstb    2
        dstb    3
;|   0o004010
;|   0o004011
;|   0o004012
;|   0o004013
        ddsp
;|   0o004020
        dixm
        diym
;|   0o005000
;|   0o004400
        drjm
;|   0o004040
        ddxm
        ddym
;|   0o004200
;|   0o004100
        dhvc
;|   0o006000

        end
