; test pseudo-ops
        org     0100

test    equ     045

start   hlt
        law     test

        org     0200

start2  nop

        org     0300

start3  nop
        bss     1
        hlt

str     asciiz  'str'
str2    asciiz  'str0'

str3    ascii   'str11'

dat     data    0
dat2    data    0177777

        end     start
;|$1   0o000000
;|     0o004000 + 0o45
;|$2   0o100000
;|$3   0o100000
;|$4   0o000000          ; BSS behaves like 'org .+n'
;|     0o071564          ; 'st'
;|     0o071000          ; 'r\0'
;|     0o071564          ; 'st'
;|     0o071060          ; 'r0'
;|     0o000000          ; zero byte
;|     0o071564          ; 'rs'
;|     0o071061          ; 't1'
;|     0o030400          ; '1\0'
;|     0o000000
;|     0o177777
