; test file with two adjacent ORG blocks
        org     0100
        law     1
        hlt

        org     0102
        law     2
        hlt
        end

;|$1   0o04000 + 1
;|     0o00000
;|     0o04000 + 2
;|     0o00000
