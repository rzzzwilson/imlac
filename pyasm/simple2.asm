; write "A" to "Z" to punch
        org     0100
start   lwc     27
        dac     count
        hon
loop    isz     count
        hlt
        lac     char
        jms     punch
;        lac     char
        iac
        dac     char
        jmp loop

punch   data    0
ready   psf
        jmp     .-1
        ppc        
        jmp     *punch

count   data    0
char    data    0102    ; ASCII 'A'

        end     start
