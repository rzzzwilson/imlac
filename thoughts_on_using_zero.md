The RC2014 Z80 microcontroller uses a Raspberry Pi Zero as a "graphics card".
The Z80 talks to the Zero through its RX/TX lines.  The Zero outputs HDMI or
composite video.  The Zero can have a USB keyboard attached and that data
could be made available to the imlac emulator.

This would no longer be a strictly software simulation.  Would probably need
to run the simulation on something like an ESP-32 board.  Could also attach
the ESP-32 to a microSD card board to get mass storage on which to store
paper tape files.  Could use the wifi connection to allow access to storage
on the local network?

Or why nor run *everything* on the Pi Zero W??  Tape images could be local
or over the local net.  So we are back to fully software emulation again!
Could be standard RpiOS or a dedicated system?  We would certainly develop
on the standard OS.  Should run on any RPi, including 4.

Maybe allow access to a mouse over USB.  Make it look like a trackball?

Links
=====

https://hackaday.io/project/9567-5-graphics-card-for-homebrew-z80

https://github.com/fbergama/pigfx
